<?php


namespace App\Application\Settings;

use App\Entity\Settings;
use App\Repository\SettingsRepository;
use Cassandra\Set;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class SettingsManager
 * @package App\Application\Settings
 */
class SettingsManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * SettingsManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return mixed
     */
    public function getJobLinkExpirationHours()
    {
        return $this->getScalarValueByKey(Settings::JOB_LINK_VALIDITY_IN_HOURS);
    }

    /**
     * @param int $hours
     * @return Settings
     */
    public function editJobLinkExpirationHours(int $hours)
    {
        $setting = $this->getSettingsRepository()->getByKey(Settings::JOB_LINK_VALIDITY_IN_HOURS);
        if (!($setting instanceof Settings)) {
            $setting = new Settings();
            $setting->setName(Settings::JOB_LINK_VALIDITY_IN_HOURS);
        }

        $setting->setCategory("general");
        $setting->setDescription("Offer expiration link");
        $setting->setData([
            'value' => $hours
        ]);

        return $this->saveSettings($setting);
    }

    /**
     * @return bool|mixed
     */
    public function getAllowVoucherApply()
    {
        return $this->getBooleanValueByKey(Settings::ALLOW_VOUCHER_APPLY);
    }

    /**
     * @param bool $value
     * @return bool|mixed
     */
    public function editAllowVoucherApply(bool $value)
    {
        return $this->setBooleanValueByKey(Settings::ALLOW_VOUCHER_APPLY, $value);
    }

    /**
     * @return array
     */
    public function getStandardPricePerHour(): array
    {
       return $this->getPriceSettingsForKey(Settings::STANDARD_PRICE_HOUR);
    }

    /**
     * @param float $price
     * @param string $currency
     * @param string $category
     * @param string $description
     * @return Settings
     */
    public function editStandardPricePerHour(float $price, string $currency, string $category = "general", string $description = "Price per hour"): Settings
    {
        return $this->editPriceForKey(Settings::STANDARD_PRICE_HOUR, $price, $currency, $category, $description);
    }

    /**
     * @param float $price
     * @param string $currency
     * @param string $category
     * @param string $description
     * @return Settings
     */
    public function editWorkerPricePerHour(float $price, string $currency, string $category = "general", string $description = "Price per hour"): Settings
    {
        return $this->editPriceForKey(Settings::STANDARD_PRICE_INDEPENDENT_WORKER, $price, $currency, $category, $description);
    }

    /**
     * @param string $key
     * @param float $price
     * @param string $currency
     * @param string $category
     * @param string $description
     * @return Settings
     */
    public function editPriceForKey(string $key, float $price, string $currency, string $category = "general", string $description = "Price per hour"): Settings
    {
        $setting = $this->getSettingsRepository()->getByKey($key);
        if (!($setting instanceof Settings)) {
            $setting = new Settings();
        }
        $setting->setName($key);
        $setting->setCategory($category);
        $setting->setDescription($description);
        $setting->setData([
            'price' => $price,
            'currency' => $currency
        ]);

        return $this->saveSettings($setting);
    }

    /**
     * @return array
     */
    public function getStandardPricePerWorker(): array
    {
        return $this->getPriceSettingsForKey(Settings::STANDARD_PRICE_INDEPENDENT_WORKER);
    }

    /**
     * @param string $key
     * @return array
     */
    private function getPriceSettingsForKey(string $key)
    {
        $setting = $this->getNonNullSettings($key);
        $data = $setting->getData();
        $price = $data['price'];
        $currency = $data['currency'];

        return [$price, $currency];
    }

    /**
     * @param string $key
     * @return Settings|object|null
     */
    public function getNonNullSettings(string $key)
    {
        $setting =  $this->getSettingsRepository()->getByKey($key);
        if (!($setting instanceof Settings)) {
            throw new \RuntimeException("Settings not found for key $key");
        }

        return $setting;
    }

    /**
     * @return SettingsRepository
     */
    public function getSettingsRepository(): SettingsRepository
    {
        return $this->entityManager->getRepository(Settings::class);
    }

    /**
     * @param string $key
     * @return mixed
     */
    private function getBooleanValueByKey(string $key): bool
    {
        $settings = $this->getNonNullSettings($key);
        return (bool) $settings->getData()['value'];
    }

    /**
     * @param string $key
     * @param bool $value
     * @return mixed
     */
    private function setBooleanValueByKey(string $key, bool $value): Settings
    {
        $settings = $this->getNonNullSettings($key);
        $settings->setData(['value' => $value]);

        return $this->saveSettings($settings);
    }

    /**
     * @param string $key
     * @return mixed
     */
    private function getScalarValueByKey(string $key)
    {
        $settings = $this->getNonNullSettings($key);
        return $settings->getData()['value'];
    }

    /**
     * @param Settings $setting
     * @return Settings
     */
    private function saveSettings(Settings $setting)
    {
        $this->entityManager->persist($setting);
        $this->entityManager->flush();
        $this->entityManager->refresh($setting);

        return $setting;
    }
}