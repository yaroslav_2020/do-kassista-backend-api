<?php

declare(strict_types=1);

namespace App\Application\Translation;

use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Translation\Exception\InvalidArgumentException;
use Symfony\Component\Translation\MessageCatalogueInterface;
use Symfony\Component\Translation\TranslatorBagInterface;
use Symfony\Contracts\Translation\LocaleAwareInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class DbTranslator
 * @package App\Application\Translation
 *
 * overrides Symfony\Bundle\FrameworkBundle\Translation\Translation
 */
class DbTranslator implements TranslatorInterface, TranslatorBagInterface, LocaleAwareInterface
{
    private $translator;

    protected $dbLoader;

    protected $locale;

    protected $catalog;

    protected $catalogLoaded = false;

    /**
     * DbTranslator constructor.
     * @param TranslatorInterface $translator
     * @param DbLoader $dbLoader
     */
    public function __construct(TranslatorInterface $translator, DbLoader $dbLoader)
    {
        $this->translator = $translator;
        $this->dbLoader = $dbLoader;
    }

    /**
     * @param string $id
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return string
     */
    public function trans($id, array $parameters = [], $domain = null, $locale = null)
    {
        if ($locale === null) {
            $locale = $this->locale;
        }
        if ($domain === null) {
            $domain = 'general';
        }
        $localeCatalog = $this->getCatalogue($locale);

        return self::translate($id, $parameters, $domain, $locale, $localeCatalog);
    }

    /**
     * @param $id
     * @param array $parameters
     * @param $domain
     * @param $locale
     * @param $localeCatalog
     * @return mixed
     */
    public static function translate($id, array $parameters, $domain, $locale, $localeCatalog)
    {
//        die(var_dump($id, $domain, $locale, $localeCatalog));
        $translatedId = $id;
        if (!empty($localeCatalog)) {
            if ($locale !== null) {
                if (isset($localeCatalog[$domain]) && isset($localeCatalog[$domain][$id])) {
                    $translatedId = $localeCatalog[$domain][$id];
                }
            }
        }

        foreach ($parameters as $parameterKey => $parameterValue) {
            $translatedId = str_replace($parameterKey, $parameterValue, $translatedId);
        }

        return $translatedId;
    }

    /**
     * @param $id
     * @param $number
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return mixed
     */
    public function transChoice($id, $number, array $parameters = [], $domain = null, $locale = null)
    {
        throw new \RuntimeException("Translation not available as choice");
    }

    /**
     * @param null $locale
     * @return array
     */
    public function getCatalogue($locale = null)
    {
        if ($this->catalogLoaded === false) {
            $this->catalog = $this->dbLoader->loadAll();
        }

        if ($locale === null) {
            return $this->catalog;
        }
        if (isset($this->catalog[$locale])) {
            return $this->catalog[$locale];
        }

        return [];
    }

    /**
     * @param string $locale
     * @return string
     */
    public function setLocale($locale)
    {
        return $this->locale = $locale;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }
}