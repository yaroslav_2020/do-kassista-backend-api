<?php

declare(strict_types=1);

namespace App\Application\Translation;

use ApiPlatform\Core\Exception\InvalidResourceException;
use App\Entity\Translation;
use App\Repository\TranslationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Symfony\Component\Translation\Loader\LoaderInterface;
use Symfony\Component\Translation\MessageCatalogue;

/**
 * Class DbLoader
 * @package App\Application\Translation
 */
class DbLoader
{
    /**
     * @var EntityManagerInterface
     */
    private $doctrine;

    /**
     * DbLoader constructor.
     * @param ContainerInterface $container
     * @param EntityManagerInterface $doctrine
     */
    public function __construct(ContainerInterface $container, EntityManagerInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @return array
     */
    public function loadAll()
    {
        $messages = $this->doctrine->getRepository(Translation::class)->findAll();
        $catalog = [];
        /** @var Translation $message */
        foreach ($messages as $message) {
            $catalog[$message->getLocale()][$message->getDomain()][$message->getSource()] = $message->getTranslation();
        }

        return $catalog;
    }
}