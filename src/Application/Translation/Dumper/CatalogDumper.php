<?php

declare(strict_types=1);

namespace App\Application\Translation\Dumper;

use App\Entity\Translation;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Translation\Dumper\DumperInterface;

/**
 * Class MainCatalogDumper
 * @package App\Application\Translation\Dumper
 */
class CatalogDumper
{
    /** @var EntityManagerInterface */
    protected $doctrine;

    /** @var string */
    protected $projectDir;

    /** @var EntityRepository */
    protected $translationRepo;

    /**
     * MainCatalogDumper constructor.
     * @param EntityManagerInterface $doctrine
     * @param $projectDir
     */
    public function __construct(EntityManagerInterface $doctrine, $projectDir)
    {
        $this->doctrine = $doctrine;
        $this->projectDir = $projectDir;
        $this->translationRepo = $this->doctrine->getRepository(Translation::class);

    }

    /**
     * Dumps translations in database
     */
    public function dump()
    {
        $return= [ "updated" => 0, "removed" => 0, "new" => 0];

        $fileSystem = new Filesystem();
        $translationFilesPath = $this->projectDir . "/src/Application/Translation/Dumper/catalog";
        
        if (!$fileSystem->exists($translationFilesPath)) {
            return;
        }

        $finder = new Finder();
        $files = $finder->in($translationFilesPath);
        
        /** @var \SplFileObject $file */
        foreach ($files as $file) {
            $realPath = $file->getRealPath();
            $translations = require($realPath);
            if (!is_array($translations) || count($translations) !== 3) {
                throw new \RuntimeException("Translations $realPath is not array or does not have only 2 keys");
            }

            if (!isset($translations['domain'])) {
                throw new \RuntimeException("Domain not set for translation in $realPath");
            }
            if (!isset($translations['messages'])) {
                throw new \RuntimeException("messages not set for translation $realPath");
            }
            if (!isset($translations['locale'])) {
                throw new \RuntimeException("locale not set for translation $realPath");
            }
            $domain = $translations['domain'];
            $messages = $translations['messages'];
            $locale = $translations['locale'];

            foreach ($messages as $messageSource => $messageTarget) {
                $translation = $this->translationRepo->findByDomainLocaleAndSource(
                    $domain,
                    $locale,
                    $messageSource
                );

                if ($translation instanceof Translation) {
                    $translation->setTranslation($messageTarget);
                    $return['updated']++;
                } else {
                    $translation = new Translation();
                    $translation->setLocale($locale);
                    $translation->setSource($messageSource);
                    $translation->setTranslation($messageTarget);
                    $translation->setDomain($domain);
                    $return["new"]++;
                }

                $this->doctrine->persist($translation);
            }

            $this->doctrine->flush();
        }

        return $return;
    }
}