<?php

return [
    "domain" => "register",
    "locale" => "en",
    "messages" => [
        "email.register.subject" => "Welcome to TRD platform",
        "email.global.register.hello" => "Hello %username%",
        "email.global.register.confirm_link" => "Please confirm the account by accessing this link <a href='%link%'>Link</a>"
    ],
];