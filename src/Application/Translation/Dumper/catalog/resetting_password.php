<?php

return [
    "domain" => "resetting_password",
    "locale" => "en",
    "messages" => [
        "email.global.subject" => "Password reset requested",
        "email.global.message.link" => 'You requested a reset password. Please click this <a href="%confirmationUrl%">Link</a>'
    ],
];