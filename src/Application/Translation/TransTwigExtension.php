<?php

declare(strict_types=1);

namespace App\Application\Translation;


use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\Translation\TranslatorInterface as LegacyTranslatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\NodeVisitor\NodeVisitorInterface;

/**
 * Class TransTwigExtension
 * @package App\Application\Translation
 */
class TransTwigExtension extends TranslationExtension
{
    /**
     * @param TranslatorInterface|null $translator
     * @param NodeVisitorInterface|null $translationNodeVisitor
     */
    public function __construct(TranslatorInterface $translator, NodeVisitorInterface $translationNodeVisitor = null)
    {
        parent::__construct($translator, $translationNodeVisitor);
    }
}