<?php

declare(strict_types=1);

namespace App\Application\Mailer;


use App\Application\Cors\WebsiteDomain;
use App\Entity\Settings;
use App\Repository\SettingsRepository;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use App\Entity\Offer;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class AuthMailer
 * @package App\Application\Mailer
 */
class AuthMailer extends AbstractController implements MailerInterface
{



    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var array
     */
    protected $emailSettings;

    /** @var WebsiteDomain */
    protected $websiteDomain;

    protected $container;

    /**
     * TwigSwiftMailer constructor.
     *
     * @param ContainerInterface $container
     * @param $MailtrapMailer
     * @param $MailgunMailer
     * @param SettingsRepository $settings
     * @param Environment $twig
     * @param WebsiteDomain $websiteDomain
     * @param array $emailSettings
     */
    public function __construct(
        ContainerInterface $container,
        $MailtrapMailer,
        $MailgunMailer,
        SettingsRepository $settings,
        Environment $twig,
        WebsiteDomain $websiteDomain,
        array $emailSettings
    ) {
        $this->mailer = ((bool)$settings->getSetting(SETTINGS::DEV_MODE)) ? $MailtrapMailer : $MailgunMailer;
        $this->twig = $twig;
        $this->emailSettings = $emailSettings;
        $this->websiteDomain = $websiteDomain;
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function sendConfirmationEmailMessage(UserInterface $user): int
    {
        $settings = $this->emailSettings['register_confirm_email'];
        $from = $settings['from'];

        $url = $this->websiteDomain->generateEmailUrl(
            'register_confirm_email',
            ['token' => $user->getConfirmationToken()]
        );

        $context = array(
            'user' => $user,
            'confirmationUrl' => $url,
        );

        return $this->sendMessage($settings, $context, $from, (string) $user->getEmail());
    }

    /**
     * {@inheritdoc}
     */
    public function sendResettingEmailMessage(UserInterface $user): int
    {
        $settings = $this->emailSettings['resetting_password'];
        $from = $settings['from'];
        $url = $this->websiteDomain->generateEmailUrl(
            'client_authenticate_reset_password',
            ['token' => $user->getConfirmationToken()]
        );

        $context = array(
            'user' => $user,
            'confirmationUrl' => $url,
        );

        return $this->sendMessage($settings, $context, $from, (string) $user->getEmail());
    }

    public function sendPaymentBlockedEmailMessage(UserInterface $user, $amount_blocked, $offer_id, $offer_created_at, $day, $day_start_time, $day_end_time): int
    {
        $settings = $this->emailSettings['payment_blocked'];
        $from = $settings['from'];

        $context = array(
            'user' => $user->getName(),
            'amount_blocked' => $amount_blocked,
            'offer_id' => $offer_id,
            'day' => $day,
            'offer_created_at' => $offer_created_at,
            'day_start_time' => $day_start_time,
            'day_end_time' => $day_end_time
        );

        return $this->sendMessage($settings, $context, $from, (string) $user->getEmail());
    }

    public function sendPaymentReleaseEmailMessage(UserInterface $user, $payment_release, $offer_id, $offer_created_at, $day, $day_start_time, $day_end_time): int
    {
        $settings = $this->emailSettings['payment_release'];
        $from = $settings['from'];

        $context = array(
            'user' => $user->getName(),
            'payment_release' => $payment_release,
            'offer_id' => $offer_id,
            'day' => $day,
            'offer_created_at' => $offer_created_at,
            'day_start_time' => $day_start_time,
            'day_end_time' => $day_end_time
        );

        return $this->sendMessage($settings, $context, $from, (string) $user->getEmail());
    }

    /**
     * {@inheritdoc}
     */
    public function sendNewOfferInfo(Offer $offer, $email): int
    {
        $settings = $this->emailSettings['new_offer'];
        $from = $settings['from'];
        $url = $this->websiteDomain->generateEmailUrl(
            'client_offer_item',
            ['id' => $offer->getId()]
        );

        $context = array(
            'client' => $offer->getClient()[0]->getName(),
            'id' => $offer->getId(),
            'voucher' => $offer->getVouchers(),
            'price_per_service' => $offer->getPricePerService(),
            'start_from_day' => $offer->getStartFromDate()->format('d-m-Y'),
            'period' => $offer->getPeriod(),
            'rooms' => $offer->getOfferBuilding()->getRoomNumber(),
            'bathrooms' => $offer->getOfferBuilding()->getBathRoomNumber(),
            'kitchen' => $offer->getOfferBuilding()->getKitchenNumber(),
            'livingrooms' => $offer->getOfferBuilding()->getLivingNumber(),
            'access' => str_replace("_", " ", $offer->getOfferBuilding()->getAccessType()),
            'access_description' => $offer->getOfferBuilding()->getAccessDescription(),
            'more_details_url' => $url,
        );

        try {
            return $this->sendMessage($settings, $context, $from, $email);;
        } catch (Exception $error) {
        }
    }

    /**
     * {@inheritdoc}
     */
    public function sendNewOfferInfoToWorkers(Offer $offer, $email, $confirmation_link): int
    {
        $settings = $this->emailSettings['new_offer_worker'];
        $from = $settings['from'];

        $context = array(
            'client' => $offer->getClient()[0]->getName(),
            'id' => $offer->getId(),
            'start_from_day' => $offer->getStartFromDate()->format('d-m-Y'),
            'period' => $offer->getPeriod(),
            'rooms' => $offer->getOfferBuilding()->getRoomNumber(),
            'bathrooms' => $offer->getOfferBuilding()->getBathRoomNumber(),
            'kitchen' => $offer->getOfferBuilding()->getKitchenNumber(),
            'livingrooms' => $offer->getOfferBuilding()->getLivingNumber(),
            'access' => str_replace("_", " ", $offer->getOfferBuilding()->getAccessType()),
            'access_description' => $offer->getOfferBuilding()->getAccessDescription(),
            'confirmation_link' => $confirmation_link,
        );

        try {
            return $this->sendMessage($settings, $context, $from, $email);;
        } catch (Exception $error) {
        }
    }

    /**
     * {@inheritdoc}
     */
    public function sendCongratulationsToAssignedWorker(Offer $offer, User $worker): int
    {
        $settings = $this->emailSettings['worker_congratulations'];
        $from = $settings['from'];

        $context = array(
            'client' => $offer->getClient()[0]->getName(),
            'id' => $offer->getId(),
            'worker' => $worker->getName(),
        );

        try {
            return $this->sendMessage($settings, $context, $from, $worker->getEmail());;
        } catch (Exception $error) {
        }
    }

    /**
     * @param array $settings
     * @param $context
     * @param $fromEmail
     * @param $toEmail
     * @return int
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws \Throwable
     */
    protected function sendMessage(
        array $settings,
        $context,
        $fromEmail,
        $toEmail
    ): int {
        $txtTemplate = $settings['template']['text'];
        $htmlTemplate = $settings['template']['html'];
        $subjectTemplate = $settings['template']['subject'];

        $templateSubject = $this->twig->load($subjectTemplate);

        $message = (new \Swift_Message());

        $context['logo'] = $message->embed(\Swift_Image::fromPath(dirname(__FILE__, 4) . '/templates/logo.png'));
        $context['email'] = $toEmail;

        $subject = $templateSubject->renderBlock('subject', $context);
        $textBody = $this->twig->render($txtTemplate, $context);
        $htmlBody = $this->twig->render($htmlTemplate, $context);

        $message->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($toEmail);

        if (!empty($htmlBody)) {
            $message->setBody($htmlBody, 'text/html')
                ->addPart($textBody, 'text/plain');
        } else {
            $message->setBody($textBody);
        }

        if (isset($this->mailer) !== NULL) {
            $response = $this->mailer->send($message);
        } else {
            $response = 1;
        }


        return $response;
    }
}
