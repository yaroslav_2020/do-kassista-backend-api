<?php


namespace App\Application\Offer;

use Psr\Log\LoggerInterface;

/**
 * Class OfferService
 * @package App\Application\Offer
 */
class OfferService
{
    /** @var LoggerInterface */
    private $logger;

    /**
     * OfferService constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


}