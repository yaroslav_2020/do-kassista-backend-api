<?php

declare(strict_types=1);

namespace App\Application\Generator\Token;


use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

/**
 * Class TokenGenerator
 * @package App\Application\Generator\Token
 */
class RegisterTokenGenerator implements TokenGeneratorInterface
{

    /**
     * Generates a CSRF token.
     *
     * @return string The generated CSRF token
     * @throws \Exception
     */
    public function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }
}