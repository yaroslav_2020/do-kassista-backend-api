<?php

namespace App\Application\PostalCode;

use App\Entity\PostalCode;
use App\Repository\PostalCodeRepository;


class PostalService
{
    private $postalRepository;


    public function __construct(
        PostalCodeRepository $postalRepository    
    )
    {
        $this->postalRepository = $postalRepository;
    }


/**
 * @param string $title
 * @param string $code
 * @param string $zoneName
 * @param string $address
 * @return PostalCode
 * @throws ORMException
 * @throws OptimisticLockException
 */
public function addPostalCode(string $title, string $code, string $zoneName, string $address){

    $postalCode = new PostalCode();
    $postalCode->setTitle($title);
    $postalCode->setCode($code);
    $postalCode->setZoneName($zoneName);
    $postalCode->setAddress($address);
    

    $this->postalRepository->save($postalCode);

    return $postalCode;

}


/**
 * @param int $id
 * @return PostalCode
 * @throws ORMException
 * @throws OptimisticLockException
 */
public function removePostalCode(PostalCode $postal){

    $this->postalRepository->remove($postal );

    return "ok";

}

}