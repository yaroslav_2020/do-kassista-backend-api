<?php

declare(strict_types=1);

namespace App\Application\Intl;

/**
 * Class LocaleStackInterface
 * @package App\Application\Intl
 */
interface LocaleStackInterface
{
    /**
     * Push locale in stack and sync components
     *
     * @param string $locale
     */
    public function push(string $locale): void;


    /**
     * Removes current locale from stack and sync components which are locale dependent
     *
     * @return string|null
     */
    public function pop(): ?string;


    /**
     * @return string|null
     */
    public function top(): ?string;

    /**
     * Clear locale stack
     */
    public function clear(): void;


    /**
     * @return int
     */
    public function count();
}