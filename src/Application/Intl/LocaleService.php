<?php

declare(strict_types=1);

namespace App\Application\Intl;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\LocaleAwareInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class LocaleService
 * @package App\Application\Intl
 */
class LocaleService implements EventSubscriberInterface, LocaleStackInterface
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /** @var RouterInterface  */
    protected $router;

    /** @var \SplStack */
    protected $localeStack;

    /** @var string  */
    protected $fallbackLocale = "en";

    /**
     * RequestListener constructor.
     * @param TranslatorInterface $translator
     * @param RouterInterface $router
     */
    public function __construct(TranslatorInterface $translator, RouterInterface $router)
    {
        $this->translator = $translator;
        $this->router = $router;
        $this->localeStack = new \SplStack();
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                ['onKernelRequest', 10]
            ]
        ];
    }

    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        if ($event->getRequestType() !== HttpKernelInterface::MASTER_REQUEST) {
            // don't do anything if it's not the master request
            return;
        }

        $request = $event->getRequest();
        $params  = $request->attributes->get('_route_params');
        if (isset($params['locale'])) {
            $this->push($params['locale']);
        } else {
            $this->push($this->fallbackLocale);
        }
    }

    /**
     * Push locale in stack and sync components
     *
     * @param string $locale
     */
    public function push(string $locale): void
    {
        $this->localeStack->push($locale);
        if ($this->translator instanceof LocaleAwareInterface) {
            $this->translator->setLocale($locale);
        }
    }

    /**
     * Removes current locale from stack and sync components which are locale dependent
     *
     * @return string|null
     */
    public function pop(): ?string
    {
        if ($this->localeStack->count() === 0) {
            return null;
        }

        $oldLocale = $this->localeStack->pop();
        if ($this->localeStack->count() !== 0) {
            if ($this->translator instanceof LocaleAwareInterface) {
                $this->translator->setLocale($this->localeStack->current());
            }
        }

        return $oldLocale;
    }

    /**
     * @return string|null
     */
    public function top(): ?string
    {
        if ($this->localeStack->count() > 0) {
            return $this->localeStack->top();
        }

        return null;
    }

    /**
     * Clear locale stack
     */
    public function clear(): void
    {
        $this->localeStack = new \SplStack();
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->localeStack->count();
    }
}