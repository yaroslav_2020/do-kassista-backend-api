<?php


namespace App\Application\Voucher;

use App\Entity\Voucher;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class VoucherManager
 * @package App\Application\Voucher
 */
class VoucherManager
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * VoucherManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Voucher $voucher
     * @return Voucher
     */
    public function activateVoucher(Voucher $voucher): Voucher
    {
        $voucher->setActiveStatus(Voucher::STATUS_ACTIVE);
        $this->entityManager->persist($voucher);
        $this->entityManager->flush();

        return $voucher;
    }

    /**
     * @param Voucher $voucher
     * @return Voucher
     */
    public function deactivateVoucher(Voucher $voucher): Voucher
    {
        $voucher->setActiveStatus(Voucher::STATUS_INACTIVE);
        $this->entityManager->persist($voucher);
        $this->entityManager->flush();

        return $voucher;
    }

    /**
     * @param Voucher $voucher
     * @param \DateTime $start
     * @param \DateTime $end
     * @return Voucher
     */
    public function setValidityInterval(Voucher $voucher, \DateTime $start, \DateTime $end): Voucher
    {
        $voucher->setStartDate($start);
        $voucher->setEndDate($end);

        $this->entityManager->persist($voucher);
        $this->entityManager->flush();

        return $voucher;
    }

    /**
     * @param Voucher $voucher
     * @return Voucher
     * @throws \Exception
     */
    public function expireVoucher(Voucher $voucher): Voucher
    {
        $voucher->setStartDate(new \DateTime('now', new \DateTimeZone('UTC')));
        $voucher->setEndDate(new \DateTime('now', new \DateTimeZone('UTC')));
        $this->entityManager->persist($voucher);
        $this->entityManager->flush();

        return $voucher;
    }

    /**
     * @param Voucher $voucher
     * @return Voucher
     */
    public function increaseUsages(Voucher $voucher): Voucher
    {
        $voucher->setUsageCount($voucher->getUsageCount() + 1);
        $this->entityManager->persist($voucher);
        $this->entityManager->flush();

        return $voucher;
    }

    /**
     * @param Voucher $voucher
     * @return bool
     * @throws \Exception
     */
    public function canUse(Voucher $voucher): bool
    {
        return $voucher->isReadyToBeUsed();
    }

    /**
     * @param array $optionsArg
     * @return Voucher
     * @throws \Exception
     */
    public function factoryVoucher(array $optionsArg): Voucher
    {
        $optionResolver = self::getOptionResolver($this->entityManager);
        $options = $optionResolver->resolve($optionsArg);
        $voucher = new Voucher();

        return $this->doResolveProperties($voucher, $options);
    }

    /**
     * @param Voucher $voucher
     * @return Voucher
     * @throws \Exception
     */
    public function resolveProperties(Voucher $voucher): Voucher
    {
        $optionResolver = self::getOptionResolver($this->entityManager);
        $options = $optionResolver->resolve([
            'title' => $voucher->getTitle(),
            'type' => $voucher->getType(),
            'discount' => $voucher->getDiscount(),
            'active_status' =>$voucher->getActiveStatus(),
            'usage_type' => $voucher->getUsageType(),
            'start' => $voucher->getStartDate(),
            'end' => $voucher->getEndDate(),
        ]);

        return $this->doResolveProperties($voucher, $options);
    }

    /**
     * @param Voucher $voucher
     * @param array $options
     * @return Voucher
     */
    private function doResolveProperties(Voucher $voucher, array $options): Voucher
    {
        $voucher->setTitle($options['title']);
        $voucher->setCode($options['code']);

        if ($options['start'] instanceof \DateTime) {
            $voucher->setStartDate($options['start']);
        }
        if ($options['end'] instanceof \DateTime) {
            $voucher->setEndDate($options['end']);
        }

        $voucher->setDiscount(floatval($options['discount']));
        $voucher->setType($options['type']);
        $voucher->setActiveStatus($options['active_status']);
        $voucher->setUsageCount(0);
        $voucher->setUsageType($options['usage_type']);

        $this->entityManager->persist($voucher);
        $this->entityManager->flush();
        $this->entityManager->refresh($voucher);

        return $voucher;
    }

    /**
     * @param Voucher $voucher
     * @return Voucher
     */
    public function save(Voucher $voucher): Voucher
    {
        $this->entityManager->persist($voucher);
        $this->entityManager->flush();
        $this->entityManager->refresh($voucher);

        return $voucher;
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @return OptionsResolver
     * @throws \Exception
     */
    public static function getOptionResolver(EntityManagerInterface $entityManager)
    {
        $optionResolver = new OptionsResolver();
        $optionResolver->setDefaults([
            'title' => "VCF_" . self::factoryVoucherTitle(),
            'code'  => self::factoryVoucherCode($entityManager),
            'start' => null,
            'end'   => null,
            'type'  => Voucher::TYPE_FIXED,
            'discount' => null,
            'active_status' => 1,
            'usage_type' => Voucher::USAGE_TYPE_BY_ANY_CLIENT_ANY_TIME,
        ]);

        return $optionResolver;
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @return string
     */
    private static function factoryVoucherCode(EntityManagerInterface $entityManager)
    {
        while (true) {
            $code = "CODE_" . strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10));

            //check code is unique
            $voucher = $entityManager->getRepository(Voucher::class)->findUsingCode($code);
            if (!($voucher instanceof Voucher)) {
                return $code;
            }
        }
    }

    /**
     * @return string
     * @throws \Exception
     */
    private static function factoryVoucherTitle()
    {
        return bin2hex(random_bytes(10));
    }
}