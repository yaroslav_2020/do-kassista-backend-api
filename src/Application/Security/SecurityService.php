<?php

declare(strict_types=1);

namespace App\Application\Security;


use App\Application\Cors\WebsiteDomain;
use App\Application\Generator\Token\RegisterTokenGenerator;
use App\Application\Mailer\AuthMailer;
use App\Entity\AclAction;
use App\Entity\User;
use App\Entity\UserAclAction;
use App\Exception\Security\ExistingUserException;
use App\Exception\Security\PasswordNotMatchedException;
use App\Exception\Security\UserNotFoundException;
use App\Repository\UserAclActionRepository;
use App\Repository\UsersRepository;
use App\Repository\OfferRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception as ExceptionAlias;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWSProvider\JWSProviderInterface;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class SecurityService
 * @package App\Application\Security
 */
class SecurityService
{
    const AUTH_MODE_REGISTER_NO_CONFIRMATION = 'register_no_confirmation';
    const AUTH_MODE_RESET_PASSWORD_NO_CONFIRMATION = 'reset_password_no_confirmation';

    /** @var UsersRepository $userRepository */
    private $usersRepository;

    /** @var OfferRepository $userRepository */
    private $offerRepository;

    /** @var CanonicalFieldsUpdater  */
    private $canonicalFieldsUpdater;

    /** @var MailerInterface */
    private $mailer;

    /** @var RegisterTokenGenerator  */
    private $tokenGenerator;

    /** @var TranslatorInterface */
    private $translator;

    /** @var UserPasswordEncoderInterface */
    private $userPasswordEncoder;

    /** @var JWTEncoderInterface  */
    private $jwtEncoder;

    /** @var JWSProviderInterface */
    private $jwsProvider;

    /** @var LoggerInterface */
    private $logger;

    /** @var EntityManager */
    private $entityManager;

    /** @var PasswordUpdaterInterface */
    private $passwordUpdater;

    /**
     * AuthController Constructor
     *
     * @param UsersRepository $usersRepository
     * @param CanonicalFieldsUpdater $canonicalFieldsUpdater
     * @param AuthMailer $mailer
     * @param RegisterTokenGenerator $tokenGenerator
     * @param TranslatorInterface $translator
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     * @param JWTEncoderInterface $jwtEncoder
     * @param JWSProviderInterface $jwsProvider
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $entityManager
     * @param PasswordUpdaterInterface $passwordUpdater
     * @param WebsiteDomain $websiteDomain
     */
    public function __construct(
        UsersRepository $usersRepository,
        OfferRepository $offerRepository,
        CanonicalFieldsUpdater $canonicalFieldsUpdater,
        AuthMailer $mailer,
        RegisterTokenGenerator $tokenGenerator,
        TranslatorInterface $translator,
        UserPasswordEncoderInterface $userPasswordEncoder,
        JWTEncoderInterface $jwtEncoder,
        JWSProviderInterface $jwsProvider,
        LoggerInterface $logger,
        EntityManagerInterface $entityManager,
        PasswordUpdaterInterface $passwordUpdater,
        WebsiteDomain $websiteDomain
    ) {
        $this->usersRepository = $usersRepository;
        $this->offerRepository = $offerRepository;
        $this->canonicalFieldsUpdater = $canonicalFieldsUpdater;
        $this->tokenGenerator = $tokenGenerator;
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->jwtEncoder = $jwtEncoder;
        $this->jwsProvider = $jwsProvider;
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->passwordUpdater = $passwordUpdater;
        $this->websiteDomain = $websiteDomain;
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @param string $confirmPassword
     * @param integer $user_type
     * @param $register_mode
     * @return User
     * @throws ExistingUserException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws PasswordNotMatchedException
     */
    public function register(string $name, string $email, string $password, string $confirmPassword, int $user_type, $register_mode, string $phone_number): User
    {


        if ($password !== $confirmPassword) {
            throw new PasswordNotMatchedException("Password does not match");
        }


        $user = new User();
        $user->setEmail($email);
        $user->setUsername($email);
        $user->setName($name);
        $user->setPlainPassword($password);
        $user->setPhoneNumber($phone_number);
        $user->setUserHash();

        if ($user_type >= -1 && $user_type < count(User::USER_ROLE_LIST)) {
            if ($user_type > -1) {
                $user->addRole(User::USER_ROLE_LIST[$user_type]);
            } else {
                $user->addRole(User::USER_ROLE_LIST[1]);
            }
        }

        $confirmation = [];
        if (!$register_mode) {
            $confirmation['register_mode'] = self::AUTH_MODE_REGISTER_NO_CONFIRMATION;
        }

        return $this->registerUserObject($user, $confirmation);
    }

    /**
     * @param User $user
     * @param array $context
     * @return User
     * @throws ExistingUserException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function registerUserObject(User $user, array $context = []): User
    {
        $this->throwExceptionIfUserEmailExists($user->getEmail());

        $registerNoConfirmation = (array_key_exists('register_mode', $context)
            && $context['register_mode'] === self::AUTH_MODE_REGISTER_NO_CONFIRMATION);

        if ($registerNoConfirmation) {
            return $this->registerNoConfirmation($user, $context);
        } else {
            return $this->registerUsingConfirmation($user, $context);
        }
    }

    /**
     * @param User $user
     * @param array $context
     * @return User
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function registerUsingConfirmation(User $user, array $context = [])
    {
        $user->setEnabled(true);
        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
        }
        $this->usersRepository->save($user);
        $this->mailer->sendConfirmationEmailMessage($user);

        return $user;
    }

    /**
     * @param User $user
     * @param array $context
     * @return User
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function registerNoConfirmation(User $user, array $context = [])
    {
        $user->setEnabled(true);
        $user->setConfirmationToken(null);
        $this->usersRepository->save($user);

        return $user;
    }

    /**
     * @param string $email
     * @throws ExistingUserException
     */
    private function throwExceptionIfUserEmailExists(string $email)
    {
        $existingUser = $this->usersRepository->findOneBy(['email' => $email]);
        if ($existingUser instanceof User) {
            throw new ExistingUserException("User with same email address " . $email . " found in database");
        }
    }

    /**
     * Receive the confirmation token from user email provider, login the user.
     *
     * @param string $token
     *
     * @return void
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function confirmNewUser($token)
    {
        $user = $this->usersRepository->findUserByConfirmationToken($token);
        if (!($user instanceof User)) {
            throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
        }

        $user->setConfirmationToken(null);
        $user->setEnabled(true);

        $this->usersRepository->save($user);
    }

    /**
     * @param string $email
     * @throws ExceptionAlias
     */
    public function requestResetPassword(string $email)
    {
        $user = $this->usersRepository->findUserByEmail($email);
        if (!($user instanceof User)) {
            throw new UserNotFoundException("User not found using email $email");
        }
        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
        }

        $user->setPasswordRequestedAt(new \DateTime());
        $this->usersRepository->save($user);

        $this->mailer->sendResettingEmailMessage($user);
    }

    /**
     * @param string $token
     * @throws UserNotFoundException
     */
    public function authenticateRequestPassword(string $token)
    {
        $user = $this->usersRepository->findUserByConfirmationToken($token);
        if (!($user instanceof User)) {
            throw new UserNotFoundException("User not found using token $token");
        }
    }

    /**
     * @param string $password
     * @param string $passwordConfirmed
     * @param string $token
     * @param array $context
     * @return User
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws PasswordNotMatchedException
     * @throws UserNotFoundException
     */
    public function doResetPassword(
        string $password,
        string $passwordConfirmed,
        string $token = null,
        array $context = []
    ): User {

        if ($password !== $passwordConfirmed) {
            throw new PasswordNotMatchedException("Password does not match");
        }

        $user = $this->usersRepository->findUserByConfirmationToken($token);
        if (!($user instanceof User)) {
            throw new UserNotFoundException("User not found using token $token");
        }

        //remove confirmation password token only if user is not confirmed
        if ($user->isEnabled()) {
            $user->setConfirmationToken(null);
        }

        $user->setPlainPassword($password);

        $this->usersRepository->save($user);

        return $user;
    }

    /**
     * @param User $user
     * @param string $newPassword
     * @param string $confirmedPassword
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws PasswordNotMatchedException
     */
    public function changePassword(User $user, string $newPassword, string $confirmedPassword)
    {
        if ($newPassword !== $confirmedPassword) {
            // return new JsonResponse(["error" => "Password does not match"]);
            throw new PasswordNotMatchedException("Password does not match");
        }
        $user->setPlainPassword($newPassword);
        $this->passwordUpdater->hashPassword($user);
        $this->usersRepository->save($user);
    }

    /**
     * @param string $email
     * @param string $password
     * @return string
     * @throws UserNotFoundException
     * @throws JWTEncodeFailureException
     */
    public function login(string $email, string $password)
    {
       

        $user = $this->usersRepository->findUserByEmail($email);
 
        $userName = $user->getUsername();
        

         if (!($user instanceof User)) {
             throw new UserNotFoundException("Your email or password may be incorrect");
         }
        $isValid = $this->userPasswordEncoder->isPasswordValid($user, $password);

        
         if ($isValid == false) {
             throw new BadCredentialsException("Your email or password may be incorrect");
         }
        $encode =  $this->jwtEncoder->encode([
            'username' => $userName,
            'exp' => time() + (3600 * 24 * 30) // one month hour expiration
        ]);        

        return $encode;
    }

    /**
     * @param User $user
     * @param string $password
     * @return boolean
     * @throws UserNotFoundException
     * @throws JWTEncodeFailureException
     */
    public function checkPassword(User $user, string $password)
    {
        $isValid = $this->userPasswordEncoder->isPasswordValid($user, $password);

        return $isValid;
    }

    /**
     * @param string $offerHash
     * @param string $userHash
     * @return string
     * @throws UserNotFoundException
     * @throws JWTEncodeFailureException
     */
    public function generateConfirmationLinkToOffers(string $offerHash, string $userHash)
    {
        $user = $this->usersRepository->findOneBy(['userHash' => $userHash]);
        if (!($user instanceof User)) {
            throw new Exception("User not found for id $userHash");
        }
        return $this->websiteDomain->generateWorkerEmailUrl(
            'worker_confirm_offer',
            [
                'o' => $offerHash,
                'u' => $userHash
            ]
        );
    }

    /**
     * Return user if token is valid
     *
     * @param string $token
     * @return User|null
     */
    public function accessToken(string $token): ?User
    {
        try {
            $jws = $this->jwsProvider->load($token);
            if ($jws->isInvalid()) {
                $this->logger->debug("invalid token, token is invalid");
                return null;
            }

            if ($jws->isExpired()) {
                $this->logger->debug("invalid token, token is expired");
                return null;
            }

            if ($jws->isVerified()) {
                $this->logger->debug("token is verified, all good");
                $payload = $jws->getPayload();
                if (empty($payload['username'])) {
                    $this->logger->error("Username not found in payload, token verified");
                    return null;
                }
                /** @var User $user */
                $user = $this->getUserRepository()->findUserByEmail($payload['username']);
                if (!($user instanceof User)) {
                    $this->logger->error("invalid token, user not found using username " . $payload['username']);
                    return null;
                }

                return $user;
            }


            $this->logger->error("token not verified, seams like valid and not expired!");
        } catch (\Throwable $throwable) {
            if ($throwable instanceof JWTDecodeFailureException) {
                $reason = $throwable->getReason();
                $this->logger->error("Token loading error, $reason, msg: " . $throwable->getMessage());
            }
        }

        return null;
    }

    /**
     * @return UsersRepository
     */
    protected function getUserRepository(): UsersRepository
    {
        return $this->entityManager->getRepository(User::class);
    }

    /**
     * Logout user, expire or remove token
     */
    public function logout()
    {
        //todo create logout request
    }

    /**
     * @param User $user
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function grandAllActionsToUser(User $user): void
    {
        $actions = $this->entityManager->getRepository(AclAction::class)->findAll();
        foreach ($actions as $action) {
            $this->grandActionToUser($user, $action);
        }
    }

    /**
     * @param User $user
     * @param AclAction $action
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function grandActionToUser(User $user, AclAction $action)
    {
        $actionGranted = $this->entityManager->getRepository(UserAclAction::class)->findOneBy([
            'user' => $user,
            'aclAction' => $action
        ]);
        if ($actionGranted instanceof UserAclAction) {
            return;
        }

        $actionGranted = new UserAclAction();
        $actionGranted->setUser($user);
        $actionGranted->setAclAction($action);

        $this->entityManager->persist($actionGranted);
        $this->entityManager->flush();
    }

    /**
     * @return array
     */
    public static function getAclListFixture(): array
    {
        return [
            "Vouchers" => [
                ["Voucher::collection::get", "View all vouchers"],
                ["Voucher::collection::new", "Create new voucher"],
                ["Voucher::item::get", "View voucher info"],
                ["Voucher::item::put", "Edit voucher"],
                ["Voucher::item::delete", "Delete vouchers"]
            ],
            "Access Control List" => [
                ["User::item::manage_acl_actions", "Edit user access control list"],
                ["User::item::view_acl_actions", "View user access control list"],
            ]
        ];
    }

    /**
     * @return AclAction[]|array|object[]
     */
    public function getAclActions(): array
    {
        return $this->entityManager->getRepository(AclAction::class)->findAll();
    }

    /**
     * @param User $user
     * @param array $actions
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateUserActions(User $user, array $actions)
    {
        //first delete all rights to make sure nothing will remain
        $this->deleteAllUserActions($user);

        //add new rights
        /** @var AclAction $action */
        foreach ($actions as $action) {
            $this->grandActionToUser($user, $action);
        }
    }

    /**
     * @param User $user
     */
    public function deleteAllUserActions(User $user)
    {
        /** @var UserAclActionRepository $userAclActionRepo */
        $userAclActionRepo = $this->entityManager->getRepository(UserAclAction::class);
        $userAclActionRepo->deleteAllForUser($user);
    }

    public function getRoles()
    {
        return $this->roles;
    }
}
