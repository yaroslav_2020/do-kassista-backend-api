<?php

declare(strict_types=1);

namespace App\Application\Security;

use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

/**
 * Class AclRequestListener
 * @package App\Application\Security
 */
class AclRequestListener implements EventSubscriberInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /** @var RouterInterface  */
    protected $router;

    /** @var Security */
    protected $security;

    /**
     * AclRequestListener constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param RouterInterface $router
     * @param Security $security
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        RouterInterface $router,
        Security $security
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
        $this->security = $security;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                ['onKernelRequest']
            ]
        ];
    }

    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        if ($event->getRequestType() !== HttpKernelInterface::MASTER_REQUEST) {
            // don't do anything if it's not the master request
            return;
        }

        $routeCollection = $this->router->getRouteCollection();
        $request = $event->getRequest();
        $routeName = $request->attributes->get("_route");
        $route = $routeCollection->get($routeName);
        if (!($route instanceof Route)) {
            return;
        }

        if (!($this->routeHasMetadata($route))) {
            return;
        }

        $actionId = $this->getActionId($route);
        if ($actionId === null) {
            return;
        }

        $user = $this->getUser();
        if (!($user instanceof User)) {
            return;
        }

        $hasAccess = $this->userIsGranted($user, $actionId);
        if (!$hasAccess) {
            throw new AccessDeniedException("User does not have access to $actionId");
        }
    }

    /**
     * @param User $user
     * @param string $actionId
     * @return bool
     */
    private function userIsGranted(User $user, string $actionId)
    {
        if ($this->security->isGranted("ROLE_SUPER_ADMIN")
            || ($this->security->isGranted("ROLE_ADMIN"))
            || ($this->security->isGranted("ROLE_USER"))
        ) {
            return true;
        }

        return $user->hasAccessToAction($actionId);
    }

    /**
     * @return object|string|null
     */
    private function getUser(): ?User
    {
        $token = $this->tokenStorage->getToken();
        if ($token instanceof TokenInterface) {
            $user = $token->getUser();
            if ($user instanceof User) {
                return $user;
            }
        }

        return null;
    }

    /**
     * @param Route $route
     * @return string|null
     */
    private function getActionId(Route $route): ?string
    {
        $metadata = $route->getOption('metadata');
        return empty($metadata['action_id']) || !is_string($metadata['action_id']) ? null : $metadata['action_id'];
    }

    /**
     * @param Route $route
     * @return bool
     */
    private function routeHasMetadata(Route $route): bool
    {
        return !empty($route->getOption("metadata"));
    }
}