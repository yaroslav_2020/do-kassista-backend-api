<?php

declare(strict_types=1);

namespace App\Application\Cors;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


/**
 * Class WebsiteDomain
 * @package App\Application\Cors
 */
class WebsiteDomain implements EventSubscriberInterface
{
    /**
     * @var string|null
     */
    private $requestSchemaAndHttpHost;

    /** @var string */
    private $originSchemeAndHost;

    /**
     * @var UrlGeneratorInterface
     */
    protected $router;

    /**
     * WebsiteDomain constructor.
     * @param UrlGeneratorInterface $router
     */
    public function __construct(
        UrlGeneratorInterface $router
    ){
        $this->router = $router;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                ['onKernelRequest', 10]
            ]
        ];
    }

    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        if ($event->getRequestType() !== HttpKernelInterface::MASTER_REQUEST) {
            // don't do anything if it's not the master request
            return;
        }

        if (!empty($event->getRequest()->headers->has('origin'))) {
            $this->originSchemeAndHost = $event->getRequest()->headers->get('origin');
        }
        $this->requestSchemaAndHttpHost = $event->getRequest()->getSchemeAndHttpHost();

        if (empty($this->originSchemeAndHost)) {
            $this->originSchemeAndHost = $this->requestSchemaAndHttpHost;
        }
    }

    /**
     * @return string|null
     */
    public function getRequestSchemaAndHttpHost(): ?string
    {
        return $this->requestSchemaAndHttpHost;
    }

    /**
     * @param string|null $requestSchemaAndHttpHost
     */
    public function setRequestSchemaAndHttpHost(?string $requestSchemaAndHttpHost): void
    {
        $this->requestSchemaAndHttpHost = $requestSchemaAndHttpHost;
    }

    /**
     * @return string
     */
    public function getOriginSchemeAndHost(): string
    {
        return $this->originSchemeAndHost;
    }

    /**
     * @param string $originSchemeAndHost
     */
    public function setOriginSchemeAndHost(string $originSchemeAndHost): void
    {
        $this->originSchemeAndHost = $originSchemeAndHost;
    }

    /**
     * @param string $routName
     * @param array $parameters
     * @return string
     */
    public function generateEmailUrl(string $routName, array $parameters)
    {
        $origin = $this->getEmailLinkSchemeAndHost();
        $urlRelative = $this->router->generate(
            $routName,
            $parameters,
            UrlGeneratorInterface::ABSOLUTE_PATH
        );

        return $origin . $urlRelative;
    }
//TO CHECK
    /**
     * @param string $routName
     * @param array $parameters
     * @return string
     */
    public function generateWorkerEmailUrl(string $routName, array $parameters)
    {
        $origin = $this->getRequestSchemaAndHttpHost();
        $urlRelative = $this->router->generate(
            $routName,
            $parameters,
            UrlGeneratorInterface::ABSOLUTE_PATH
        );

        return $origin . $urlRelative;
    }

    /**
     * @return string|null
     */
    public function getEmailLinkSchemeAndHost()
    {
        if (empty($this->originSchemeAndHost)) {
            return $this->requestSchemaAndHttpHost;
        }

        return $this->originSchemeAndHost;
    }
}