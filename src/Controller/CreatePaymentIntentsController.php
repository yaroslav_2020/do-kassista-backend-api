<?php

namespace App\Controller;

use App\Repository\UsersRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class CreatePaymentIntentsController extends AbstractController
{

    private $stripe;
    private $usersRepository;
    private $logger;

    public function __construct(
        StripeController $stripeController,
        UsersRepository $usersRepository,
        LoggerInterface $logger

    )
    {
        $this->stripe = $stripeController;
        $this->stripe->setStripeKey($_ENV['STRIPE_KEY']);
        $this->usersRepository = $usersRepository;
        $this->logger = $logger;
    }

    public function generateStripeSetupIntent(Request $request): JsonResponse
    {
        $data = @json_decode($request->getContent(), true);

        $client = $this->usersRepository->findUserByEmail($data['customer']);

        //VERIFY IF THE USER HAS A STRIPE COSTUMER ID ON THE DATABASE
        //IF THE USER DON'T HAVE A CUSTOMER ID, CREATE ONE
        $verify = $this->stripe->verifyUserInStripe($client->getCustomerStripeId(),$client->getEmail());

        if($verify){
            $this->logger->info('[NEW OFFER::START]-------------Creating Customer');
            $customerId = $this->stripe->createCustomer($client->getEmail(),$client->getName());
            $client->setCustomerStripeId($customerId);
            $em = $this->getDoctrine()->getManager();
            $em->persist($client);
            $em->flush();

        }else{
            $this->logger->info("[NEW OFFER::START]-------------Don't Create the customer");
        }

        $data['customer'] = $client->getCustomerStripeId();

        $intent = $this->stripe->setupIntent($data);

        return new JsonResponse($intent);
    }

}
