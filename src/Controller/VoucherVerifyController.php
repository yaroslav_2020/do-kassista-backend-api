<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Voucher;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Request;
use App\Helpers\ArrayAssert;
use App\Exception\ArrayAssert\ArrayAssertException;
use App\Helpers\IntTypeAssert;
use App\Exception\IntTypeAssert\IntTypeAssertException;
use App\Application\Security\SecurityService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Log\Logger;
use App\Entity\User;

class VoucherVerifyController extends AbstractController
{
    /**
     * @Route("/voucher/verify", name="voucher_verify")
     */
    public function index()
    {
        return $this->render('voucher_verify/index.html.twig', [
            'controller_name' => 'VoucherVerifyController',
        ]);
    }

    /**
     * @return User
     */

    public function __invoke(LoggerInterface $logger,SecurityService $securityService,SerializerInterface $serializer,Request $request) 
    {        
        try {
            
            $search = @json_decode($request->getContent(),true);
            
            if(!empty($search["id"]) && IntTypeAssert::validateOrException($search["id"])){
                $verify = $this->verifyVoucher($securityService,$request,$serializer,"id",$search["id"]);
            }else if(ArrayAssert::validateOrException($search)->hasNonEmptyKey('code')){
                $verify = $this->verifyVoucher($securityService,$request,$serializer,"code",$search["code"]);
            }

        } catch (ArrayAssertException $arrayAssertException) {
            $logger->error("Error while registering new user, invalid data: " . $arrayAssertException->getMessage());

            return new JsonResponse(["error" => ApiResponseCodesInterface::ERR_INVALID_DATA], 400);
        } catch (IntTypeAssertException $intTypeAssertException) {
            $logger->error("Error while registering new user, invalid type data: " . $intTypeAssertException->getMessage());

            return new JsonResponse(["error" => ApiResponseCodesInterface::IVALID_TYPE], 400);
        }

        
        return $verify;

        
    }

    public function verifyVoucher(SecurityService $securityService,Request $request,SerializerInterface $serializer,$key,$value)
    {
        $token = $request->headers->get('Authorization');

        $voucher = $this->getDoctrine()->getRepository(Voucher::class)->findOneBy([$key => $value]);

        if(!($voucher instanceof Voucher)){
            return new JsonResponse([
                "result" => "Voucher not found",
                "translation_response" => "notFound"
            ]);
        }       

        if(empty($token)){
            $loggedIn = false;
        }else{
            $user = $securityService->accessToken(explode(' ',$token)[1]);
            if(!($user instanceof User)){
                $loggedIn = false;
            }else{
                $loggedIn = true;
            }            
        }        

        if($loggedIn && ($voucher->getUser() == $user)){
            $owner = true;
        }else{
            $owner = false;
        }

                 
        if (($voucher->getUsageType() === Voucher::USAGE_ONE_TIME_BY_SPECIFIC_CLIENT) && !$owner){
            return new JsonResponse([
                "result" => "This voucher is not assigned to this user",
                "translation_response" => "notAvailable"
            ]);
        } else if ( ($voucher->getUsageType() === Voucher::USAGE_ONE_TIME_BY_SPECIFIC_CLIENT) && !$loggedIn) {
            return new JsonResponse([
                "result" => "This voucher requires an owner. No user provided",
                "translation_response" => "notAvailable"
            ]);
        } else if (!$loggedIn) {
            return new JsonResponse([
                "result" => "This voucher requires an user. No user provided",
                "translation_response" => "notAvailable"
            ]);
        }

        $verify = $voucher->isReadyToBeUsed() ? 
                            json_decode($serializer->serialize(
                                $voucher,
                                'json', ['groups' => 'admin:read']
                            ))
                            : $voucher->isReadyToBeUsed();

        return new JsonResponse([
            "UserLoggedIn" => $loggedIn,
            "UserOwner" => $owner,
            "VoucherVerify" => $verify
        ]);
    }
}