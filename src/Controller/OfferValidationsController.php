<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Repository\SettingsRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Settings;
use App\Controller\OfferFullScheduleController;
use Psr\Log\LoggerInterface;

class OfferValidationsController
{
    private $settingRepository;

    private $offerFullScheduleController;

    private $logger;

    private $errorMessage;

    public function __construct(
        SettingsRepository $settingRepository,
        OfferFullScheduleController $offerFullScheduleController,
        LoggerInterface $logger
    ) {
        $this->settingRepository = $settingRepository;
        $this->offerFullScheduleController = $offerFullScheduleController;
        $this->logger = $logger;
        $this->errorMessage = '';
    }

    public function validate(Offer $offer)
    {
        $startHour = $offer->getPeriodPoints()[0]->getStartHour();
        $endHour = $offer->getPeriodPoints()[0]->getEndHour();

        $hoursNeeded = $this->calculateHoursNeeded($startHour, $endHour);

        $pricePerHour = $offer->getPricePerDay() / $hoursNeeded;

        if (
            !$this->validateIntervalDateService($offer->getStartFromDate(), $offer->getEndDate()) ||
            !$this->validateHoursNeeded($hoursNeeded) ||
            !$this->validateStartFromDate($offer->getStartFromDate()) ||
            !$this->validatePastDate($offer->getStartFromDate()) ||
            !$this->validatePastDate($offer->getEndDate()) ||
            !$this->validateIntervalHoursWithLaborSchedule($startHour, $endHour) ||
            !$this->validateStartHourWithHoursNeeded($startHour, $hoursNeeded) ||
            !$this->validatePrice($offer->getPricePerService()) ||
            !$this->validatePrice($offer->getPricePerDay()) ||
            !$this->validatePricePerHour($pricePerHour) ||
            !$this->validateFrecuency($offer->getStartFromDate(), $offer->getEndDate(), $offer->getPeriod(), $offer->getPeriodPoints(), $startHour, $endHour)
        ) {
            return [
                "verify" => false,
                "errorMessage" => $this->errorMessage,
            ];
        }

        return [
            "verify" => true,
            "errorMessage" => ''
        ];
    }

    private function validateStartFromDate($startFromDate)
    {
        if ($startFromDate instanceof \DateTime) {
            $startDate = new \DateTime($startFromDate->format('Y-m-d 0:0:0'), new \DateTimeZone("UTC"));
            $today = new \DateTime('now', new \DateTimeZone("UTC"));
            $today->setTime(0, 0, 0);

            if ($startDate > $today)
                return true;
        }

        $this->logger->error('StartFromDate can not be today');
        $this->errorMessage = 'StartFromDate can not be today';

        return false;
    }

    private function validatePastDate($date)
    {
        if ($date instanceof \DateTime) {
            $date->setTime(0, 0, 0);
            $today = new \DateTime('now', new \DateTimeZone("UTC"));

            $today->setTime(0, 0, 0);

            if ($date >= $today)
                return true;
        }

        $this->logger->error('Dates can not be in the past');
        $this->errorMessage = 'Dates can not be in the past';

        return false;
    }

    private function validateIntervalDateService($startFromDate, $endDate)
    {
        if ($startFromDate instanceof \DateTime && $endDate instanceof \DateTime) {
            $startFromDate->setTime(0, 0, 0);
            $endDate->setTime(0, 0, 0);
            if ($startFromDate < $endDate) {
                return true;
            }
        }

        $this->logger->error('Offer start date is bigger than end Date');
        $this->errorMessage = 'Offer start date is bigger than end Date';
        return false;
    }

    private function validateHoursNeeded($hoursNeeded)
    {

        $minHours = NULL !== ($this->settingRepository->getSetting(Settings::MIN_HOURS)) ? 2 : 1;
        $maxHours = NULL !== $this->settingRepository->getSetting(Settings::MAX_HOURS);

        if ($hoursNeeded >= floatval($minHours) && $hoursNeeded < floatval($maxHours))
            return true;

        $this->logger->error('Service Hour interval is not between the settings interval');
        $this->errorMessage = 'Service Hour interval is not between the settings interval';
        return false;
    }

    private function validateIntervalHoursWithLaborSchedule($startHour, $endHour)
    {
        $startServiceHour = $this->offerFullScheduleController->createDateTimeToCompare($startHour);
        $endServiceHour = $this->offerFullScheduleController->createDateTimeToCompare($startHour);

        $startLaborScheduleSetting = $this->settingRepository->getSetting(Settings::START_LABOR_SCHEDULE);
        $endLaborScheduleSetting = $this->settingRepository->getSetting(Settings::END_LABOR_SCHEDULE);

        $startLaborHour = $this->offerFullScheduleController->createDateTimeToCompare($startLaborScheduleSetting->getValue());
        $endLaborHour = $this->offerFullScheduleController->createDateTimeToCompare($endLaborScheduleSetting->getValue());

        if ($startServiceHour >= $startLaborHour && $endServiceHour < $endLaborHour)
            return true;

        $this->logger->error('Service hours interval is not between the labor interval');
        $this->errorMessage = 'Service hours interval is not between the labor interval';
        return false;
    }

    private function validateStartHourWithHoursNeeded($startHour, $hoursNeeded)
    {
        $startServiceHour = $this->offerFullScheduleController->createDateTimeToCompare($startHour);

        $startServiceHourWithHoursNeeded = $this->offerFullScheduleController->incrementHours($startServiceHour, $hoursNeeded);
        $endLaborScheduleSetting = $this->settingRepository->getSetting(Settings::END_LABOR_SCHEDULE);

        $endLaborHour = $this->offerFullScheduleController->createDateTimeToCompare($endLaborScheduleSetting->getValue());

        if ($startServiceHourWithHoursNeeded < $endLaborHour)
            return true;

        $this->logger->error('Start service hour plus hours needed is bigger than the end labor schedule');
        $this->errorMessage = 'Start service hour plus hours needed is bigger than the end labor schedule';
        return false;
    }

    private function validatePrice($price)
    {
        if (floatval($price) > 0)
            return true;

        $this->logger->error('Price can not be lower or equal than 0');
        $this->errorMessage = 'Price can not be lower or equal than 0';
        return false;
    }

    private function validatePricePerHour($pricePerHour)
    {
        $minPricePerHourSetting = $this->settingRepository->getSetting(Settings::MIN_PRICE_PER_HOUR);
        $maxPricePerHourSetting = $this->settingRepository->getSetting(Settings::MAX_PRICE_PER_HOUR);

        $pricePerHour = floatval($pricePerHour);

        if ($pricePerHour >= floatval($minPricePerHourSetting->getValue()) && $pricePerHour <= floatval($maxPricePerHourSetting->getValue()))
            return true;


        $this->logger->error('ENTER ON THE VALIDATION AND FAILS');
        $this->logger->error('Price per hour is not in the settings interval');
        $this->errorMessage = 'Price per hour is not in the settings interval';
        return false;
    }

    private function validateFrecuency($startFromDate, $endDate, $frecuency, $periodPoints, $startHour, $endHour)
    {

        $daysData = $this->offerFullScheduleController->handlePeriodPointsDays($periodPoints);
        $schedule = $this->offerFullScheduleController->calculateSchedule($startFromDate, $endDate, $daysData, $startHour, $endHour);

        if ($frecuency !== Offer::PERIOD_ONLY_ONCE && count($schedule) >= 2)
            return true;

        if ($frecuency === Offer::PERIOD_ONLY_ONCE && count($schedule) === 1)
            return true;

        $this->logger->error('Not enough iteratios for this frecuency');
        $this->errorMessage = 'Not enough iteratios for this frecuency';
        return false;
    }

    private function calculateHoursNeeded($startHour, $endHour)
    {
        $startServiceHour = $this->offerFullScheduleController->createDateTimeToCompare($startHour);
        $endServiceHour = $this->offerFullScheduleController->createDateTimeToCompare($endHour);

        $interval = $endServiceHour->diff($startServiceHour);

        $hoursNeeded = floatval($interval->format('%H')) + (floatval($interval->format('%i')) / 60);

        return $hoursNeeded;
    }
}
