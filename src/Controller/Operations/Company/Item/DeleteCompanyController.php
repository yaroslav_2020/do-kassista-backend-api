<?php

namespace App\Controller\Operations\Company\Item;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DeleteCompanyController extends AbstractController
{
    /**
     * @Route("/delete/company", name="delete_company")
     */
    public function index()
    {
        return $this->render('delete_company/index.html.twig', [
            'controller_name' => 'DeleteCompanyController',
        ]);
    }

    public function __invoke($data)
    {        
        return $data;        
    }
}
