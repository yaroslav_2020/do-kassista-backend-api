<?php

declare(strict_types=1);

namespace App\Controller\Operations\Voucher\Collection;

use App\Application\Voucher\VoucherManager;
use App\Entity\Voucher;

/**
 * Class PostVoucherController
 * @package App\Controller\Operations\Voucher\Collection
 */
class PostVoucherController
{
    /** @var VoucherManager */
    protected $voucherManager;

    /**
     * PostVoucherController constructor.
     * @param VoucherManager $voucherManager
     */
    public function __construct(VoucherManager $voucherManager)
    {
        $this->voucherManager = $voucherManager;
    }

    /**
     * @param Voucher $data
     * @return Voucher
     * @throws \Exception
     */
    public function __invoke(Voucher $data): Voucher
    {
        /*
         * Factory some props like code in an automatic mode
         */
        return $this->voucherManager->resolveProperties($data);
    }
}