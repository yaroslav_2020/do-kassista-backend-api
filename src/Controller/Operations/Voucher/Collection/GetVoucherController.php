<?php

declare(strict_types=1);

namespace App\Controller\Operations\Voucher\Collection;

use App\Entity\Voucher;

/**
 * Class GetVoucherController
 * @package App\Controller\Operations\Voucher\Collection
 */
class GetVoucherController
{
    /**
     * GetVoucherController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param Voucher $data
     * @return Voucher
     * @throws \Exception
     */
    public function __invoke($data)
    {
        return $data;
    }
}