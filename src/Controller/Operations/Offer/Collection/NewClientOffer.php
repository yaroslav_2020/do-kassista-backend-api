<?php


namespace App\Controller\Operations\Offer\Collection;

use App\Entity\Offer;
use App\Entity\User;
use App\Entity\Voucher;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Application\Mailer\AuthMailer;
use FOS\UserBundle\Mailer\MailerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Application\Security\SecurityService;
use App\Entity\EmailConfirmation;
use Psr\Log\LoggerInterface;
use App\Controller\OfferValidationsController;

/**
 * Class NewClientOffer
 * @package App\Controller\Operations\Offer\Collection
 */
class NewClientOffer extends AbstractController
{

    /** @var MailerInterface */
    private $mailer;

    private $offerValidationsController;

    /**
     * NewClientOffer constructor.
     */
    public function __construct(
        OfferValidationsController $offerValidationsController
    ){
        $this->offerValidationsController = $offerValidationsController;
    }

    /**
     * @param Request $request
     * @param AuthMailer $mailer
     */
    public function __invoke(
        SecurityService $securityService,
        LoggerInterface $logger,
        Request $request,
        AuthMailer $mailer,
        Offer $data)
    {
        $postData = json_decode($request->getContent());


        $verify = $this->offerValidationsController->validate($data);

        if($verify['verify']){
            $data->setOfferHash();
            $client = $data->getClient()[0];
            $client->setActivePaymentMethod($postData->activePaymentMethod);
            $em = $this->getDoctrine()->getManager();
            $em->persist($client);
            $em->flush();
            return $data;
        }else{
            return new JsonResponse(['error' => $verify['errorMessage']]);
        }        
    }
}