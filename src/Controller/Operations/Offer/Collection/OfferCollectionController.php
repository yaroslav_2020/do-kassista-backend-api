<?php

namespace App\Controller\Operations\Offer\Collection;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Application\Security\SecurityService;
use Symfony\Component\Serializer\SerializerInterface;
use App\Entity\User;
use App\Entity\Offer;

class OfferCollectionController extends AbstractController
{
    /**
     * @Route("/offer/collection", name="offer_collection")
     */
    public function index()
    {
        return $this->render('offer_collection/index.html.twig', [
            'controller_name' => 'OfferCollectionController',
        ]);
    }

    public function __invoke( SecurityService $securityService, Request $request, $data )
    {        
         $token = $request->headers->get('Authorization');

         $user = $securityService->accessToken(explode(' ',$token)[1]);
            
     if(in_array(User::ROLE_ADMIN, $user->getRoles(), true) || in_array(User::ROLE_COMERCIAL, $user->getRoles(), true)){
         return $data;
         }else{

             $offers = $this->getDoctrine()->getRepository(Offer::class)->findBy(["deleted" => false]);

             return $offers;
            
         }
        return $data;
        
    }   
}
