<?php

namespace App\Controller\Operations\Offer\Item;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Application\Security\SecurityService;
use Symfony\Component\Serializer\SerializerInterface;
use App\Entity\User;

class DeleteOfferController extends AbstractController
{
    /**
     * @Route("/delete/offer", name="delete_offer")
     */
    public function index()
    {
        return $this->render('delete_offer/index.html.twig', [
            'controller_name' => 'DeleteOfferController',
        ]);
    }

    public function __invoke(SecurityService $securityService,Request $request,$data)
    {        
        $token = $request->headers->get('Authorization');

        $user = $securityService->accessToken(explode(' ',$token)[1]);
            
        if(in_array(User::ROLE_ADMIN, $user->getRoles(), true) || in_array(User::ROLE_COMERCIAL, $user->getRoles(), true)){
            return $data;
        }else{
            return new JsonResponse(["permission_denied" => "You don't have permission to do this"]);
        }
        
    }
}
