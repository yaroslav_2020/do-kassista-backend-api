<?php

namespace App\Controller\Operations\Offer\Item;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\User;
use App\Entity\Offer;
use App\Application\Mailer\AuthMailer;
use FOS\UserBundle\Mailer\MailerInterface;
use App\Entity\EmailConfirmation;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Application\Cors\WebsiteDomain;


class ConfirmOfferWorkerController extends AbstractController
{
    public function confirmWorker(WebsiteDomain $url,AuthMailer $mailer,Request $request){

        if(empty($request->query->get('o')) || empty($request->query->get('u')))
            return new JsonResponse("Invalid data");

        $offer = $this->getDoctrine()->getRepository(Offer::class)->findOneBy(["offerHash" => $request->query->get('o')]);
        if(!($offer instanceof Offer)){
            return new JsonResponse("Offer not found");
        }
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(["userHash" => $request->query->get("u")]);
        if(!($user instanceof User)){
            return new JsonResponse("User not found");
        }
        
        $assignedWorker = $offer->getAssignedWorker();

        if($assignedWorker instanceof User){
            return new RedirectResponse("/error/page?u=".$user->getName());
        }else{
            $offer->setAssignedWorker($user);
            $this->getDoctrine()->getRepository(Offer::class)->update($offer);
            $status = $mailer->sendCongratulationsToAssignedWorker($offer,$user);
            $this->getDoctrine()->getRepository(EmailConfirmation::class)->save($offer,$user,"OFFER_WORKER",$status);
            return new RedirectResponse("/congratulations/page?w=".$user->getName()."&c=".$offer->getClient()[0]->getName());
//            return new JsonResponse("Offer Confirmed");

//            return new RedirectResponse(
//                $url->getOriginSchemeAndHost()
//            );
        }
    }

    /**
     * @Route("/error/page")
     */
    public function renderErrorConfirmPage(Request $request){
        if(empty($request->query->get('u')))
            return new JsonResponse("Invalid data");

        return $this->render('email/offer/error_page/error_page.html.twig', [
            'worker' => $request->query->get('u'),
            'logo' => dirname(__FILE__, 5) . '/templates/logo.png'
        ]);
    }

    /**
     * @Route("/congratulations/page")
     */
    public function renderCongratulationsConfirmPage(Request $request){
        if(empty($request->query->get('c')) || empty($request->query->get('w')))
            return new JsonResponse("Invalid data");

        return $this->render('email/offer/congratulation_page/congratulation_page.html.twig', [
            'worker' => $request->query->get('w'),
            'client' => $request->query->get('c'),
            'logo' => dirname(__FILE__, 5) . '/templates/logo.png'
        ]);
    }
}
