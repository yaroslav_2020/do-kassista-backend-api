<?php
namespace App\Controller\Operations\Address\Item;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DeleteAddressController extends AbstractController
{
    /**
     * @Route("/delete/address", name="delete_address")
     */
    public function index()
    {
        return $this->render('delete_address/index.html.twig', [
            'controller_name' => 'DeleteAddressController',
        ]);
    }

    public function __invoke($data)
    {        
        return $data;        
    }
}
