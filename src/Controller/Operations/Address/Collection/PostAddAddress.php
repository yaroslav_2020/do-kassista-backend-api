<?php


namespace App\Controller\Operations\Address\Collection;

use App\Entity\Address;
use App\Helpers\AddressTypeAssert;
use App\Helpers\ArrayAssert;
use App\Exception\AddressTypeAssert\AddressTypeAssertException;
use App\Exception\ArrayAssert\ArrayAssertException;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Log\Logger;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Controller\ApiResponseCodesInterface;
/**
 * Class PostAddAddress
 */
class PostAddAddress
{

    public function __construct()
    {
    }

    /**
     * @param LoggerInterface $logger
     * @param Request $request
     * @param Address $data
     */
    public function __invoke(LoggerInterface $logger,Request $request,$data)
    {
        

        try{
            $info = @json_decode($request->getContent(), true);
            ArrayAssert::validateOrException($info)
                ->hasNonEmptyKey('phoneNumber')
                ->hasNonEmptyKey("street")
                ->hasKey('nr')
                ->hasNonEmptyKey('country')
                ->hasNonEmptyKey('fullName')
                ->hasNonEmptyKey('addressType');

            AddressTypeAssert::validateOrException($info['addressType']);

            return $data;
        }catch (ArrayAssertException $arrayAssertException) {
            $logger->error("Error while registering new user, invalid data: " . $arrayAssertException->getMessage());

            return new JsonResponse(["error" => ApiResponseCodesInterface::ERR_INVALID_DATA], 400);
        }catch (AddressTypeAssertException $addressTypeAssertException) {
            $logger->error("Error while registering new user, invalid type data: " . $addressTypeAssertException->getMessage());

            return new JsonResponse(["error" => ApiResponseCodesInterface::IVALID_ADDRESS_TYPE], 400);
        }
    }
}