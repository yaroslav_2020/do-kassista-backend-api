<?php

declare(strict_types=1);


namespace App\Controller\Operations\Settings\Collection;


use App\Controller\Operations\Settings\SettingsValidator;
use App\Entity\Settings;
use App\Entity\Voucher;

/**
 * Class PostSettings
 * @package App\Controller\Operations\Settings\Collection
 */
class PostSettings
{
    /**
     * GetVoucherController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param Settings $data
     * @return Settings
     */
    public function __invoke(Settings $data): Settings
    {
        SettingsValidator::validate($data);
        return $data;
    }
}