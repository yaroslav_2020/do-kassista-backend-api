<?php

declare(strict_types=1);


namespace App\Controller\Operations\Settings\Collection;


use App\Entity\User;
use App\Entity\Voucher;

/**
 * Class GetSettings
 * @package App\Controller\Operations\Settings\Collection
 */
class GetSettings
{
    /**
     * GetVoucherController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param Voucher $data
     * @return Voucher
     * @throws \Exception
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function __invoke($data)
    {
        return $data;
    }
}