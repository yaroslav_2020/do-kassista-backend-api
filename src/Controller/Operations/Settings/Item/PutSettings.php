<?php

declare(strict_types=1);

namespace App\Controller\Operations\Settings\Item;


use App\Entity\Settings;
use App\Entity\User;
use App\Entity\Voucher;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class PutSettings
 * @package App\Controller\Operations\Settings\Item
 */
class PutSettings
{
    /**
     * GetVoucherController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param Settings $data
     * @return Settings
     */
    public function __invoke(Settings $data): Settings
    {
        return $data;
    }
}