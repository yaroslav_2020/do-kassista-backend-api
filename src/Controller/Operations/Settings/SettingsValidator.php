<?php

declare(strict_types=1);

namespace App\Controller\Operations\Settings;

use App\Entity\Settings;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class SettingsValidator
 * @package App\Controller\Operations\Settings
 */
class SettingsValidator
{
    /**
     * @param Settings $settings
     */
    public static function validate(Settings $settings)
    {
        $allowed = [
            Settings::JOB_LINK_VALIDITY_IN_HOURS,
            Settings::STANDARD_PRICE_INDEPENDENT_WORKER,
            Settings::STANDARD_PRICE_HOUR,
            Settings::ALLOW_VOUCHER_APPLY,
            Settings::ROOM_CLEANING_DURATION_TIME,
            Settings::BATHROOM_CLEANING_DURATION_TIME,
            Settings::KITCHEN_CLEANING_DURATION_TIME,
            Settings::LIVING_CLEANING_DURATION_TIME,
            Settings::MAX_HOURS,
            Settings::MIN_HOURS,
            Settings::MAX_PRICE_PER_HOUR,
            Settings::MIN_PRICE_PER_HOUR,
            Settings::PRICE_PER_CLEANNIG_TOOLS,
            Settings::START_LABOR_SCHEDULE,
            Settings::END_LABOR_SCHEDULE,
            Settings::HOURS_BUFFER,
            Settings::time_buffer_to_block_iteration,
            Settings::time_buffer_to_realease_blocked_payment,
            Settings::DEV_MODE,
        ];

        $typePrice = [Settings::STANDARD_PRICE_HOUR, Settings::STANDARD_PRICE_INDEPENDENT_WORKER];
        if (!in_array($settings->getName(), $allowed)) {
            throw new BadRequestHttpException("Setting name not allowed");
        }
        if (empty($settings->getData()['value'])) {
            throw new BadRequestHttpException("Value not set on settings");
        }

        if (in_array($settings->getName(), $typePrice)) {
            if (empty($settings->getData()['currency'])) {
                throw new BadRequestHttpException("Currency not set on settings");
            }
        }
    }
}