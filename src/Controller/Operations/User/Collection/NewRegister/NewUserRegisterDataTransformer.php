<?php

declare(strict_types=1);

namespace App\Controller\Operations\User\Collection\NewRegister;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\User;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


/**
 * Class NewUserRegisterDataTransformer
 * @package App\Controller\Operations\User\Item\NewRegister
 */
class NewUserRegisterDataTransformer implements DataTransformerInterface
{

    /** @var ValidatorInterface */
    private $validator;

    /**
     * NewUserRegisterInput constructor.
     * @param NewUserRegisterValidator $validator
     */
    public function __construct(NewUserRegisterValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Transforms the given object to something else, usually another object.
     * This must return the original object if no transformation has been done.
     *
     * @param object $object
     *
     * @param string $to
     * @param array $context
     * @return User
     */
    public function transform($object, string $to, array $context = [])
    {
        if (!($object instanceof NewUserRegisterInput)) {
            throw new BadRequestHttpException("Invalid data");
        }

        $this->validator->validate($object);

        $user = new User();
        $user->setEmail($object->getEmail());
        $user->setPlainPassword($object->getPassword());
        $user->setName($object->getDisplayName());
        $user->setUsername($object->getEmail());
        $user->setRoles([ $object->getRole() ]);

        return $user;
    }

    /**
     * Checks whether the transformation is supported for a given object and context.
     *
     * @param object $object
     * @param string $to
     * @param array $context
     * @return bool
     */
    public function supportsTransformation($object, string $to, array $context = []): bool
    {
        if ($object instanceof User) {
            return false;
        }
        if ($context['input']['class'] === NewUserRegisterInput::class) {
            return User::class === $to && null !== ($context['input']['class'] ?? null);
        }

        return false;
    }
}