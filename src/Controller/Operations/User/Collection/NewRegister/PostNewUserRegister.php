<?php


namespace App\Controller\Operations\User\Collection\NewRegister;


use App\Application\Security\SecurityService;
use App\Entity\User;
use App\Entity\Voucher;

/**
 * Class PostNewUserRegister
 * @package App\Controller\Operations\User\Item\NewRegister
 */
class PostNewUserRegister
{
    /**
     * @var SecurityService
     */
    private $securityService;

    /**
     * @param SecurityService $securityService
     */

    public function __construct(SecurityService $securityService)
    {
        $this->securityService = $securityService;
    }

    /**
     * @param User $data
     * @return void
     * @throws \App\Exception\Security\ExistingUserException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke($data): User
    {
        $user = $this->securityService->registerUserObject(
            $data,
            [
                'register_mode' => SecurityService::AUTH_MODE_REGISTER_NO_CONFIRMATION
            ]
        );

        return $user;
    }
}
