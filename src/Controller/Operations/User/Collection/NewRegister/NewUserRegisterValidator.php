<?php

declare(strict_types=1);

namespace App\Controller\Operations\User\Collection\NewRegister;

use ApiPlatform\Core\Bridge\Symfony\Validator\Exception\ValidationException;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\User;
use http\Exception\InvalidArgumentException;
use http\Exception\RuntimeException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class NewUserRegisterValidator
 * @package App\Controller\Operations\User\Item\NewRegister
 */
class NewUserRegisterValidator implements ValidatorInterface
{

    /**
     * Validates an item.
     *
     * @param object $data
     *
     * @param array $context
     */
    public function validate($data, array $context = [])
    {
        if (!($data instanceof NewUserRegisterInput)) {
            throw new \RuntimeException("Invalid argument");
        }

        if ($data->getPassword() !== $data->getConfirmPassword()) {
            throw new BadRequestHttpException("Password does not match the confirmation password");
        }

        if (!in_array($data->getRole(), User::USER_ROLE_LIST, true)) {
            throw new BadRequestHttpException("User role is invalid");
        }
    }
}