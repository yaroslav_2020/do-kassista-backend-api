<?php

declare(strict_types=1);


namespace App\Controller\Operations\User\Item;


use App\Entity\AclAction;
use App\Entity\User;
use App\Entity\UserAclAction;
use App\Entity\Voucher;
use App\Normalizer\AclActionNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;

class GetAclActions
{
    /**
     * GetVoucherController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param Voucher $data
     * @return Voucher
     * @throws \Exception
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function __invoke(User $data)
    {
        return $data;
//        $userAclActions = $data->getUserAclActions();
//        $actions = [];
//        /** @var UserAclAction $userAclAction */
//        foreach ($userAclActions as $userAclAction) {
//            $actions[] = $userAclAction->getAclAction();
//        }
//        $serializer = new Serializer([new AclActionNormalizer()]);
//        return new JsonResponse($serializer->normalize($actions, null, ["SymfonyController"]));
    }
}