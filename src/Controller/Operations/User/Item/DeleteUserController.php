<?php

namespace App\Controller\Operations\User\Item;

use App\Entity\User;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DeleteUserController extends AbstractController
{
    public function __invoke(User $data)
    {
        return $data;
    }
}
