<?php

namespace App\Controller\Operations\User\Item;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;

class UpdateUserController extends AbstractController
{
    /**
     * @Route("/update/user", name="update_user")
     */
    public function index()
    {
        return $this->render('update_user/index.html.twig', [
            'controller_name' => 'UpdateUserController',
        ]);
    }

    public function __invoke(User $data)
    {
        return $data;
    }
}
