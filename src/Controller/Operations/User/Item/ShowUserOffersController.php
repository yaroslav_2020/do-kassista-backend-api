<?php

namespace App\Controller\Operations\User\Item;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Application\Security\SecurityService;
use Symfony\Component\Serializer\SerializerInterface;
use App\Entity\User;
use App\Entity\Offer;

class ShowUserOffersController extends AbstractController
{
    /**
     * @Route("/a/s/show/user/offers", name="a_s_show_user_offers")
     */
    public function index()
    {
        return $this->render('as_show_user_offers/index.html.twig', [
            'controller_name' => 'ShowUserOffersController',
        ]);
    }

    public function __invoke($data){

        return $data;
    }
}
