<?php

declare(strict_types=1);


namespace App\Controller\Operations\User\Item;

use App\Entity\User;
use App\Entity\UserAclAction;
use App\Entity\Voucher;
use App\Normalizer\AclActionNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;

class GetUser
{
    /**
     * GetVoucherController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param Voucher $data
     * @return Voucher
     * @throws \Exception
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function __invoke(User $data)
    {
        return $data;
    }
}