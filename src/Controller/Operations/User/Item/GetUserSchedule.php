<?php

declare(strict_types=1);


namespace App\Controller\Operations\User\Item;

use App\Entity\User;
use App\Entity\UserAclAction;

class GetUserSchedule
{
    /**
     * GetVoucherController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param Voucher $data
     * @return User
     * @throws \Exception
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function __invoke(User $data)
    {
        return $data;
    }
}