<?php


namespace App\Controller\Operations\User\Item\ResetPassword;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Application\Security\SecurityService;
use App\Controller\Operations\User\Collection\NewRegister\NewUserRegisterInput;
use App\Controller\Operations\User\Collection\NewRegister\NewUserRegisterValidator;
use App\Entity\User;
use App\Exception\Security\PasswordNotMatchedException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class ResetPasswordTransformer
 * @package App\Controller\Operations\User\Item\ResetPassword
 */
class ResetPasswordTransformer implements DataTransformerInterface
{

    /** @var ValidatorInterface */
    private $validator;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var SecurityService  */
    private $securityService;

    /**
     * NewUserRegisterInput constructor.
     * @param RestPasswordValidator $validator
     * @param EntityManagerInterface $entityManager
     * @param SecurityService $securityService
     */
    public function __construct(
        RestPasswordValidator $validator,
        EntityManagerInterface $entityManager,
        SecurityService $securityService
    ) {
        $this->validator = $validator;
        $this->entityManager = $entityManager;
        $this->securityService = $securityService;
    }

    /**
     * Transforms the given object to something else, usually another object.
     * This must return the original object if no transformation has been done.
     *
     * @param object $object
     *
     * @param string $to
     * @param array $context
     * @return User
     * @throws PasswordNotMatchedException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function transform($object, string $to, array $context = [])
    {
        if (!($object instanceof RestPasswordInput)) {
            throw new BadRequestHttpException("Invalid data");
        }

        $this->validator->validate($object);

        /** @var User $existingUser */
        $existingUser = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE];
        if ($existingUser->getId() !== $object->getUserId()) {
            throw new BadRequestHttpException("User id not matched");
        }

        $existingUser->setPlainPassword($object->getPassword());
        $existingUser->setPassword(null);
        $this->securityService->changePassword(
            $existingUser,
            $existingUser->getPlainPassword(),
            $existingUser->getPlainPassword()
        );

        return $existingUser;
    }

    /**
     * Checks whether the transformation is supported for a given object and context.
     *
     * @param object $object
     * @param string $to
     * @param array $context
     * @return bool
     */
    public function supportsTransformation($object, string $to, array $context = []): bool
    {
        if ($object instanceof User) {
            return false;
        }
        if ($context['input']['class'] === RestPasswordInput::class) {
            return User::class === $to && null !== ($context['input']['class'] ?? null);
        }

        return false;
    }

}