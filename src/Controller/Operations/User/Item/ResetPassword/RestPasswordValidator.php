<?php


namespace App\Controller\Operations\User\Item\ResetPassword;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Controller\Operations\User\Collection\NewRegister\NewUserRegisterInput;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class RestPasswordValidator
 * @package App\Controller\Operations\User\Item\ResetPassword
 */
class RestPasswordValidator implements ValidatorInterface
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * RestPasswordValidator constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Validates an item.
     *
     * @param object $data
     *
     * @param array $context
     */
    public function validate($data, array $context = [])
    {
        if (!($data instanceof RestPasswordInput)) {
            throw new \RuntimeException("Invalid argument");
        }

        if (empty($data->getUserId())) {
            throw new BadRequestHttpException("User id is empty");
        }

        if ((int) $data->getUserId() <= 0) {
            throw new BadRequestHttpException("User id is invalid");
        }

        if ($data->getPassword() !== $data->getConfirmPassword()) {
            throw new BadRequestHttpException("Password does not match the confirmation password");
        }

        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'id' => (int) $data->getUserId()
        ]);
        if (!($user instanceof User)) {
            throw new BadRequestHttpException("User not found in system");
        }
    }
}