<?php

declare(strict_types=1);


namespace App\Controller\Operations\User\Item;


use App\Application\Security\SecurityService;
use App\Entity\AclAction;
use App\Entity\User;
use App\Entity\Voucher;
use App\Repository\AclActionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class EditAclActions
 * @package App\Controller\Operations\User\Item
 */
class EditAclActions
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var SecurityService
     */
    protected $securityService;

    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * GetVoucherController constructor.
     * @param RequestStack $requestStack
     * @param SecurityService $securityService
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        RequestStack $requestStack,
        SecurityService $securityService,
        EntityManagerInterface $entityManager
    ) {
        $this->requestStack = $requestStack;
        $this->securityService = $securityService;
        $this->entityManager = $entityManager;
    }

    /**
     * @param User $data
     * @return User
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(User $data)
    {
        $request = $this->requestStack->getCurrentRequest();
        if (!($request instanceof Request)) {
            throw new \RuntimeException("Request not found");
        }
        $actionsAllowed = $request->request->get("acl_actions_allowed", []);
        $allowedDecoded = @json_decode($request->getContent(), true);
        if ($allowedDecoded === false) {
            throw new HttpException("Request content can not be decoded");
        }
        if (!array_key_exists("acl_actions_allowed", $allowedDecoded)) {
            throw new \InvalidArgumentException("Allowed list not found");
        }

        /** @var AclActionRepository $aclActionRepository */
        $aclActionRepository = $this->entityManager->getRepository(AclAction::class);
        $actions = $aclActionRepository->findAllUsingIds($allowedDecoded['acl_actions_allowed']);

        $this->securityService->updateUserActions($data, $actions);

        return $data;
    }
}