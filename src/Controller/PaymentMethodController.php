<?php

namespace App\Controller;

use App\Repository\UsersRepository;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PaymentMethodController extends AbstractController
{
    private $stripe;
    private $usersRepository;
    private $logger;

    public function __construct(
        StripeController $stripeController,
        UsersRepository $usersRepository,
        LoggerInterface $logger

    )
    {
        $this->stripe = $stripeController;
        $this->stripe->setStripeKey($_ENV['STRIPE_KEY']);
        $this->usersRepository = $usersRepository;
        $this->logger = $logger;
    }

    public function listPaymentMethods(Request $request): JsonResponse
    {
        $data = @json_decode($request->getContent(), true);

        $client = $this->usersRepository->findUserByEmail($data['customer']);

        $paymentMethodList = $this->stripe->paymentMethods($client->getCustomerStripeId());

        return new JsonResponse($paymentMethodList);
    }

    public function detachPaymentMethod(Request $request): JsonResponse
    {
        $data = @json_decode($request->getContent(), true);

        $detachedPaymentMethod = $this->stripe->detachPaymentMethod($data['paymentMethod']);

        return new JsonResponse($detachedPaymentMethod);
    }

    public function updatePaymentMethod(Request $request): JsonResponse
    {
        $data = @json_decode($request->getContent(), true);
        $updatedPaymentMethod = $this->stripe->updatePaymentMethod($data);

        return new JsonResponse($updatedPaymentMethod);
    }

    public function defaultPaymentMethod(Request $request): JsonResponse
    {
        $data = @json_decode($request->getContent(), true);

        $client = $this->usersRepository->findUserByEmail($data['customer']);
        $client->setActivePaymentMethod($data['invoice_settings']['default_payment_method']);
        $em = $this->getDoctrine()->getManager();
        $em->persist($client);
        $em->flush();

        $data['customer'] = $client->getCustomerStripeId();

        $updatedCustomerPaymentMethod = $this->stripe->setDefaultPaymentMethod($data);

        return new JsonResponse($updatedCustomerPaymentMethod);
    }

}
