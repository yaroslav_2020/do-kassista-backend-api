<?php

declare(strict_types=1);

namespace App\Controller;

/**
 * Interface ApiResponseCodesInterface
 * @package App\Controller
 */
interface ApiResponseCodesInterface
{
    const ERR_USER_NOT_FOUND = "user_not_found";
    const ERR_EXISTING_USER = "existing_user";
    const ERR_INVALID_DATA = "invalid_data";
    const ERR_INTERNAL_ERROR = "internal_error";
    const ERR_PASSWORDS_NOT_MATCH = "passwords_not_matched";
    const STATUS_OK = "ok";
    const INVALID_TOKEN_OR_USER_NOT_FOUND = "invalid_token_or_user_not_found";
    const IVALID_TYPE = "type_must_be_an_integer";
    const IVALID_ADDRESS_TYPE = "invalid_address_type";
}