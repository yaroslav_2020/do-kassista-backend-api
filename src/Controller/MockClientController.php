<?php


namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class MockupApiController
 * @package App\Controller
 *
 * Here we have all routes used to redirect user from backend/email to client
 */
class MockClientController
{
    /**
     * Mock confirm new account route
     *
     * @param Request $request
     * @param $token
     */
    public function confirmNewAccount(Request $request, $token)
    {
        throw new \RuntimeException("Nothing to implemented here, handle from frontend");
    }

    /**
     * Mock confirm new account route
     *
     * @param Request $request
     * @param $token
     */
    public function authenticateResetPassword(Request $request)
    {
        throw new \RuntimeException("Nothing to implemented here, handle from frontend");
    }

    /**
     * Mock confirm new account route
     *
     * @param Request $request
     * @param $id
     */
    public function newOfferDetails(Request $request, $id)
    {
        throw new \RuntimeException("Nothing to implemented here, handle from frontend");
    }
}