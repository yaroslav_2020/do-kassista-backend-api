<?php


namespace App\Controller;


use App\Application\Translation\DbTranslator;
use App\Entity\Faq;
use App\Entity\KnowledgeBase;
use App\Normalizer\Faq\FaqNormalizer;
use App\Normalizer\KnowledgeBase\KnowledgeBaseNormalizer;
use App\Repository\FaqRepository;
use App\Repository\KnowledgeBaseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * Class FaqController
 * @package App\Controller
 */
class FaqController extends AbstractController
{
    /** @var FaqRepository */
    private $faqRepository;

    /** @var KnowledgeBaseRepository */
    private $knowledgeBaseRepository;

    /**
     * FaqController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->faqRepository = $entityManager->getRepository(Faq::class);
        $this->knowledgeBaseRepository = $entityManager->getRepository(KnowledgeBase::class);
    }

    /**
     * @param DbTranslator $dbTranslator
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function getFaq(DbTranslator $dbTranslator)
    {
        $faqs = $this->faqRepository->findAll();
        $serializer = new Serializer([new FaqNormalizer($dbTranslator)]);
        $data = [];
        /** @var Faq $faq */
        foreach ($faqs as $faq) {
            $data[] = $serializer->normalize($faq);
        }

        return new JsonResponse($data);
    }

    /**
     * @param DbTranslator $dbTranslator
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function knowledgeBase(DbTranslator $dbTranslator)
    {
        $faqs = $this->knowledgeBaseRepository->findAll();
        $serializer = new Serializer([new KnowledgeBaseNormalizer($dbTranslator)]);
        $data = [
            'id'              => '1',
            'title'           => 'Support',
            'path'            => '/pages/knowledge-base',
            'articlesCount'   => 0,
            'featuredArticles'=> []
        ];

        /** @var Faq $faq */
        foreach ($faqs as $faq) {
            $data['featuredArticles'][] = $serializer->normalize($faq);
            $data['articlesCount']++;
        }

        return new JsonResponse([$data]);
    }
}