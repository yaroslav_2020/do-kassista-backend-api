<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Offer;
use App\Application\Mailer\AuthMailer;
use FOS\UserBundle\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\User;

class ShowOfferInfoController extends AbstractController
{

    /** @var MailerInterface */
    private $mailer;

    /**
     * @Route("/show/offer/info", name="show_offer_info")
     */
    public function index()
    {
        return $this->render('show_offer_info/index.html.twig', [
            'controller_name' => 'ShowOfferInfoController',
        ]);
    }

    /**
     * @param AuthMailer $mailer
     * @param Offer $data
     */
    public function __invoke(AuthMailer $mailer,Offer $data)
    {
        $this->mailer = $mailer;
        $this->mailer->sendNewOfferInfo($data);
        // $offer = $this->getDoctrine()->getRepository(User::class)->findAll();
        // return new JsonResponse($offer);
        return $data;
    }
}