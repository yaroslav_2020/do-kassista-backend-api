<?php

declare(strict_types=1);

namespace App\Controller;



use App\Application\Generator\Token\RegisterTokenGenerator;
use App\Application\Mailer\AuthMailer;
use App\Application\Security\SecurityService;
use App\Entity\User;
use App\Exception\ArrayAssert\ArrayAssertException;
use App\Exception\IntTypeAssert\IntTypeAssertException;
use App\Exception\InvalidDataException;
use App\Exception\Security\ExistingUserException;
use App\Exception\Security\PasswordNotMatchedException;
use App\Exception\Security\UserNotFoundException;
use App\Helpers\ArrayAssert;
use App\Helpers\IntTypeAssert;
use App\Normalizer\AclActionNormalizer;
use App\Normalizer\User\UserDataNormalizer;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use PetstoreIO\ApiResponse;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Log\Logger;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

/**
 * Class AuthController
 * @package App\Controller
 */
class AuthController extends AbstractController
{

    /** @var UsersRepository $userRepository */
    private $usersRepository;

    /** @var CanonicalFieldsUpdater  */
    private $canonicalFieldsUpdater;

    /** @var MailerInterface */
    private $mailer;

    /** @var RegisterTokenGenerator  */
    private $tokenGenerator;
    /**
     * @var StripeController
     */
    private $stripe;

    /**
     * AuthController Constructor
     *
     * @param UsersRepository $usersRepository
     * @param CanonicalFieldsUpdater $canonicalFieldsUpdater
     * @param AuthMailer $mailer
     * @param RegisterTokenGenerator $tokenGenerator
     */
    public function __construct(

        UsersRepository $usersRepository,
        CanonicalFieldsUpdater $canonicalFieldsUpdater,
        AuthMailer $mailer,
        RegisterTokenGenerator $tokenGenerator,
        StripeController $stripeController

    ) {
        $this->usersRepository = $usersRepository;
        $this->canonicalFieldsUpdater = $canonicalFieldsUpdater;
        $this->tokenGenerator = $tokenGenerator;
        $this->mailer = $mailer;
        $this->stripe = $stripeController;
        $this->stripe->setStripeKey($_ENV['STRIPE_KEY']);
    }

    /**
     * Register new user
     * @param LoggerInterface $logger
     * @param SecurityService $securityService
     * @param Request $request
     *
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function register(
        LoggerInterface $logger,
        SecurityService $securityService,
        Request $request,
        EntityManagerInterface $entityManager
    ) {

        $exception = null;
        $logger->error($request->getContent());

        

        try {

            $data = @json_decode($request->getContent(), true);

            ArrayAssert::validateOrException($data)
                ->hasNonEmptyKey('email')
                ->hasNonEmptyKey("name")
                ->hasNonEmptyKey('password')
                ->hasNonEmptyKey('confirm_password')
                ->hasBooleanKey('register_mode')
                ->hasBooleanKey('phone_number');

            if (!empty($data["user_type"])) {
                IntTypeAssert::validateOrException($data["user_type"]);
                $user_type = $data["user_type"];
            } else if ($data["user_type"] == 0) {
                $user_type = $data["user_type"];
            } else {
                $user_type = -1;
            }

            $securityService->register(
                $data['name'],
                $data['email'],
                $data['password'],
                $data['confirm_password'],
                $user_type,
                $data['register_mode'],
                $data['phone_number']
            );

            $token = $securityService->login($data['email'], $data['password']);
            $userRepository = $entityManager->getRepository(User::class);
            $user = $userRepository->findUserByEmail($data['email']);
                if (!($user instanceof User)) {
                    throw new \RuntimeException("User not fond after register process was successful");
                }

            if (in_array('ROLE_CLIENT', $user->getRoles())) { //CHECK
                $existingUser = $this->stripe->verifyUserInStripe($user->getEmail());

                $customerId = null;

                if (is_bool($existingUser) && $existingUser === false) {
                    $logger->info('[NEW CLIENT::UNREGISTERED]-------------Creating Stripe Customer');
                    $customerId = $this->stripe->createCustomer($user->getEmail(), $user->getName());
                } else {
                    $customerId = $existingUser;
                    $logger->info("[NEW CLIENT::REGISTERED]-------------Don't Create the customer");
                }

                //UPDATE THE USER WITH THE CUSTOMER ID FROM STRIPE
                $user->setCustomerStripeId((string)$customerId);
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
            }

            $serializer = new Serializer([new UserDataNormalizer()]);

            return new JsonResponse([
                'access_token' => $token,
                'status' => ApiResponseCodesInterface::STATUS_OK,
                'user' => $serializer->normalize($user, null, ["SymfonyController"])
            ]);
        } catch (ExistingUserException $existingUserException) {
            $logger->error("Error while registering user: " . $existingUserException->getMessage());

            return new JsonResponse(['error' => ApiResponseCodesInterface::ERR_EXISTING_USER]);
        } catch (ArrayAssertException $arrayAssertException) {
            $logger->error("Error while registering new user, invalid data: " . $arrayAssertException->getMessage());

            return new JsonResponse(["error" => ApiResponseCodesInterface::ERR_INVALID_DATA]);
        } catch (IntTypeAssertException $intTypeAssertException) {
            $logger->error("Error while registering new user, invalid type data: " . $intTypeAssertException->getMessage());

            return new JsonResponse(["error" => ApiResponseCodesInterface::IVALID_TYPE]);
        } catch (\Throwable $exception) {
            $logger->error("Error while registering new user: " . $exception->getMessage());

            return new JsonResponse(["error" => ApiResponseCodesInterface::ERR_INTERNAL_ERROR]);
        }
    }

    /**
     * @param LoggerInterface $logger
     * @param SecurityService $securityService
     * @param Request $request
     * @param $token
     * @return JsonResponse
     */
    public function confirmNewUser(
        LoggerInterface $logger,
        SecurityService $securityService,
        Request $request,
        $token
    ) {
        try {
            $securityService->confirmNewUser($token);

            return new JsonResponse(["status" => ApiResponseCodesInterface::STATUS_OK]);
        } catch (\Exception $exception) {
            $logger->error("Error while confirming new user: " . $exception->getMessage());
            return new JsonResponse(["error" => ApiResponseCodesInterface::ERR_INTERNAL_ERROR]);
        }
    }

    /**
     * @param LoggerInterface $logger
     * @param SecurityService $securityService
     * @param Request $request
     * @return JsonResponse
     */
    public function requestResetPassword(
        LoggerInterface $logger,
        SecurityService $securityService,
        Request $request
    ) {
        try {
            $data = @json_decode($request->getContent(), true);
            ArrayAssert::validateOrException($data)->hasNonEmptyKey('email');
            $securityService->requestResetPassword($data['email']);

            return new JsonResponse(["status" => ApiResponseCodesInterface::STATUS_OK]);
        } catch (UserNotFoundException $userNotFoundException) {
            $logger->error("Error while registering new user, user not found: " . $userNotFoundException->getMessage());

            return new JsonResponse(['error' => ApiResponseCodesInterface::ERR_USER_NOT_FOUND]);
        } catch (ArrayAssertException $arrayAssertException) {
            $logger->error("Error while registering new user, invalid data: " . $arrayAssertException->getMessage());

            return new JsonResponse(["error" => ApiResponseCodesInterface::ERR_INVALID_DATA]);
        } catch (\Exception $exception) {
            $logger->error("Error while confirming new user: " . $exception->getMessage());

            return new JsonResponse(["error" => ApiResponseCodesInterface::ERR_INTERNAL_ERROR]);
        }

        return new JsonResponse([]);
    }

    /**
     * @param LoggerInterface $logger
     * @param SecurityService $securityService
     * @param Request $request
     * @return JsonResponse
     */
    public function authenticateResetPassword(
        LoggerInterface $logger,
        SecurityService $securityService,
        Request $request
    ) {
        $authenticatePasswordResponse = $this->doAuthenticatePassword($logger, $securityService, $request);
        if ($authenticatePasswordResponse instanceof JsonResponse) {
            return $authenticatePasswordResponse;
        }

        return new JsonResponse(["status" => ApiResponseCodesInterface::STATUS_OK]);
    }

    /**
     * @param LoggerInterface $logger
     * @param SecurityService $securityService
     * @param Request $request
     * @return JsonResponse
     */
    public function resetPassword(
        LoggerInterface $logger,
        SecurityService $securityService,
        Request $request
    ) {
        $authenticatePasswordResponse = $this->doAuthenticatePassword($logger, $securityService, $request);
        if ($authenticatePasswordResponse instanceof JsonResponse) {
            return $authenticatePasswordResponse;
        }

        try {
            $data = @json_decode($request->getContent(), true);
            ArrayAssert::validateOrException($data)
                ->hasNonEmptyKey('password')
                ->hasNonEmptyKey('confirmation_password');


            $securityService->doResetPassword(
                $data['password'],
                $data['confirmation_password'],
                $request->query->get('token')
            );

            return new JsonResponse(["status" => ApiResponseCodesInterface::STATUS_OK]);
        } catch (UserNotFoundException $userNotFoundException) {
            $logger->error("Error while resetting password, user not found" . $userNotFoundException->getMessage());

            return new JsonResponse(['error' => ApiResponseCodesInterface::ERR_USER_NOT_FOUND]);
        } catch (PasswordNotMatchedException $notMatchedException) {
            $logger->error("Error while resetting password, password not matched " . $notMatchedException->getMessage());

            return new JsonResponse(['error' => ApiResponseCodesInterface::ERR_PASSWORDS_NOT_MATCH]);
        } catch (\Exception $exception) {
            $logger->error("Error while resetting password: " . $exception->getMessage());

            return new JsonResponse(["error" => ApiResponseCodesInterface::ERR_INTERNAL_ERROR]);
        }
    }

    /**
     * @param LoggerInterface $logger
     * @param SecurityService $securityService
     * @param Request $request
     * @return JsonResponse|null
     */
    private function doAuthenticatePassword(
        LoggerInterface $logger,
        SecurityService $securityService,
        Request $request
    ) {
        try {
            $token = $request->query->get('token', null);
            if (empty($token)) {
                throw new InvalidDataException("Token is empty");
            }
            $securityService->authenticateRequestPassword($token);
        } catch (UserNotFoundException $userNotFoundException) {
            $logger->error("Error while do authenticating for new password, missing user: " . $userNotFoundException->getMessage());

            return new JsonResponse(["error" => ApiResponseCodesInterface::ERR_USER_NOT_FOUND]);
        } catch (\Exception $exception) {
            $logger->error("Error while do authenticating for new password: " . $exception->getMessage());

            return new JsonResponse(["error" => ApiResponseCodesInterface::ERR_INTERNAL_ERROR]);
        } catch (InvalidDataException $invalidDataException) {
            $logger->error("Error while do authenticating for new password: " . $invalidDataException->getMessage());

            return new JsonResponse(["error" => ApiResponseCodesInterface::ERR_INVALID_DATA]);
        }
    }

    /**
     * @param LoggerInterface $logger
     * @param SecurityService $securityService
     * @param Request $request
     * @return JsonResponse|null
     */
    public function changePassword(
        LoggerInterface $logger,
        SecurityService $securityService,
        Request $request
    ) {
        try {
            $user = $this->getUser();
            if (!($user instanceof User)) {
                throw new \RuntimeException("User not found");
            }
            $data = @json_decode($request->getContent(), true);
            ArrayAssert::validateOrException($data)
                ->hasNonEmptyKey('prev_password')
                ->hasNonEmptyKey('password')
                ->hasNonEmptyKey('confirmed_password');

            $isValid = $securityService->checkPassword($user, $data['prev_password']);

            if ($isValid) {
                $securityService->changePassword($user, $data['password'], $data['confirmed_password']);
            } else {
                return new JsonResponse(["error" => "La contrasena actual es incorrecta"]);
            }
        } catch (PasswordNotMatchedException $passwordNotMatchedException) {
            $logger->error("Error while changing password: " . $passwordNotMatchedException->getMessage());
            return new JsonResponse(["error" => ApiResponseCodesInterface::ERR_PASSWORDS_NOT_MATCH]);
        } catch (UserNotFoundException $userNotFoundException) {
            $logger->error("Error while changing password: , missing user: " . $userNotFoundException->getMessage());
            return new JsonResponse(["error" => ApiResponseCodesInterface::ERR_USER_NOT_FOUND]);
        } catch (\Exception $exception) {
            $logger->error("Error while changing password: : " . $exception->getMessage());
            return new JsonResponse(["error" => ApiResponseCodesInterface::ERR_INTERNAL_ERROR]);
        }

        return new JsonResponse(["status" => ApiResponseCodesInterface::STATUS_OK]);
    }

    /**
     * @param LoggerInterface $logger
     * @param SecurityService $securityService
     * @param Request $request
     * @param EntityManagerInterface $entityManager
    //  * @return JsonResponse|null
     * @throws ExceptionInterface
     */
    public function login(
        LoggerInterface $logger,
        SecurityService $securityService,
        Request $request,
        EntityManagerInterface $entityManager
    ) {
       
        try {
            
            $data = @json_decode($request->getContent(), true);

            ArrayAssert::validateOrException($data)
                ->hasNonEmptyKey('email')
                ->hasNonEmptyKey('password');

            $email = $data['email'];
            $password = $data['password'];

            $token = $securityService->login($email, $password);
            
            $userRepository = $entityManager->getRepository(User::class);
            $user = $userRepository->findUserByEmail($email);
            if (!($user instanceof User)) {
                throw new \RuntimeException("User not fond after login process was successful");
            }

            $serializer = new Serializer([new UserDataNormalizer()]);

             
            return new JsonResponse([
                'status' => ApiResponseCodesInterface::STATUS_OK,
                'access_token' => $token,
                'user' => $serializer->normalize($user, null, ["SymfonyController"]),

               
            ]);
        } catch (UserNotFoundException $userNotFoundException) {
            $logger->error("Error on login: " . $userNotFoundException->getMessage());
            return new JsonResponse(["status" => -1, "error" => $userNotFoundException->getMessage()]);
        } catch (BadCredentialsException $BadCredentialsException) {
            $logger->error("Error on login: " . $BadCredentialsException->getMessage());
            return new JsonResponse(["status" => -2, "error" => $BadCredentialsException->getMessage()]);
        } catch (\Exception $exception) {
            $logger->error("Error on login: " . $exception->getMessage() . $exception->getTraceAsString() . $exception->getFile() . " " . $exception->getLine());
            return new JsonResponse(["status" => -3, "error" => ApiResponseCodesInterface::ERR_INTERNAL_ERROR]);
        }
    }

    /**
     * @param LoggerInterface $logger
     * @param Request $request
     * @param SecurityService $securityService
     * @return JsonResponse
     */
    public function accessToken(LoggerInterface $logger, Request $request, SecurityService $securityService)
    {
        try {
            $accessToken = $request->query->get('accessToken');
            if (empty($accessToken)) {
                return new JsonResponse(['error' => ApiResponseCodesInterface::ERR_INVALID_DATA]);
            }
            /** @var User $user */
            $user = $securityService->accessToken($accessToken);

            if ($user instanceof User) {
                $serializer = new Serializer([new UserDataNormalizer()]);
                return new JsonResponse([
                    'access_token' => $accessToken,
                    'user' => $serializer->normalize($user, null, ["SymfonyController"])
                ]);
            }
        } catch (\Throwable $throwable) {
            $logger->error("There was an axception while checking token access, msg: " . $throwable->getMessage());
            return new JsonResponse(['error' => ApiResponseCodesInterface::ERR_INTERNAL_ERROR]);
        }

        return new JsonResponse(['error' => ApiResponseCodesInterface::INVALID_TOKEN_OR_USER_NOT_FOUND]);
    }
}
