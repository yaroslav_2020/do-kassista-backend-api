<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class StripeCustomerController extends AbstractController
{
    public static function createCustomer()
    {
        \Stripe\Stripe::setApiKey("sk_test_51HOnftCwN6PYafv7NZKOW7TAjgcFegx3FuoKGLeh7qwGNUxDEn3wa6ZyGKn6zDGHytDH4Sw7Ou0RWcqGnh6IhIUC00RAbCEHmf");
        
        $stripe = new \Stripe\StripeClient(
            'sk_test_51HOnftCwN6PYafv7NZKOW7TAjgcFegx3FuoKGLeh7qwGNUxDEn3wa6ZyGKn6zDGHytDH4Sw7Ou0RWcqGnh6IhIUC00RAbCEHmf'
        );

        //create customer
        $customer = $stripe->customers->create([
            'email' => 'coffecake@gmail.com',
            'name' => 'Lolero',
        ]);
        
        //create token with the card details, this part comes from frontend
        $token = $stripe->tokens->create([
            'card' => [
                'number' => '4242424242424242',
                'exp_month' => '5',
                'exp_year' => '2025',
                'cvc' => '123',
            ],
        ]);
        
        //create card and asign it to an user
        $card = $stripe->customers->createSource(
            $customer->id,
            ['source' => $token->id]
        );

        return new JsonResponse($customer);
    }

    public static function getAllCustomers() {
        \Stripe\Stripe::setApiKey("sk_test_51HOnftCwN6PYafv7NZKOW7TAjgcFegx3FuoKGLeh7qwGNUxDEn3wa6ZyGKn6zDGHytDH4Sw7Ou0RWcqGnh6IhIUC00RAbCEHmf");
        
        $stripe = new \Stripe\StripeClient(
            'sk_test_51HOnftCwN6PYafv7NZKOW7TAjgcFegx3FuoKGLeh7qwGNUxDEn3wa6ZyGKn6zDGHytDH4Sw7Ou0RWcqGnh6IhIUC00RAbCEHmf'
        );

        $customers = $stripe->customers->all();

        return new JsonResponse($customers);
    }

    public static function verifyCustomerByEmail() {
        \Stripe\Stripe::setApiKey("sk_test_51HOnftCwN6PYafv7NZKOW7TAjgcFegx3FuoKGLeh7qwGNUxDEn3wa6ZyGKn6zDGHytDH4Sw7Ou0RWcqGnh6IhIUC00RAbCEHmf");
        
        $stripe = new \Stripe\StripeClient(
            'sk_test_51HOnftCwN6PYafv7NZKOW7TAjgcFegx3FuoKGLeh7qwGNUxDEn3wa6ZyGKn6zDGHytDH4Sw7Ou0RWcqGnh6IhIUC00RAbCEHmf'
        );

        $customers = $stripe->customers->all([
            'email' => 'cnvnchgfgxx@gmail.com'
        ]);

        return new JsonResponse($customers);
    }

    public static function verifyCustomerById() {
        \Stripe\Stripe::setApiKey("sk_test_51HOnftCwN6PYafv7NZKOW7TAjgcFegx3FuoKGLeh7qwGNUxDEn3wa6ZyGKn6zDGHytDH4Sw7Ou0RWcqGnh6IhIUC00RAbCEHmf");
        
        $stripe = new \Stripe\StripeClient(
            'sk_test_51HOnftCwN6PYafv7NZKOW7TAjgcFegx3FuoKGLeh7qwGNUxDEn3wa6ZyGKn6zDGHytDH4Sw7Ou0RWcqGnh6IhIUC00RAbCEHmf'
        );

        $customers = $stripe->customers->retrieve(
            'cus_IJmR4sdvsdvbmN4HEDav',
            []
        );

        return new JsonResponse($customers);
    }
}
