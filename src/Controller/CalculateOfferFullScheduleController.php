<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Controller\OfferFullScheduleController;

class CalculateOfferFullScheduleController extends AbstractController
{
    private $offerFullScheduleController;

    public function __construct(OfferFullScheduleController $offerFullScheduleController)
    {
        $this->offerFullScheduleController = $offerFullScheduleController;
    }

    public function calculateOfferFullSchedule(Request $request)
    {
        $data = json_decode($request->query->get('data'));   

        $response = $this->offerFullScheduleController->calculateOfferFullSchedule($data);

        return new JsonResponse($response);
    }
}
