<?php

namespace App\Controller;

class StripeController
{
    /**
     * @var String
     */
    private $stripeKey;

    private $stripe;

    public function __construct()
    {
    }

    public function setStripeKey($stripeKey)
    {
        $this->stripeKey = $stripeKey;
        $this->setStripe();
    }

    private function setStripe()
    {
        $this->stripe = new \Stripe\StripeClient($this->stripeKey);
    }

    public function createCustomer($email,$name)
    {
        $customer = $this->stripe->customers->create([
            'email' => $email,
            'name' => $name,
        ]);

        return $customer->id;
    }

//    public function verifyUserInStripe($customerStripeId,$email)
//    {
//        $verifyEmail = $this->findCustomerByEmail($email);
//
//        if($customerStripeId !== '' && $customerStripeId !== null ){
//            $verifyId = $this->findCustomerByStripeId($customerStripeId);
//            if(is_object($verifyId["customer"])){
//                return false;
//            }
//        }
//
//        if ($verifyEmail->data !== []) {
//            return false;
//        }
//
//
//        return true;
//    }

    /**
     * @param $email
     * @return false
     *
     */
    public function verifyUserInStripe($email)
    {
        $verifyEmail = $this->findCustomerByEmail($email);

        if($verifyEmail->data !== []){

            return $verifyEmail->data[0]->id;
        }

        return false;
    }

    public function findCustomerByEmail($email)
    {
        return $this->stripe->customers->all([
            'email' => $email
        ]);
    }

    public function findCustomerByStripeId($customerId)
    {
        $errorMessage = '';

        try{
            $customer = $this->stripe->customers->retrieve(
                $customerId,
                []
            );
        }catch(Exception $error){

        }

        return [
            "customer" => $customer,
            "error" => $errorMessage
        ];
    }

    public function addCardToCustomer($customerId,$cardToken)
    {
        return $this->stripe->customers->createSource(
            $customerId,
            ['source' => $cardToken]
        );
    }

    public function charge($data)
    {
        return $this->stripe->charge->create([
            "amount" => $data->amount,
            "currency" => $data->currency,
            "description" => $data->description,
            "source" => $data->cardToken
        ]);
    }

    public function findChargeById($chargeId)
    {
        return $this->stripe->charges->retrieve(
            $chargeId,
            []
        );
    }

    public function generateTestCardToken()
    {
        $token = $this->stripe->tokens->create([
            'card' => [
                'number' => "4242424242424242",
                'exp_month' => '10',
                'exp_year' => '2026',
                'cvc' => '123',
            ],
        ]);

        return $token->id;
    }

    public function chargeOnHold($customerId,$data)
    {
        $charge = null;

        try {
            $errorMessage = '';

            $customer = $this->findCustomerByStripeId($customerId);
            
            if($customer["error"] !== "")
                return [
                    "charge" => null,
                    "error" => $customer["error"]
                ];

            $charge = $this->stripe->charges->create([
                "amount" => $data['amount'],
                "currency" => $data['currency'],
                "description" => $data['description'],
                "customer" => $customer["customer"]->id,
                "source" => $customer["customer"]->default_source,
                'capture' => false
            ]);
        } catch(\Stripe\Exception\CardException $e) {
            $errorMessage = 'Card Exception';
        } catch (\Stripe\Exception\RateLimitException $e) {
            $errorMessage = 'Too many requests made to the API too quickly';
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $errorMessage = 'Invalid parameters were supplied to Stripe API';
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $errorMessage = 'Authentication with Stripe API failed';
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $errorMessage = 'Network communication with Stripe failed';
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $errorMessage = 'Display a very generic error to the user, and maybe send yourself an email';
        } catch (Exception $e) {
            $errorMessage = 'Something else happened, completely unrelated to Stripe';
        }

        return [
            "charge" => $charge,
            "error" => $errorMessage
        ];
    }

    public function realeaseChargeOnHold($charge_id)
    {
        $errorMessage = '';

        $charge = null;

        try{
            $charge = $this->findChargeById($charge_id);

            $charge->capture();
        }catch(Exception $error){
            $errorMessage = 'Something wrong when try to release the money';
        }

        return [
            "charge" => $charge,
            "error" => $errorMessage
        ];
    }

    /////////////////////////////////////// PAYMENT INTENTS ////////////////////////////////////////////////////

    public function setDefaultPaymentMethod($paymentMethod)
    {
        $updateCustomerPaymentMethod = null;
        try {
            $errorMessage = '';

            $updateCustomerPaymentMethod = $this->stripe->customers->update(
                $paymentMethod['customer'],
                ['invoice_settings' => $paymentMethod['invoice_settings']]
            );

        } catch(\Stripe\Exception\CardException $e) {
            $errorMessage = 'Card Exception';
        } catch (\Stripe\Exception\RateLimitException $e) {
            $errorMessage = 'Too many requests made to the API too quickly';
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $errorMessage = 'Invalid parameters were supplied to Stripe API';
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $errorMessage = 'Authentication with Stripe API failed';
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $errorMessage = 'Network communication with Stripe failed';
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $errorMessage = 'Display a very generic error to the user, and maybe send yourself an email';
        } catch (Exception $e) {
            $errorMessage = 'Something else happened, completely unrelated to Stripe';
        }

        return [
            "updatedCustomerPaymentMethod" => $updateCustomerPaymentMethod,
            "error" => $errorMessage
        ];
    }

    public function updatePaymentMethod($paymentMethod)
    {
        $updatePaymentMethod = null;

        try {
            $errorMessage = '';

            $updatePaymentMethod = $this->stripe->paymentMethods->update(
                $paymentMethod['paymentMethod'],
                ['card' => $paymentMethod['card']]
            );

        } catch(\Stripe\Exception\CardException $e) {
            $errorMessage = 'Card Exception';
        } catch (\Stripe\Exception\RateLimitException $e) {
            $errorMessage = 'Too many requests made to the API too quickly';
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $errorMessage = 'Invalid parameters were supplied to Stripe API';
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $errorMessage = 'Authentication with Stripe API failed';
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $errorMessage = 'Network communication with Stripe failed';
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $errorMessage = 'Display a very generic error to the user, and maybe send yourself an email';
        } catch (Exception $e) {
            $errorMessage = 'Something else happened, completely unrelated to Stripe';
        }

        return [
            "paymentMethodUpdated" => $updatePaymentMethod,
            "error" => $errorMessage
        ];
    }

    public function detachPaymentMethod($paymentMethod)
    {
        $detachPaymentMethod = null;

        try {
            $errorMessage = '';

            $detachPaymentMethod = $this->stripe->paymentMethods->detach(
                $paymentMethod,
                []
            );

        } catch(\Stripe\Exception\CardException $e) {
            $errorMessage = 'Card Exception';
        } catch (\Stripe\Exception\RateLimitException $e) {
            $errorMessage = 'Too many requests made to the API too quickly';
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $errorMessage = 'Invalid parameters were supplied to Stripe API';
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $errorMessage = 'Authentication with Stripe API failed';
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $errorMessage = 'Network communication with Stripe failed';
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $errorMessage = 'Display a very generic error to the user, and maybe send yourself an email';
        } catch (Exception $e) {
            $errorMessage = 'Something else happened, completely unrelated to Stripe';
        }

        return [
            "paymentMethodDetached" => $detachPaymentMethod,
            "error" => $errorMessage
        ];
    }

    public function paymentMethods($customerId)
    {
        $paymentMethods = null;
        $defaultPaymentMethod = null;
        try {
            $errorMessage = '';

            $customer = $this->findCustomerByStripeId($customerId);

            if($customer["error"] !== "")
                return [
                    "paymentMethods" => null,
                    "error" => $customer["error"]
                ];

            $paymentMethods = $this->stripe->paymentMethods->all([
                "customer" => $customer["customer"]->id,
                'type' => 'card',
            ]);

            $defaultPaymentMethod = $customer['customer']['invoice_settings']['default_payment_method'];

        } catch(\Stripe\Exception\CardException $e) {
            $errorMessage = 'Card Exception';
        } catch (\Stripe\Exception\RateLimitException $e) {
            $errorMessage = 'Too many requests made to the API too quickly';
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $errorMessage = 'Invalid parameters were supplied to Stripe API';
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $errorMessage = 'Authentication with Stripe API failed';
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $errorMessage = 'Network communication with Stripe failed';
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $errorMessage = 'Display a very generic error to the user, and maybe send yourself an email';
        } catch (Exception $e) {
            $errorMessage = 'Something else happened, completely unrelated to Stripe';
        }

        return [
            "paymentMethods" => $paymentMethods,
            "defaultPaymentMethod" => $defaultPaymentMethod,
            "error" => $errorMessage
        ];
    }

    public function setupIntent($data): array
    {
        $intent = null;
        try {

            $errorMessage = '';

            $intent = $this->stripe->setupIntents->create([
                'customer' => $data['customer']
            ]);

        } catch(\Stripe\Exception\CardException $e) {
            $errorMessage = 'Card Exception';
        } catch (\Stripe\Exception\RateLimitException $e) {
            $errorMessage = 'Too many requests made to the API too quickly';
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $errorMessage = 'Invalid parameters were supplied to Stripe API';
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $errorMessage = 'Authentication with Stripe API failed';
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $errorMessage = 'Network communication with Stripe failed';
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $errorMessage = 'Display a very generic error to the user, and maybe send yourself an email';
        } catch (Exception $e) {
            $errorMessage = 'Something else happened, completely unrelated to Stripe';
        }

        if ($errorMessage !== '')
            return [
                "setupIntent" => null,
                "error" => $errorMessage
            ];

        return [
            "setupIntent" => $intent,
            "error" => $errorMessage
        ];
    }

    public function paymentIntentsWithConfirmation($data): array
    {
        $intent = null;
        try {

            $errorMessage = '';

            $intent = $this->stripe->paymentIntents->create([
                "amount" => 10000,
                "currency" => 'eur',
                'customer' => 'cus_JV7VuQtbmuZMwB',
                'payment_method' => 'card_1Is7GYHawkJRotrI0IY6v8QP',
                'off_session' => true,
                'confirm' => true

            ]);
        } catch (\Stripe\Exception\CardException $e) {
            // Error code will be authentication_required if authentication is needed
            echo 'Error code is:' . $e->getError()->code;
            $payment_intent_id = $e->getError()->payment_intent->id;
            $payment_intent = \Stripe\PaymentIntent::retrieve($payment_intent_id);
        } catch(\Stripe\Exception\CardException $e) {
            $errorMessage = 'Card Exception';
        } catch (\Stripe\Exception\RateLimitException $e) {
            $errorMessage = 'Too many requests made to the API too quickly';
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $errorMessage = 'Invalid parameters were supplied to Stripe API';
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $errorMessage = 'Authentication with Stripe API failed';
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $errorMessage = 'Network communication with Stripe failed';
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $errorMessage = 'Display a very generic error to the user, and maybe send yourself an email';
        } catch (Exception $e) {
            $errorMessage = 'Something else happened, completely unrelated to Stripe';
        }

        if ($errorMessage !== '')
            return [
                "paymentIntents" => null,
                "error" => $errorMessage
            ];

        return [
            "paymentIntents" => $intent,
            "error" => $errorMessage
        ];
    }

    public function paymentIntents($data): array
    {
        $intent = null;
        try {

            $errorMessage = '';

            $intent = $this->stripe->paymentIntents->create([
                "amount" => $data['amount'],
                "currency" => 'eur',
                'payment_method_types' => ['card'],
                'capture_method' => 'manual',

                'customer' => $data['customer'],
                'setup_future_usage' => 'off_session',


//                'off_session' => true,
//                'confirm' => true

//            "description" => 'payment intent',
            ]);
        } catch (\Stripe\Exception\CardException $e) {
            // Error code will be authentication_required if authentication is needed
            echo 'Error code is:' . $e->getError()->code;
            $payment_intent_id = $e->getError()->payment_intent->id;
            $payment_intent = \Stripe\PaymentIntent::retrieve($payment_intent_id);
        } catch(\Stripe\Exception\CardException $e) {
            $errorMessage = 'Card Exception';
        } catch (\Stripe\Exception\RateLimitException $e) {
            $errorMessage = 'Too many requests made to the API too quickly';
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $errorMessage = 'Invalid parameters were supplied to Stripe API';
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $errorMessage = 'Authentication with Stripe API failed';
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $errorMessage = 'Network communication with Stripe failed';
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $errorMessage = 'Display a very generic error to the user, and maybe send yourself an email';
        } catch (Exception $e) {
            $errorMessage = 'Something else happened, completely unrelated to Stripe';
        }

        if ($errorMessage !== '')
            return [
                "paymentIntents" => null,
                "error" => $errorMessage
            ];

        return [
            "paymentIntents" => $intent,
            "error" => $errorMessage
        ];
    }

    public function findPaymentIntentsById($paymentIntentsId)
    {
        return $this->stripe->paymentIntents->retrieve(
            $paymentIntentsId,
            []
        );
    }

    public function paymentIntentsOnHold($data)
    {
        $paymentIntents = null;

        try {
            $errorMessage = '';

            $customer = $this->findCustomerByStripeId($data['customer']);

            if($customer["error"] !== "")
                return [
                    "paymentIntents" => null,
                    "error" => $customer["error"]
                ];

            $paymentIntents = $this->stripe->paymentIntents->create([
                "amount" => $data['amount'],
                "currency" => $data['currency'],
                "description" => $data['description'],
                "customer" => $customer["customer"]->id,
//                "customer" => $data['customer'],
                "payment_method" => $data['payment_method'],
                "payment_method_types" => ['card'],
                "capture_method" => 'manual',
                'off_session' => true,
                'confirm' => true,
            ]);

        } catch(\Stripe\Exception\CardException $e) {
            $errorMessage = 'Card Exception';
        } catch (\Stripe\Exception\RateLimitException $e) {
            $errorMessage = 'Too many requests made to the API too quickly';
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $errorMessage = 'Invalid parameters were supplied to Stripe API';
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $errorMessage = 'Authentication with Stripe API failed';
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $errorMessage = 'Network communication with Stripe failed';
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $errorMessage = 'Display a very generic error to the user, and maybe send yourself an email';
        } catch (Exception $e) {
            $errorMessage = 'Something else happened, completely unrelated to Stripe';
        }

        return [
            "paymentIntents" => $paymentIntents,
            "error" => $errorMessage
        ];
    }

    public function realeasePaymentIntentsOnHold($paymentIntentsId)
    {
        $errorMessage = '';

        $paymentIntents = null;

        try{
            $paymentIntents = $this->findPaymentIntentsById($paymentIntentsId);

            $paymentIntents->capture();     //TO CHECK
        }catch(Exception $error){
            $errorMessage = 'Something wrong when try to release the money';
        }

        return [
            "paymentIntents" => $paymentIntents,
            "error" => $errorMessage
        ];
    }

}