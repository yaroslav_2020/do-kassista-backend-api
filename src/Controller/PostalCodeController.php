<?php 

namespace App\Controller;

use App\Application\PostalCode\PostalService;
use App\Entity\PostalCode;
use App\Repository\PostalCodeRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Swagger\Serializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer as SerializerSerializer;

/**
 * Class PostalCodeController
 * @package App\Controller
 */
class PostalCodeController extends AbstractController
{

    private $postalCodeRepository;
    private $postalService;

    public function __construct(
        PostalCodeRepository $postalCodeRepository,
        PostalService $postalService
    )
    {
        $this->postalService = $postalService;
        $this->postalCodeRepository = $postalCodeRepository;
    }
    
    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param PostalService $postalService
     * @param PostalRepository $postalRepository
     */
    public function addNewPostalCode(
        Request $request,
        EntityManagerInterface $entityManager,
        PostalCodeRepository $postalRepository,
        PostalService $postalService
    ){
        $data = @json_decode($request->getContent(), true);

        $postalService->addPostalCode(
            $data['title'],
            $data['code'],
            $data['zoneName'],
            $data['address']
        );

        $postalRepository = $entityManager->getRepository(PostalCode::class);
        $postal = $postalRepository->findUsingCode($data['code']);
    

       if (!($postal instanceof PostalCode)) {
              throw new \RuntimeException("Postal Code not fond after adding process was successful");
            }

            $postals = [
                "title" => $postal->getTitle(),
                "code" => $postal->getCode(),
                "zoneName" => $postal->getZoneName(),
                "address" => $postal->getAddress(),

        ];
               
            return new JsonResponse([
                'status' => ApiResponseCodesInterface::STATUS_OK
            ]);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function removePostalCode(
        int $id
    ){
        $postal = $this->postalCodeRepository->findUsingId($id);

        if (!($postal instanceof postalCode)) {
            throw new \RuntimeException("Postal Code not found after the searching");
        }

        $this->postalService->removePostalCode($postal);

        
        return new JsonResponse([
            'status' => ApiResponseCodesInterface::STATUS_OK
        ]);  
    }

    public function update(){

    }

    public function getItemPostalCode(
        int $id,
        PostalCode $postalCode
    ){

        $postal = $this->postalCodeRepository->findAllUsingId($id);     
        
        $data = array(
                    'id'        => $postalCode->getId(),
                    'title'     => $postalCode->getTitle(),
                    'code'      => $postalCode->getCode(),
                    'zoneName'  => $postalCode->getZoneName(),
                    'address'   => $postalCode->getAddress());

        $encode = json_encode($data);

        return new JsonResponse([
            'status' => ApiResponseCodesInterface::STATUS_OK,
            'data' => $encode
        ]);
    }
}