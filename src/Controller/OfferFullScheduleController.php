<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Entity\OfferFullSchedule;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Repository\OfferFullScheduleRepository;

use App\Repository\SettingsRepository;

class OfferFullScheduleController
{
    private $offerFullScheduleRepository;
    private $days;
    private $settingsRepository;

    public function __construct(
        OfferFullScheduleRepository $offerFullScheduleRepository,
        SettingsRepository $settingsRepository
    )
    {
        $this->offerFullScheduleRepository = $offerFullScheduleRepository;
        $this->settingsRepository = $settingsRepository;
        $this->days = [
            "mon" => "MONDAY",
            "tue" => "TUESDAY",
            "wed" => "WEDNESDAY",
            "thu" => "THURSDAY",
            "fri" => "FRIDAY",
            "sat" => "SATURDAY",
            "sun" => "SUNDAY",
        ];
    }

    /**
     * @throws \Exception
     */
    public function createOfferFullSchedule($offer)
    {

        $periodPoint = $offer->getPeriodPoints()[0];
        $startHour = $periodPoint->getStartHour();
        $endHour = $periodPoint->getEndHour();

        $daysData = $this->handlePeriodPointsDays($offer->getPeriodPoints());
        $schedule = $this->calculateSchedule($offer->getStartFromDate(),$offer->getEndDate(),$daysData, $startHour, $endHour);
        foreach($schedule as $day){
            $offerFullSchedule = new OfferFullSchedule();
            $offerFullSchedule->setOffer($offer);
            $offerFullSchedule->setWorkerOfferStatus(OfferFullSchedule::STATUS_PENDING);
            $offerFullSchedule->setClientOfferStatus(OfferFullSchedule::STATUS_PENDING);
            $offerFullSchedule->setDateTimeStart($day['startHour']);
            $offerFullSchedule->setDateTimeEnd($day['endHour']);
            $offerFullSchedule->setDateTimeUpdated(new \DateTime('now', new \DateTimeZone("UTC")));
            $offerFullSchedule->setPricePerDay($offer->getPricePerDay());
            $offerFullSchedule->setPaymentStatus(OfferFullSchedule::PAYMENT_STATUS_PENDING);
            $this->offerFullScheduleRepository->insert($offerFullSchedule);
        }
    }
    
    public function calculateOfferFullScheduleFromOffer($offer)
    {
        $daysData = $this->handlePeriodPointsDays($offer->getPeriodPoints());

        $periodPoint = $offer->getPeriodPoints()[0];

        $startHour = $periodPoint->getStartHour();
        $endHour = $periodPoint->getEndHour();

        return $this->calculateSchedule($offer->getStartFromDate(),$offer->getEndDate(),$daysData, $startHour, $endHour);
    }

    public function handlePeriodPointsDays($periodPoints)
    {
        $days = [];
        $daysSchedule = [];

        for($i = 0; $i < count($periodPoints); $i++){
            $days[] = $periodPoints[$i]->getDay();
            $daysSchedule[$periodPoints[$i]->getDay()] = [
                'start' => $periodPoints[$i]->getStartHour(),
                'end' => $periodPoints[$i]->getEndHour(),
            ];
        }

        return [
            "days" => $days,
            "schedule" => $daysSchedule
        ];
    }

    /**
     * @throws \Exception
     */
    public function calculateOfferFullSchedule($data){

        $schedule = [];

        foreach($data->days as $day)
            $schedule[$day] = [
                'start' => $data->startHour,
                'end' => $data->endHour,
            ];
        
        $daysData = ["days" => $data->days,"schedule" => $schedule];

        $startFromDate = new \DateTime($data->startFromDate);
        $endDate = new \DateTime($data->endDate);

        $registers = $this->calculateSchedule($startFromDate,$endDate,$daysData,$data->startHour,$data->endHour);

        if(!$this->isNextDayAvailable($startFromDate,$daysData,$data->startHour,$data->endHour))
            array_shift($registers);

        return $registers;
    }

    public function calculateSchedule($start,$final,$daysData,$startHour,$endHour)
    {
        $settings = $this->getSettings();

        if($start instanceof \DateTime && $final instanceof \DateTime){
            if($start < $final){

                $start->setTime(0,0,0);                
                $final->setTime(0,0,0);

                $final = $final->modify('+1 day');

                $interval = \DateInterval::createFromDateString('1 day');
                $period = new \DatePeriod($start, $interval, $final);     

                $registers = [];

                $startServiceHour = $this->createDateTimeToCompare($startHour);
                $endServiceHour = $this->createDateTimeToCompare($endHour);

                $endLaborSchedule = $this->createDateTimeToCompare($settings['end_labor_schedule']);
                $startLaborSchedule = $this->createDateTimeToCompare($settings['start_labor_schedule']);

                if($startServiceHour >= $startLaborSchedule && $endServiceHour < $endLaborSchedule){
                    foreach ($period as $day) {
                        if(in_array(strtoupper($day->format('l')),$daysData['days'])){

                            $endHour = $this->handleHourStringToDatetime($daysData['schedule'][strtoupper($day->format('l'))]['end'],$day);
                            $startHour = $this->handleHourStringToDatetime($daysData['schedule'][strtoupper($day->format('l'))]['start'],$day);
                            
                            $registers[] = [

                                "startHour" => $startHour,
                                "endHour" => $endHour,
                            ];
                            
                        }            
                    }
                }                

                return $registers;
            }
        }

        return [];
    }

    private function handleHourStringToDatetime($stringHour,$day)
    {
        $hourSplit = explode(':',$stringHour);
        $date = clone $day;

        return $date->setTime(intval($hourSplit[0]),intval($hourSplit[1]));
    }

    /**
     * @throws \Exception
     */
    private function isNextDayAvailable($startFromDate, $daysData, $startHour, $endHour)
    {
        $settings = $this->getSettings();

        $startServiceHour = $this->createDateTimeToCompare($startHour);
        $endServiceHour = $this->createDateTimeToCompare($endHour);
        
        $tomorrow = $this->calculateNextDayFromDate(new \DateTime('now', new \DateTimeZone("UTC"))); //use utc new \DateTime('now', new \DateTimeZone("UTC"))

        $endLaborSchedule = $this->createDateTimeToCompare($settings['end_labor_schedule']);
        $startLaborSchedule = $this->createDateTimeToCompare($settings['start_labor_schedule']);

        if($startServiceHour >= $startLaborSchedule && $endServiceHour < $endLaborSchedule){
            //if the next day of today is the start day

            $tomorrow->setTime(0,0,0);
            $startFromDate->setTime(0,0,0);

            if($tomorrow == $startFromDate){

                //at this point we know that tomorrow is the start day of the service
                //now we have to know if the weekday of tomorrow was selected in the schedule

                $weekDayTomorrow = strtoupper($this->days[strtolower($tomorrow->format('D'))]);

                if(in_array($weekDayTomorrow,$daysData['days'])){
                    //at this point we know that the weekday of tomorrow was selected in the schedule

                    //Check if today we have enough time to eneable all the hours of tomorrow
                    
                    $saveToday = $this->incrementHours(new \DateTime('now', new \DateTimeZone("UTC")),floatval($settings['hours_buffer'])); //use utc for datetime as line 132
                    
                    
                    if($saveToday > $endLaborSchedule){
                        //If today we don't have enough time we need to check the hours available for tomorrow

                        //We have to define the save hours for tomorrow
                        $saveStartTomorrow = $this->createDateTimeToCompare($settings['start_labor_schedule']);
                        $saveStartTomorrow = $this->incrementHours($saveStartTomorrow,floatval($settings['hours_buffer']));

                        if(!($startServiceHour >= $saveStartTomorrow && $endServiceHour < $endLaborSchedule)){
                            //In this case the service can not be tomorrow so we skip it
                            return false;
                        }else{
                            //if the interval of hours selected by the user is in the save interval for tomorrow, we can make the service
                        }
                    }else{
                        //If today we have enough time yet, the service can be tomorrow at any hour
                    }
                }
            }
        }        

        return true;
    }

    /**
     * @throws \Exception
     */
    public function incrementHours($date, $increment){
        $time = new \DateTime($date->format('Y-m-d H:m:s'),new \DateTimeZone("UTC"));

        $minutes = $increment * 60;
        $hours = intval($minutes / 60);
        $minutes = $minutes % 60;

        $time->add(new \DateInterval("PT{$hours}H{$minutes}M"));   


        return $time;
    }

    /**
     * @throws \Exception
     */
    public function createDateTimeToCompare($time) //08:00
    {
        $date = new \DateTime('now', new \DateTimeZone("UTC")); //use utc
        
        $split = explode(':',$time);
        
        if(count($split) === 2){
            $date->setTime(intval($split[0]),intval($split[1]));
        }

        return $date;
    }

    /**
     * @throws \Exception
     */
    private function calculateNextDayFromDate($date)
    {
        $nextDay = new \DateTime($date->format('Y-m-d H:m:s'), new \DateTimeZone("UTC"));
        $nextDay->add(new \DateInterval('P1D'));

        return $nextDay;
    }

    private function getSettings()
    {
        $settingsObjectArray = $this->settingsRepository->getSettingsForOffferFullSchedule();
        $settings = [];

        foreach($settingsObjectArray as $setting){
            $settings[$setting->getName()] = $setting->getValue();
        }

        return $settings;
    }
}