<?php


namespace App\DataFixtures;

use App\Entity\Faq;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class FaqFixtures
 * @package App\DataFixtures
 */
class FaqFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $faq = $manager->getRepository(Faq::class)->findOneBy(['id' => $i]);
            if ($faq instanceof Faq) {
                continue;
            }
            $faq = Faq::create($i, "Question " . $i, "Answer " . $i);
            $manager->persist($faq);
            $manager->flush();
        }
    }
}