<?php


namespace App\Command;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class OfferRequestWorkersCommand
 * @package App\Command
 */
class OfferRequestWorkersCommand extends Command
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * OfferRequestWorkersCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param string|null $name
     */
    public function __construct(EntityManagerInterface $entityManager, string $name = null)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName("app:offer:workers:request");
    }

    /**
     * Initializes the command after the input has been bound and before the input
     * is validated.
     *
     * This is mainly useful when a lot of commands extends one main command
     * where some things need to be initialized based on the input arguments and options.
     *
     * @see InputInterface::bind()
     * @see InputInterface::validate()
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void null or 0 if everything went fine, or an error code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Requesting workers...");

        //select all offers with status in negociation and eligible to be assigned to workers
        //send email with accept offers to all workers
        //todo accept the first worker or gather them in a list to request approval from ceo?
        //assign worker to offer todo here should he know all workers assigned to an offer? like one gets sick we need another. List all workers ever assigned to an offer?
    }
}