<?php

declare(strict_types=1);

namespace App\Command;

use App\Application\Security\SecurityService;
use App\Application\Translation\Dumper\CatalogDumper;
use App\Entity\AclAction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ImportAclActionsCommand
 * @package App\Command
 */
class ImportAclActionsCommand extends Command
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * ImportTranslationCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param string|null $name
     * @param CatalogDumper $catalogDumper
     */
    public function __construct(EntityManagerInterface $entityManager, string $name = null, CatalogDumper $catalogDumper = null)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName("app:aclaction:update");
    }

    /**
     * Initializes the command after the input has been bound and before the input
     * is validated.
     *
     * This is mainly useful when a lot of commands extends one main command
     * where some things need to be initialized based on the input arguments and options.
     *
     * @see InputInterface::bind()
     * @see InputInterface::validate()
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
    }

    /**
     * Interacts with the user.
     *
     * This method is executed before the InputDefinition is validated.
     * This means that this is the only place where the command can
     * interactively ask for values of missing required arguments.
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void null or 0 if everything went fine, or an error code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Importing translations...");
        $actions = SecurityService::getAclListFixture();
        foreach ($actions as $actionCategory => $actionList) {
            foreach ($actionList as $actionConfig) {
                list ($actionId, $actionPrettryName) = $actionConfig;
                $actionEntity = $this->entityManager->getRepository(AclAction::class)->findOneBy([
                    'category' => $actionCategory,
                    "actionId" => $actionId,
                    "actionPrettyName" => $actionPrettryName
                ]);

                if ($actionEntity instanceof AclAction) {
                    $output->writeln("<info>Already exists action: $actionCategory =>  $actionId </info>");
                    continue;
                }

                $actionEntity = new AclAction();
                $actionEntity->setCategory($actionCategory);
                $actionEntity->setActionId($actionId);
                $actionEntity->setActionPrettyName($actionPrettryName);

                $this->entityManager->persist($actionEntity);
                $this->entityManager->flush();

                $output->writeln("<info>Created new action: $actionCategory =>  $actionId </info>");
            }
        }
    }
}