<?php

declare(strict_types=1);

namespace App\Command;


use App\Application\Translation\Dumper\CatalogDumper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ImportTranslationCommand
 * @package App\Command
 */
class ImportTranslationCommand extends Command
{
    /** @var CatalogDumper */
    protected $catalogDumper;

    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * ImportTranslationCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param string|null $name
     * @param CatalogDumper $catalogDumper
     */
    public function __construct(EntityManagerInterface $entityManager, string $name = null, CatalogDumper $catalogDumper = null)
    {
        parent::__construct($name);
        $this->catalogDumper = $catalogDumper;
        $this->entityManager = $entityManager;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName("app:translation:import");
    }

    /**
     * Initializes the command after the input has been bound and before the input
     * is validated.
     *
     * This is mainly useful when a lot of commands extends one main command
     * where some things need to be initialized based on the input arguments and options.
     *
     * @see InputInterface::bind()
     * @see InputInterface::validate()
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
    }

    /**
     * Interacts with the user.
     *
     * This method is executed before the InputDefinition is validated.
     * This means that this is the only place where the command can
     * interactively ask for values of missing required arguments.
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @return int|null null or 0 if everything went fine, or an error code
     *
     * @throws LogicException When this abstract method is not implemented
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Importing translations...");
        $import = $this->catalogDumper->dump();
        $output->writeln(sprintf(
            "<info>Updated %s, imported as new %s</info>",
            $import['updated'],
            $import['new']
        ));
    }
}