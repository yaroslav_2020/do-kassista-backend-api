<?php

declare(strict_types=1);

namespace App\Command;

use App\Application\Security\SecurityService;
use App\Application\Translation\Dumper\CatalogDumper;
use App\Entity\AclAction;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GrandAdminAclActionCommand
 * @package App\Command
 */
class GrandAdminAclActionCommand extends Command
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var SecurityService */
    protected $securityService;

    /**
     * ImportTranslationCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param string|null $name
     * @param SecurityService $securityService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        string $name = null,
        SecurityService $securityService = null
    ) {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->securityService = $securityService;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName("app:aclaction:grand")
            ->addArgument("userId");
    }

    /**
     * Initializes the command after the input has been bound and before the input
     * is validated.
     *
     * This is mainly useful when a lot of commands extends one main command
     * where some things need to be initialized based on the input arguments and options.
     *
     * @see InputInterface::bind()
     * @see InputInterface::validate()
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
    }

    /**
     * Interacts with the user.
     *
     * This method is executed before the InputDefinition is validated.
     * This means that this is the only place where the command can
     * interactively ask for values of missing required arguments.
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void null or 0 if everything went fine, or an error code
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userId = $input->getArgument("userId");
        if (empty($userId)) {
            throw new \InvalidArgumentException("Empty user id");
        }

        $user = $this->entityManager->getRepository(User::class)->findOneBy(["id" => $userId]);
        if (!($user instanceof User)) {
            throw new \InvalidArgumentException("user not found");
        }

        $this->securityService->grandAllActionsToUser($user);
        $output->writeln("<info>Granted</info>");
    }
}