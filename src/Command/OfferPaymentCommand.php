<?php

namespace App\Command;

use App\Repository\OfferRepository;
use App\Repository\OfferFullScheduleRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\SerializerInterface;
use App\Controller\OfferFullScheduleController;
use App\Controller\StripeController;
use Symfony\Component\Console\Question\Question;
use App\Entity\Settings;
use App\Repository\SettingsRepository;
use App\Repository\PaymentRepository;
use App\Application\Mailer\AuthMailer;

class OfferPaymentCommand extends Command
{
    private $offerRepository;
    private $offerFullSchedule;
    private $serializer;
    private $offerFullScheduleController;
    private $stripe;
    private $bufferTimeToBlock;
    private $bufferTimeToRelease;
    private $paymentRepository;
    private $email;

    protected static $defaultName = 'app:offer-payment';

    public function __construct(
        OfferRepository $offerRepository,
        OfferFullScheduleRepository $offerFullSchedule,
        SerializerInterface $serializer,
        OfferFullScheduleController $offerFullScheduleController,
        StripeController $stripe,
        SettingsRepository $settingsRepository,
        PaymentRepository $paymentRepository,
        AuthMailer $email
    ) {
        $this->email = $email;
        $this->offerRepository = $offerRepository;
        $this->offerFullSchedule = $offerFullSchedule;
        $this->serializer = $serializer;
        $this->offerFullScheduleController = $offerFullScheduleController;
        $this->stripe = $stripe;
        $this->stripe->setStripeKey($_ENV['STRIPE_KEY']);
        $this->paymentRepository = $paymentRepository;
        $this->bufferTimeToBlock = NULL !== $settingsRepository->getSetting(Settings::time_buffer_to_block_iteration);
        $this->bufferTimeToRelease = NULL !== $settingsRepository->getSetting(Settings::time_buffer_to_realease_blocked_payment);

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName("app:offer-payment")
            ->setDescription('Offer payments via Stripe')
            ->setDefinition(array(
                new InputArgument('block_payment', InputArgument::REQUIRED, 'Have to block the Payment?'),
                new InputArgument('dry_run', InputArgument::REQUIRED, 'Do you want to set dry_run on?')

            ))
            ->setHelp('This command block or make the payments of service days');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // pending - assigned - in_negotiation
        // get status by multiple status
        $offers = $this->offerRepository->getOffersByStatus('pending');

        foreach ($offers as $offer) {
            if ($input->getArgument('block_payment')) {
                $schedule = $this->offerFullSchedule->getDaysToBlockPayment($offer);

                $this->blockingPayments($offer, $schedule, $input->getArgument('dry_run'), $output);
            } else {
                $schedule = $this->offerFullSchedule->getDaysToPay($offer);
                $fullSchedule = $this->offerFullScheduleController->calculateOfferFullScheduleFromOffer($offer);

                $lastDay = $fullSchedule[count($fullSchedule) - 1];
                $lastDate =  $this->handleStringDateTime($lastDay['startHour']->format('d-m-Y 0:0:0'));

                $this->makingCharges($offer, $schedule, $lastDate, $input->getArgument('dry_run'), $output);
            }
        }


        return 0;
    }

    public function handleStringDateTime($string)
    {
        return new \DateTime($string, new \DateTimeZone("UTC"));
    }

    private function blockingPayments($offer, $schedule, $dryRun, $output)
    {

        $output->writeln(
            sprintf(
                "<info>--------> Processing order # %s, to block amount</info>",
                $offer->getId()
            )
        );
        foreach ($schedule as $day) {

            $output->writeln(
                sprintf(
                    "<info>--------> Processing day: %s - %s, to block amount</info>",
                    $day->getDateTimeStart()->format('d/m/Y'),
                    $day->getDateTimeEnd()->format('d/m/Y')
                )
            );

            $timeUpdated = $day->getDateTimeUpdated();
            $timeStart = $day->getDateTimeStart();
            $today = new \DateTime('now', new \DateTimeZone("UTC"));

            //skip it for production
            if ($dryRun) {
                $today = $this->offerFullScheduleController->incrementHours($today, 24 * 365);
            }

            //if time start is not null then bufferTimeToBlock hours after time start 
            if ($timeStart instanceof \DateTime) {
                //if now is bigger than timeStart plus bufferTimeToBlock hours, block the money
                $timeToBlock = $this->offerFullScheduleController->incrementHours($timeStart, floatval($this->bufferTimeToBlock->getValue()));
            } else if ($timeUpdated instanceof \DateTime) {
                //if now is bigger than timeUpdated plus bufferTimeToBlock hours, block the money
                $timeToBlock = $this->offerFullScheduleController->incrementHours($timeUpdated, floatval($this->bufferTimeToBlock->getValue()));
            }

            if ($today >= $timeToBlock) {
                $client = $offer->getClient()[0];

                $output->writeln(
                    sprintf(
                        "<info>--------> Customer ID: %s - Payment Method: %s</info>",
                        $client->getCustomerStripeId(),
                        $client->getActivePaymentMethod()
                    )
                );

                $paymentData = [
                    'amount' => floatval($offer->getPricePerDay()) * 100,
                    'currency' => 'eur',
                    'description' => 'Offer ' . $offer->getId() . ' service day payment - ' . $day->getDateTimeStart()->format('d/m/Y'), //put the date dd-mm-YYYY  01/07/2021
                    'customer' => $client->getCustomerStripeId(),
                    'payment_method' => $client->getActivePaymentMethod()
                ];

                $paymentIntent = $this->stripe->paymentIntentsOnHold($paymentData);

                if ($paymentIntent["error"] !== "") {
                    $this->offerFullSchedule->updateBlockedMoneyStatus($day, $paymentIntent["paymentIntents"], true); //send email to the unsuccessful status
                } else {

                    $output->writeln(
                        sprintf(
                            "<info>--------> Amount Blocked with paymentIntent ID: %s</info>",
                            $paymentIntent["paymentIntents"]->id
                        )
                    );

                    $this->offerFullSchedule->updateBlockedMoneyStatus($day, $paymentIntent["paymentIntents"]);
                    $this->email->sendPaymentBlockedEmailMessage(
                        $client,
                        floatval($offer->getPricePerDay()),
                        $offer->getId(),
                        $offer->getTimestapableCreated()->format('d/m/Y'),
                        $day->getDateTimeStart()->format('d/m/Y'),
                        $day->getDateTimeStart()->format('H:s'),
                        $day->getDateTimeEnd()->format('H:s')
                    );
                }
            }
            sleep(1);
        }
    }

    /**
     * @throws \Exception
     */
    private function makingCharges($offer, $schedule, $lastDate, $dryRun, $output)
    {

        $output->writeln(
            sprintf(
                "<info>--------> Processing order # %s, to release charges</info>",
                $offer->getId()
            )
        );

        foreach ($schedule as $day) {

            $output->writeln(
                sprintf(
                    "<info>--------> Processing day: %s - %s, to release charge</info>",
                    $day->getDateTimeStart()->format('d-m-Y 0:0:0'),
                    $day->getDateTimeEnd()->format('d-m-Y 0:0:0')
                )
            );

            $timeUpdated = $day->getDateTimeUpdated();
            $dateTimeEnd = $day->getDateTimeEnd();

            $serviceDay = $day->getDateTimeStart();
            $serviceDay = $this->handleStringDateTime($serviceDay->format('d-m-Y 0:0:0'));

            $today = new \DateTime('now', new \DateTimeZone("UTC"));

            //skip it for production
            if ($dryRun) {
                $today = $this->offerFullScheduleController->incrementHours($today, 24 * 365);
            }

            //if time updated is not null then 24 hours after time updated
            if ($timeUpdated instanceof \DateTime) {
                //if now is bigger than timeUpdated plus 24 hours, make the charge 
                $timeToCharge = $this->offerFullScheduleController->incrementHours($timeUpdated, floatval($this->bufferTimeToRelease->getValue()));
            } else if ($dateTimeEnd instanceof \DateTime) {
                //in this case we need to verify if today is bigger than the endDate plus 24 hours
                $timeToCharge = $this->offerFullScheduleController->incrementHours($dateTimeEnd, floatval($this->bufferTimeToRelease->getValue()));
            }
            $output->writeln("Today: " . $today->format('d-m-Y 0:0:0'));
            $output->writeln("Time To Charge: " . $timeToCharge->format('d-m-Y 0:0:0'));

            if ($today >= $timeToCharge) {
                $chargeBlocked = $day->getChargeId();
                $response = $this->stripe->realeasePaymentIntentsOnHold($chargeBlocked);
                $client = $offer->getClient()[0];

                if ($response['error'] === '' && $response['paymentIntents'] !== null) {

                    $output->writeln(
                        sprintf(
                            "<info>--------> Charge on paymentIntents Released for Date: %s</info>",
                            $day->getDateTimeStart()->format('d/m/Y')
                        )
                    );

                    $this->offerFullSchedule->updateCompletedStatus($day);
                    $this->email->sendPaymentReleaseEmailMessage(
                        $client,
                        floatval($offer->getPricePerDay()),
                        $offer->getId(),
                        $offer->getTimestapableCreated()->format('d/m/Y'),
                        $day->getDateTimeStart()->format('d/m/Y'),
                        $day->getDateTimeStart()->format('H:s'),
                        $day->getDateTimeEnd()->format('H:s')
                    );

                    if ($lastDate == $serviceDay) {   //Last Transaction is set in true if the day is the last day
                        $this->offerRepository->updateStatusCompleted($offer);
                    }
                    $data = [
                        "status" => $response['paymentIntents']->status,
                        "date" => new \DateTime('now', new \DateTimeZone("UTC")),
                        "code" => $response['paymentIntents']->id,
                        "amount" => floatval($offer->getPricePerDay()),
                        //                        "payment_url" => $response['paymentIntents']->charges->data[0]->receipt_url, //If not ready by thursday, let it in blank
                        "payment_url" => "", //If not ready by thursday, let it in blank
                        "currency" => $response['paymentIntents']->currency, //EUR
                        "description" => 'Offer ' . $offer->getId() . ' service day payment - ' . $day->getDateTimeStart()->format('d/m/Y'),
                        "last_transaction" => $lastDate == $serviceDay,     //Update with the LastDate to true
                        "card_token" => $response['paymentIntents']->payment_method
                        //                            "card_token" => $client->getActiveCardToken(),       //FROM USER get the token
                    ];

                    $this->paymentRepository->savePaymentDetails($offer, $data);
                } else {
                    $this->offerFullSchedule->updateCompletedStatus($day, true); //send the email for unsuccess status
                }
            }
            sleep(1);
        }
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = array();

        if (!$input->getArgument('block_payment')) {
            $question = new Question('Block payments? [Y/n]:');

            $question->setValidator(function ($entity) {
                if (empty($entity)) {
                    throw new \Exception('Block payment cannot be empty');
                }

                $entity = strtolower($entity);

                if ($entity !== 'y' && $entity !== 'n')
                    throw new \Exception('Choose between Y or N');

                return $entity === 'y';
            });
            $questions['block_payment'] = $question;
        }

        //Create a new interact for dryRun Argument

        if (!$input->getArgument('dry_run')) {
            $question = new Question('Want Dry Run mode on? [Y/n]:');

            $question->setValidator(function ($entity) {
                if (empty($entity)) {
                    throw new \Exception('Dry Run cannot be empty');
                }

                $entity = strtolower($entity);

                if ($entity !== 'y' && $entity !== 'n')
                    throw new \Exception('Choose between Y or N');

                return $entity === 'y';
            });
            $questions['dry_run'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }
}
