<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Currency\MoneyCurrency;
use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * Class PostalCode
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="postal_code")
 * @ORM\Entity(repositoryClass="App\Repository\PostalCodeRepository")
 */
#[ApiResource]
class PostalCode
{
    use TraitTimestampableCreated, TraitTimestapableUpdated;

    const ALLOW_OFFER_ALLOWED = 1;
    const ALLOW_OFFER_DENIED = 0;

    /**
     * Post code status
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const PREFERRED_PRICE_NOT_SET = -1;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"show_address","show:offer:billing","admin:view:acl","show:companies"})
     */
    protected $id;

    /**
     * implemented optimistic offline locking
     * @var integer
     * @ORM\Version()
     * @ORM\Column(type="integer")
     */
    private $version = 0;

    /**
     *
     * @ORM\Column(type="string", length=255, name="title")
     * @Groups({"show_address","show:offer:billing","admin:view:acl","show:companies"})
     */
    protected $title;

    /**
     *
     * @ORM\Column(type="float", name="preferred_price", options={"defaults": -1})
     * @Groups({"show_address","show:offer:billing","admin:view:acl"})
     */
    protected $preferredPrice = self::PREFERRED_PRICE_NOT_SET;

    /**
     *
     * @ORM\Column(type="string", name="currency", length=10)
     * @Groups({"show_address","show:offer:billing","admin:view:acl"})
     */
    protected $currencyPreferredPrice = MoneyCurrency::EUR;

    /**
     *
     * @ORM\Column(type="string", length=255, name="code")
     * @Groups({"show_address","show:offer:billing","admin:view:acl","show:companies"})
     */
    protected $code;

    /**
     *
     * @ORM\Column(type="string", length=255, name="zone_name")
     * @Groups({"show_address","show:offer:billing","admin:view:acl"})
     */
    protected $zoneName;

    /**
     *
     * @ORM\Column(type="text", name="address", nullable=true)
     * @Groups({"show_address","show:offer:billing","admin:view:acl"})
     */
    protected $address;

    /**
     * @ORM\Column(type="integer", name="allow_offer", options={"defaults": 1})
     * @Groups({"show_address","show:offer:billing","admin:view:acl"})
     *
     */
    protected $allowOffer = self::ALLOW_OFFER_ALLOWED;

    /**
     *
     * @ORM\Column(type="integer", name="active_status")
     * @Groups({"show_address","show:offer:billing","admin:view:acl"})     *
     */
    protected $activeStatus = self::STATUS_ACTIVE;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Address", mappedBy="postalCode")
     */
    protected $addressesLinked;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PostalCodeGroup", inversedBy="postalCode",cascade={"persist"})
     * @Groups({"show_address","show:offer:billing","admin:view:acl"})
     */
    private $postalCodeGroup;

    /**
     * PostalCode constructor.
     */
    public function __construct()
    {
        $this->addressesLinked = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version): void
    {
        $this->version = $version;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getAllowOffer(): int
    {
        return $this->allowOffer;
    }

    /**
     * @param int $allowOffer
     */
    public function setAllowOffer(int $allowOffer): void
    {
        $this->allowOffer = $allowOffer;
    }

    /**
     * @return int
     */
    public function getActiveStatus(): int
    {
        return $this->activeStatus;
    }

    /**
     * @param int $activeStatus
     */
    public function setActiveStatus(int $activeStatus): void
    {
        $this->activeStatus = $activeStatus;
    }

    /**
     * @return mixed
     */
    public function getZoneName()
    {
        return $this->zoneName;
    }

    /**
     * @param mixed $zoneName
     */
    public function setZoneName($zoneName): void
    {
        $this->zoneName = $zoneName;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return float
     */
    public function getPreferredPrice(): float
    {
        return $this->preferredPrice;
    }

    /**
     * @param int $preferredPrice
     */
    public function setPreferredPrice(int $preferredPrice): void
    {
        $this->preferredPrice = $preferredPrice;
    }

    /**
     * @return string
     */
    public function getCurrencyPreferredPrice(): string
    {
        return $this->currencyPreferredPrice;
    }

    /**
     * @param string $currencyPreferredPrice
     */
    public function setCurrencyPreferredPrice(string $currencyPreferredPrice): void
    {
        $this->currencyPreferredPrice = $currencyPreferredPrice;
    }

    public function getPostalCodeGroup(): ?PostalCodeGroup
    {
        return $this->postalCodeGroup;
    }

    public function setPostalCodeGroup(?PostalCodeGroup $postalCodeGroup): self
    {
        $this->postalCodeGroup = $postalCodeGroup;

        return $this;
    }
}
