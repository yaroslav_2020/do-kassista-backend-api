<?php


declare(strict_types=1);

namespace App\Entity;


use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * Class OfferPointService
 * @package App\Entity
 * @ORM\Table(name="offer_point_service")
 * @ORM\Entity()
 */
class OfferPointService
{
    use TraitTimestampableCreated, TraitTimestapableUpdated;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"offer:view","user:offers"})
     */
    protected $id;

    /**
     * implemented optimistic offline locking
     * @var integer
     * @ORM\Version()
     * @ORM\Column(type="integer")
     * @Groups({"offer:view","user:offers"})
     */
    private $version = 0;

    /**
     * @var ExtraService
     *
     * Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="App\Entity\ExtraService", inversedBy="pointServices", cascade={"persist"})
     * @ORM\JoinColumn(name="extra_services_id", referencedColumnName="id")
     * @Groups({"offer:view","client:create","user:offers"})
     */
    private $extraService;

    /**
     * @var OfferPeriodPoint
     * @ORM\ManyToOne(targetEntity="App\Entity\OfferPeriodPoint", inversedBy="pointServices",cascade={"persist"})
     * @Groups({"client:create"})
     */
    private $offerPeriodPoint;

    /**
     * @ORM\Column(type="float")
     * @Groups({"offer:view","client:create","user:offers"})
     */
    private $hours;

    /**
     * OfferPointService constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version): void
    {
        $this->version = $version;
    }

    /**
     * @return ExtraService
     */
    public function getExtraService(): ExtraService
    {
        return $this->extraService;
    }

    /**
     * @param ExtraService $extraService
     */
    public function setExtraService(ExtraService $extraService): void
    {
        $this->extraService = $extraService;
    }

    public function getOfferPeriodPoint(): ?OfferPeriodPoint
    {
        return $this->offerPeriodPoint;
    }

    public function setOfferPeriodPoint(?OfferPeriodPoint $offerPeriodPoint): self
    {
        $this->offerPeriodPoint = $offerPeriodPoint;

        return $this;
    }

    public function getHours(): ?float
    {
        return $this->hours;
    }

    public function setHours(float $hours): self
    {
        if (!is_float($hours) || $hours <= 0) {
            throw new \RuntimeException("Invalid hours: $hours");
        }

        $this->hours = $hours;

        return $this;
    }
}