<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Currency\MoneyCurrency;
use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Form\Tests\Extension\Core\Type\Money;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * Class ExtraService
 * @package App\Entity
 * @ORM\Table(name="extra_service")
 * @ORM\Entity(repositoryClass="App\Repository\ExtraServiceRepository")
 */
#[ApiResource]
class ExtraService
{
    /**
     * Voucher stauts
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @ORM\Id
     * @ApiProperty(writable=false)
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups("admin:read")
     * @Groups({"client:new", "offer:view"})
     */
    protected $id;

    /**
     * implemented optimistic offline locking
     * @var integer
     * @ORM\Version()
     * @ORM\Column(type="integer")
     * @Groups({"admin:read", "offer:view"})
     */
    private $version = 0;

    /**
     *
     * @ORM\Column(type="string", length=255, name="name")
     * @Groups({"offer:view"})
     */
    protected $name = "";

    /**
     *
     * @ORM\Column(type="text", name="description", nullable=true)
     * @Groups({"offer:view"})
     */
    protected $description = "";

    /**
     *
     * @ORM\Column(type="integer", name="active_status")
     * @Groups({"offer:view"})
     */
    protected $activeStatus = self::STATUS_ACTIVE;

    /**
     *
     * @ORM\Column(type="integer", name="added_value")
     * @Groups({"offer:view"})
     */
    protected $addedValue = 0;

    /**
     *
     * @ORM\Column(type="string", name="currency", length=10)
     * @Groups({"offer:view"})
     */
    protected $currency = MoneyCurrency::EUR;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\OfferPointService", mappedBy="extraService")
     */
    protected $pointServices;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offer:view"})
     */
    private $icon;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserPreference", mappedBy="extraService")
     */
    private $userPreferences;

    /**
     * ExtraService constructor.
     */
    public function __construct()
    {
        $this->pointServices = new ArrayCollection();
        $this->userPreferences = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version): void
    {
        $this->version = $version;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getActiveStatus()
    {
        return $this->activeStatus;
    }

    /**
     * @param mixed $activeStatus
     */
    public function setActiveStatus($activeStatus): void
    {
        $this->activeStatus = $activeStatus;
    }

    /**
     * @return mixed
     */
    public function getAddedValue(): int
    {
        return $this->addedValue;
    }

    /**
     * @param mixed $addedValue
     */
    public function setAddedValue(int $addedValue): void
    {
        $this->addedValue = $addedValue;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return Collection|UserPreference[]
     */
    public function getUserPreferences(): Collection
    {
        return $this->userPreferences;
    }

    public function addUserPreference(UserPreference $userPreference): self
    {
        if (!$this->userPreferences->contains($userPreference)) {
            $this->userPreferences[] = $userPreference;
            $userPreference->setExtraService($this);
        }

        return $this;
    }

    public function removeUserPreference(UserPreference $userPreference): self
    {
        if ($this->userPreferences->contains($userPreference)) {
            $this->userPreferences->removeElement($userPreference);
            // set the owning side to null (unless already changed)
            if ($userPreference->getExtraService() === $this) {
                $userPreference->setExtraService(null);
            }
        }

        return $this;
    }
}
