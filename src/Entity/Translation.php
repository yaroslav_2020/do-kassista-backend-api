<?php

declare(strict_types=1);

namespace App\Entity;

use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\TranslationRepository")
 */
class Translation implements EntityInterface
{
    use TraitTimestampableCreated, TraitTimestapableUpdated;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $domain;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $locale;

    /**
     * @ORM\Column(type="string", name="source", length=255)
     */
    private $source;

    /**
     * @ORM\Column(type="text")
     */
    private $translation;

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return string|null
     */
    public function getDomain(): ?string
    {
        return $this->domain;
    }
    /**
     * @param string $domain
     *
     * @return Translation
     */
    public function setDomain(string $domain): self
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     *
     * @return Translation
     */
    public function setLocale(string $locale): self
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string $source
     *
     * @return Translation
     */
    public function setSource(string $source): self
    {
        $this->source = $source;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getTranslation(): ?string
    {
        return $this->translation;
    }

    /**
     * @param string $translation
     *
     * @return Translation
     */
    public function setTranslation(string $translation): self
    {
        $this->translation = $translation;
        return $this;
    }
}