<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use FOS\UserBundle\Model\GroupInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 */

class User extends BaseUser implements EntityInterface
{



    /**
     * Role constants
     */
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_CLIENT = 'ROLE_CLIENT';
    const ROLE_COMERCIAL = 'ROLE_COMERCIAL';
    const ROLE_INDEPENDENT_WORKER = 'ROLE_INDEPENDENT_WORKER';
    const ROLE_EMPLOYEE_WORKER = 'ROLE_EMPLOYEE_WORKER';

    const USER_ROLE_LIST = [
        self::ROLE_ADMIN,
        self::ROLE_CLIENT,
        self::ROLE_COMERCIAL,
        self::ROLE_INDEPENDENT_WORKER,
        self::ROLE_EMPLOYEE_WORKER
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"admin:user:read", "admin:view:acl","offer:view"})
     */
    protected $id;

    /**
     * @var string
     * @orm\Column(type="string", length=255, name="name")
     * @Groups({"admin:user:read", "admin:view:acl","offer:view","user:offers","user:update"})
     */
    protected $name;

    /**
     * @var string
     * @Groups({"admin:user:read", "admin:view:acl"})
     */
    protected $username;

    /**
     * @var string
     * @Groups({"admin:user:read", "admin:view:acl"})
     */
    protected $usernameCanonical;

    /**
     * @var string
     * @Groups({"admin:user:read", "admin:view:acl","offer:view","user:offers"})
     */
    protected $email;

    /**
     * @var string
     * @Groups({"admin:user:read", "admin:view:acl"})
     */
    protected $emailCanonical;

    /**
     * @var bool
     * @Groups({"admin:user:read", "admin:view:acl"})
     */
    protected $enabled;

    /**
     * The salt to use for hashing.
     *
     * @var string
     */
    protected $salt;

    /**
     * Encrypted password. Must be persisted.
     *
     * @var string
     */
    protected $password;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     */
    protected $plainPassword;

    /**
     * @var \DateTime|null
     */
    protected $lastLogin;

    /**
     * Random string sent to the user email address in order to verify it.
     *
     * @var string|null
     */
    protected $confirmationToken;

    /**
     * @var \DateTime|null
     */
    protected $passwordRequestedAt;

    /**
     * @var GroupInterface[]|Collection
     * @Groups({"admin:user:read", "admin:view:acl"})
     */
    protected $groups;

    /**
     * @var array
     * @Groups({"admin:user:read", "admin:view:acl","user:update"})
     */
    protected $roles;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="App\Entity\UserAclAction", mappedBy="user",cascade={"remove","persist"})
     * @Groups({"admin:view:acl", "admin:user:read"})
     */

    protected $userAclActions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserSchedule", mappedBy="user", orphanRemoval=true,cascade={"remove","persist"})
     * @Groups({"admin:view:acl", "admin:user:read","user_schedule"})
     */
    private $userSchedules;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer", mappedBy="comercial",cascade={"remove","persist"})
     */
    private $offerComercial;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer", mappedBy="assignedWorker",cascade={"remove","persist"})
     */
    private $assignedWorkerOffer;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Offer", mappedBy="client",cascade={"remove","persist"})
     * @Groups({"user:offers"})
     */
    private $offers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Voucher", mappedBy="user",cascade={"remove","persist"})
     * @Groups({"admin:user:read","admin:view:acl"})
     */
    private $vouchers;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $userHash;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\EmailConfirmation", mappedBy="user")
     */
    private $emailConfirmations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Address", mappedBy="user",cascade={"remove","persist"})
     * @Groups({"admin:user:read","admin:view:acl"})
     */
    private $addresses;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Company", mappedBy="user",cascade={"remove","persist"})
     * @Groups({"admin:user:read", "admin:view:acl","offer:view"})
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"admin:user:read", "admin:view:acl","offer:view","user:offers","user:update"})
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"admin:user:read", "admin:view:acl","offer:view","user:offers","user:update"})
     */
    private $bankName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"admin:user:read", "admin:view:acl","offer:view","user:offers","user:update"})
     */
    private $bankAccount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $initialCardHolderName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customerStripeId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $activePaymentMethod;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserPreference", mappedBy="user", orphanRemoval=true, cascade={"persist"})
     * @Groups({"user_preference:update", "user_preference:read"})
     */
    private $userPreferences;

    /**
     * User constructor.
     */
    public function __construct()
    {

        parent::__construct();
        $this->userAclActions = new ArrayCollection();
        $this->userSchedules = new ArrayCollection();
        $this->offerComercial = new ArrayCollection();
        $this->assignedWorkerOffer = new ArrayCollection();
        $this->offers = new ArrayCollection();
        $this->vouchers = new ArrayCollection();
        $this->emailConfirmations = new ArrayCollection();
        $this->addresses = new ArrayCollection();
        $this->company = new ArrayCollection();
        $this->userPreferences = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUserAclActions(): Collection
    {
        return $this->userAclActions;
    }

    /**
     * @param mixed $userAclActions
     */
    public function setUserAclActions(Collection $userAclActions): void
    {
        $this->userAclActions = $userAclActions;
    }

    /**
     * @param string $actionId
     * @return bool
     */
    public function hasAccessToAction(string $actionId)
    {
        $userAclActions = $this->getUserAclActions();
        /** @var UserAclAction $action */
        foreach ($userAclActions as $action) {
            if ($action->getAclAction()->getActionId() === $actionId) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return Collection|UserSchedule[]
     */
    public function getUserSchedules(): Collection
    {
        return $this->userSchedules;
    }

    public function addUserSchedule(UserSchedule $userSchedule): self
    {
        if (!$this->userSchedules->contains($userSchedule)) {
            $this->userSchedules[] = $userSchedule;
            $userSchedule->setUser($this);
        }

        return $this;
    }

    public function removeUserSchedule(UserSchedule $userSchedule): self
    {
        if ($this->userSchedules->contains($userSchedule)) {
            $this->userSchedules->removeElement($userSchedule);
            // set the owning side to null (unless already changed)
            if ($userSchedule->getUser() === $this) {
                $userSchedule->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOfferComercial(): Collection
    {
        return $this->offerComercial;
    }

    public function addOfferComercial(Offer $offerComercial): self
    {
        if (!$this->offerComercial->contains($offerComercial)) {
            $this->offerComercial[] = $offerComercial;
            $offerComercial->setComercial($this);
        }

        return $this;
    }

    public function removeOfferComercial(Offer $offerComercial): self
    {
        if ($this->offerComercial->contains($offerComercial)) {
            $this->offerComercial->removeElement($offerComercial);
            // set the owning side to null (unless already changed)
            if ($offerComercial->getComercial() === $this) {
                $offerComercial->setComercial(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getAssignedWorkerOffer(): Collection
    {
        return $this->assignedWorkerOffer;
    }

    public function addAssignedWorkerOffer(?Offer $assignedWorkerOffer): self
    {
        if (!$this->assignedWorkerOffer->contains($assignedWorkerOffer)) {
            $this->assignedWorkerOffer[] = $assignedWorkerOffer;
            $assignedWorkerOffer->setAssignedWorker($this);
        }

        return $this;
    }

    public function removeAssignedWorkerOffer(?Offer $assignedWorkerOffer): self
    {
        if ($this->assignedWorkerOffer->contains($assignedWorkerOffer)) {
            $this->assignedWorkerOffer->removeElement($assignedWorkerOffer);
            // set the owning side to null (unless already changed)
            if ( null !== $assignedWorkerOffer->getAssignedWorker() && $assignedWorkerOffer->getAssignedWorker() === $this) {
                $assignedWorkerOffer->setAssignedWorker(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
            $offer->addClient($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): self
    {
        if ($this->offers->contains($offer)) {
            $this->offers->removeElement($offer);
            $offer->removeClient($this);
        }

        return $this;
    }

    /**
     * @return Collection|Voucher[]
     */
    public function getVouchers(): Collection
    {
        return $this->vouchers;
    }

    public function addVoucher(Voucher $voucher): self
    {
        if (!$this->vouchers->contains($voucher)) {
            $this->vouchers[] = $voucher;
            $voucher->setUser($this);
        }

        return $this;
    }

    public function removeVoucher(Voucher $voucher): self
    {
        if ($this->vouchers->contains($voucher)) {
            $this->vouchers->removeElement($voucher);
            // set the owning side to null (unless already changed)
            if ($voucher->getUser() === $this) {
                $voucher->setUser(null);
            }
        }

        return $this;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getUserHash(): ?string
    {
        return $this->userHash;
    }

    public function setUserHash(): self
    {
        $userHash = hash('sha256', $this->email . $this->name . $this->username . rand());
        $this->userHash = $userHash;

        return $this;
    }

    /**
     * @return Collection|EmailConfirmation[]
     */
    public function getEmailConfirmations(): Collection
    {
        return $this->emailConfirmations;
    }

    public function addEmailConfirmation(EmailConfirmation $emailConfirmation): self
    {
        if (!$this->emailConfirmations->contains($emailConfirmation)) {
            $this->emailConfirmations[] = $emailConfirmation;
            $emailConfirmation->addUser($this);
        }

        return $this;
    }

    public function removeEmailConfirmation(EmailConfirmation $emailConfirmation): self
    {
        if ($this->emailConfirmations->contains($emailConfirmation)) {
            $this->emailConfirmations->removeElement($emailConfirmation);
            $emailConfirmation->removeUser($this);
        }

        return $this;
    }

    /**
     * @return Collection|Address[]
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    public function addAddress(Address $address): self
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses[] = $address;
            $address->setUser($this);
        }

        return $this;
    }

    public function removeAddress(Address $address): self
    {
        if ($this->addresses->contains($address)) {
            $this->addresses->removeElement($address);
            // set the owning side to null (unless already changed)
            if ($address->getUser() === $this) {
                $address->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Company[]
     */
    public function getCompany(): Collection
    {
        return $this->company;
    }

    public function addCompany(Company $company): self
    {
        if (!$this->company->contains($company)) {
            $this->company[] = $company;
            $company->setUser($this);
        }

        return $this;
    }

    public function removeCompany(Company $company): self
    {
        if ($this->company->contains($company)) {
            $this->company->removeElement($company);
            // set the owning side to null (unless already changed)
            if ($company->getUser() === $this) {
                $company->setUser(null);
            }
        }

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getBankName(): ?string
    {
        return $this->bankName;
    }

    public function setBankName(?string $bankName): self
    {
        $this->bankName = $bankName;

        return $this;
    }

    public function getBankAccount(): ?string
    {
        return $this->bankAccount;
    }

    public function setBankAccount(?string $bankAccount): self
    {
        $this->bankAccount = $bankAccount;

        return $this;
    }

    public function getInitialCardHolderName(): ?string
    {
        return $this->initialCardHolderName;
    }

    public function setInitialCardHolderName(?string $initialCardHolderName): self
    {
        $this->initialCardHolderName = $initialCardHolderName;

        return $this;
    }

    public function getCustomerStripeId(): ?string
    {
        return $this->customerStripeId;
    }

    public function setCustomerStripeId(?string $customerStripeId): self
    {
        $this->customerStripeId = $customerStripeId;

        return $this;
    }

    public function getActivePaymentMethod(): ?string
    {
        return $this->activePaymentMethod;
    }

    public function setActivePaymentMethod(?string $activePaymentMethod): self
    {
        $this->activePaymentMethod = $activePaymentMethod;

        return $this;
    }

    /**
     * @return Collection|UserPreference[]
     */
    public function getUserPreferences(): Collection
    {
        return $this->userPreferences;
    }

    public function addUserPreference(UserPreference $userPreference): self
    {
        if (!$this->userPreferences->contains($userPreference)) {
            $this->userPreferences[] = $userPreference;
            $userPreference->setUser($this);
        }

        return $this;
    }

    public function removeUserPreference(UserPreference $userPreference): self
    {
        if ($this->userPreferences->contains($userPreference)) {
            $this->userPreferences->removeElement($userPreference);
            // set the owning side to null (unless already changed)
            if ($userPreference->getUser() === $this) {
                $userPreference->setUser(null);
            }
        }

        return $this;
    }
}
