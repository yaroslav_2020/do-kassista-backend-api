<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Currency\MoneyCurrency;
use Symfony\Component\Serializer\Annotation\Groups;

use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * Class PostalCodeGroup
 * @package App\Entity
 * @ORM\Table(name="postal_code_group")
 * @ORM\Entity(repositoryClass="App\Repository\PostalCodeGroupRepository")
 */
#[ApiResource]
class PostalCodeGroup
{
    const PREFERRED_PRICE_NOT_SET = -1;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     *
     * @ORM\Column(type="float", length=255, name="preferred_price", options={"defaults": -1})
     */
    protected $preferredPrice = self::PREFERRED_PRICE_NOT_SET;

    /**
     *
     * @ORM\Column(type="string", name="currency", length=10)
     */
    protected $currencyPreferredPrice = MoneyCurrency::EUR;

    /**
     * implemented optimistic offline locking
     * @var integer
     * @ORM\Version()
     * @ORM\Column(type="integer")
     *
     */
    private $version = 0;

    /**
     *
     * @ORM\Column(type="string", length=255, name="title")
     */
    protected $title;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\PostalCode", mappedBy="postalCodeGroup",cascade={"remove"})
     */
    private $postalCode;

    /**
     * PostalCodeGroup constructor.
     */
    public function __construct()
    {
        $this->postalCode = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version): void
    {
        $this->version = $version;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return float
     */
    public function getPreferredPrice(): float
    {
        return $this->preferredPrice;
    }

    /**
     * @param int $preferredPrice
     */
    public function setPreferredPrice(int $preferredPrice): void
    {
        $this->preferredPrice = $preferredPrice;
    }

    /**
     * @return string
     */
    public function getCurrencyPreferredPrice(): string
    {
        return $this->currencyPreferredPrice;
    }

    /**
     * @param string $currencyPreferredPrice
     */
    public function setCurrencyPreferredPrice(string $currencyPreferredPrice): void
    {
        $this->currencyPreferredPrice = $currencyPreferredPrice;
    }

    /**
     * @return Collection|PostalCode[]
     */
    public function getPostalCode(): Collection
    {
        return $this->postalCode;
    }

    public function addPostalCode(PostalCode $postalCode): self
    {
        if (!$this->postalCode->contains($postalCode)) {
            $this->postalCode[] = $postalCode;
            $postalCode->setPostalCodeGroup($this);
        }

        return $this;
    }

    public function removePostalCode(PostalCode $postalCode): self
    {
        if ($this->postalCode->contains($postalCode)) {
            $this->postalCode->removeElement($postalCode);
            // set the owning side to null (unless already changed)
            if ($postalCode->getPostalCodeGroup() === $this) {
                $postalCode->setPostalCodeGroup(null);
            }
        }

        return $this;
    }
}
