<?php

declare(strict_types=1);

namespace App\Entity;

use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use FOS\UserBundle\Model\GroupInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * Class Settings
 * @package App\Entity
 * @ORM\Table(name="settings", uniqueConstraints={@ORM\UniqueConstraint(name="unique_key", columns={"name"})})
 * @ORM\Entity(repositoryClass="App\Repository\SettingsRepository")
 */
#[ApiResource]
class Settings
{
    use TraitTimestampableCreated, TraitTimestapableUpdated;

    /**
     * price per cleanning tools
     */
    const DEV_MODE = "dev_mode";

    /**
     * An offer standard price for an employee
     */
    const STANDARD_PRICE_HOUR = "standard_price";

    /**
     * An offer standard price for an worker
     */
    const STANDARD_PRICE_INDEPENDENT_WORKER = "standard_price_independent_worker";

    /**
     * Expiration date of offer links used to access the offer
     */
    const JOB_LINK_VALIDITY_IN_HOURS = "job_link_validity_in_hours";

    /**
     * Allows vouchers to be applied on offer, or not
     */
    const ALLOW_VOUCHER_APPLY = "allow_voucher_apply";

    /**
     * duration time  for rooms
     */
    const ROOM_CLEANING_DURATION_TIME = "room_cleaning_duration_time";
    const BATHROOM_CLEANING_DURATION_TIME = "bathroom_cleaning_duration_time";
    const KITCHEN_CLEANING_DURATION_TIME = "kitchen_cleaning_duration_time";
    const LIVING_CLEANING_DURATION_TIME = "living_cleaning_duration_time";

    /**
     * min and max hours for Services
     */
    const MAX_HOURS = "max_hours";
    const MIN_HOURS = "min_hours";

    /**
     * min and max price per hour
     */
    const MAX_PRICE_PER_HOUR = "max_price_per_hour";
    const MIN_PRICE_PER_HOUR = "min_price_per_hour";

    /**
     * price per cleanning tools
     */
    const PRICE_PER_CLEANNIG_TOOLS = "price_per_cleanning_tools";

    /**
     * labor schedule settings and hours buffer
     */
    const START_LABOR_SCHEDULE = 'start_labor_schedule';
    const END_LABOR_SCHEDULE = 'end_labor_schedule';
    const HOURS_BUFFER = 'hours_buffer';

    /**
     * constants to make payments
     */
    const time_buffer_to_block_iteration = 'time_buffer_to_block_iteration';
    const time_buffer_to_realease_blocked_payment = 'time_buffer_to_realease_blocked_payment';



    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ApiProperty(writable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"settings:update"})
     */
    private $name;

    /**
     * @ORM\Column(type="array", length=255, nullable=false)
     * @Groups({"settings:update"})
     */
    private $data = [];

    /**
     * @ORM\Column(type="text", length=255, nullable=true)
     * @Groups({"settings:update"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"settings:update"})
     */
    private $category = "general";

    /**
     * Settings constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        if (!array_key_exists("value", $data)) {
            throw new \InvalidArgumentException("Key value must be set");
        }
        $this->data = $data;
    }

    /**
     * @param $value
     */
    public function setValue($value)
    {
        $this->data['value'] = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {

        return $this->data['value'];
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category): void
    {
        $this->category = $category;
    }
}
