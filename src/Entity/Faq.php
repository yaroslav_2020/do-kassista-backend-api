<?php

declare(strict_types=1);

namespace App\Entity;

use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * Class Faq
 * @package App\Entity *
 * @ORM\Entity(repositoryClass="App\Repository\FaqRepository")
 */
class Faq
{
    use TraitTimestampableCreated, TraitTimestapableUpdated;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ApiProperty(writable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $question;

    /**
     * @ORM\Column(type="string", length=2000000)
     */
    protected $answer;

    /**
     * Faq constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param mixed $question
     */
    public function setQuestion($question): void
    {
        $this->question = $question;
    }

    /**
     * @return mixed
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param mixed $answer
     */
    public function setAnswer($answer): void
    {
        $this->answer = $answer;
    }

    /**
     * @param int $id
     * @param string $question
     * @param string $answer
     * @return Faq
     */
    public static function create(int $id, string $question, string $answer): Faq
    {
        $faq = new Faq();
        $faq->setId($id);
        $faq->setAnswer($answer);
        $faq->setQuestion($question);

        return $faq;
    }
}
