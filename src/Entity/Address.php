<?php

declare(strict_types=1);

namespace App\Entity;

use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * Class Address
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\AddressRepository")
 */
class Address
{

    /**
     * Address Type
     */
    const SERVICE_ADDRESS = 'SERVICE_ADDRESS';
    const COMPANY_ADDRESS = 'COMPANY_ADDRESS';
    const BILLING_ADDRESS = 'BILLING_ADDRESS';

    use TraitTimestampableCreated, TraitTimestapableUpdated;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ApiProperty(writable=false)
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $phoneNumber = "";

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $fullName = "";

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $street = "";

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $nr;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $customPostCode;

    /**
     * @var PostalCode
     *
     * Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="App\Entity\PostalCode", inversedBy="addressesLinked")
     * @ORM\JoinColumn(name="postal_code_id", referencedColumnName="id")
     */
    protected $postalCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $addressType = self::SERVICE_ADDRESS;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="addresses")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Country", inversedBy="addresses")
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\County", inversedBy="addresses")
     */
    private $county;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City", inversedBy="addresses")
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $floor;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OfferBilling", mappedBy="address")
     */
    private $offerBillings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer", mappedBy="serviceAddress")
     */
    private $offers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Company", mappedBy="address")
     */
    private $company;

    /**
     * @ORM\Column(type="boolean")
     */
    private $deleted;

    /**
     * Address constructor.
     */
    public function __construct()
    {
        $this->offerBillings = new ArrayCollection();
        $this->offers = new ArrayCollection();
        $this->company = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * @return mixed
     */
    public function getNr()
    {
        return $this->nr;
    }

    /**
     * @param mixed $nr
     */
    public function setNr($nr): void
    {
        $this->nr = $nr;
    }

    /**
     * @return mixed
     */
    public function getCustomPostCode()
    {
        return $this->customPostCode;
    }

    /**
     * @param mixed $customPostCode
     */
    public function setCustomPostCode($customPostCode): void
    {
        $this->customPostCode = $customPostCode;
    }

    /**
     * @return PostalCode
     */
    public function getPostalCode(): ?PostalCode
    {
        return $this->postalCode;
    }

    /**
     * @param PostalCode $postalCode
     */
    public function setPostalCode(PostalCode $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     */
    public function setFullName(string $fullName): void
    {
        $this->fullName = $fullName;
    }

    public function getAddressType(): ?string
    {
        return $this->addressType;
    }

    public function setAddressType(string $addressType): self
    {
        $this->addressType = $addressType;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCounty(): ?County
    {
        return $this->county;
    }

    public function setCounty(?County $county): self
    {
        $this->county = $county;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getFloor(): ?string
    {
        return $this->floor;
    }

    public function setFloor(string $floor): self
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * @return Collection|OfferBilling[]
     */
    public function getOfferBillings(): Collection
    {
        return $this->offerBillings;
    }

    public function addOfferBilling(OfferBilling $offerBilling): self
    {
        if (!$this->offerBillings->contains($offerBilling)) {
            $this->offerBillings[] = $offerBilling;
            $offerBilling->setAddress($this);
        }

        return $this;
    }

    public function removeOfferBilling(OfferBilling $offerBilling): self
    {
        if ($this->offerBillings->contains($offerBilling)) {
            $this->offerBillings->removeElement($offerBilling);
            // set the owning side to null (unless already changed)
            if ($offerBilling->getAddress() === $this) {
                $offerBilling->setAddress(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
            $offer->setServiceAddress($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): self
    {
        if ($this->offers->contains($offer)) {
            $this->offers->removeElement($offer);
            // set the owning side to null (unless already changed)
            if ($offer->getServiceAddress() === $this) {
                $offer->setServiceAddress(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Company[]
     */
    public function getCompany(): Collection
    {
        return $this->company;
    }

    public function addCompany(Company $company): self
    {
        if (!$this->company->contains($company)) {
            $this->company[] = $company;
            $company->setAddress($this);
        }

        return $this;
    }

    public function removeCompany(Company $company): self
    {
        if ($this->company->contains($company)) {
            $this->company->removeElement($company);
            // set the owning side to null (unless already changed)
            if ($company->getAddress() === $this) {
                $company->setAddress(null);
            }
        }

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }
}
