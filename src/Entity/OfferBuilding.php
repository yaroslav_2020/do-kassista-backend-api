<?php

declare(strict_types=1);

namespace App\Entity;

use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class OfferBuilding
 * @package App\Entity
 * @ORM\Table(name="offer_building")
 * @ORM\Entity()
 */
class OfferBuilding
{
    use TraitTimestampableCreated, TraitTimestapableUpdated;

    /**
     * Defines constant for access types
     */
    const ACCESS_TYPE_I_AM_AT_HOME = "i_am_at_home";
    const ACCESS_TYPE_SPECS_IN_DESCRIPTION = "specs_in_description";

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"offer:view","user:offers"})
     */
    protected $id;

    /**
     * implemented optimistic offline locking
     * @var integer
     * @ORM\Version()
     * @ORM\Column(type="integer")
     * @Groups({"client:new", "offer:view","user:offers"})
     */
    private $version = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", name="room_number", options={"defaults": 0})
     * @Groups({"client:new", "offer:view","client:create","user:offers"})
     */
    private $roomNumber = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", name="bath_room_number", options={"defaults": 0})
     * @Groups({"client:new", "offer:view","client:create","user:offers"})
     */
    private $bathRoomNumber = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", name="kitchen_number", options={"defaults": 0})
     * @Groups({"client:new", "offer:view","client:create","user:offers"})
     */
    private $kitchenNumber = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", name="living_number", options={"defaults": 0})
     * @Groups({"client:new", "offer:view","client:create","user:offers"})
     */
    private $livingNumber = 0;

    /**
     * @var string
     * @ORM\Column(type="string", length=20, name="access_type", nullable=false)
     * @Groups({"client:new", "offer:view","client:create","user:offers"})
     */
    private $accessType = self::ACCESS_TYPE_I_AM_AT_HOME;

    /**
     * @var string
     * @ORM\Column(type="text", name="access_description", nullable=true)
     * @Groups({"client:new", "offer:view","client:create","user:offers"})
     */
    private $accessDescription;

    /**
     * OfferBuilding constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version): void
    {
        $this->version = $version;
    }

    /**
     * @return int
     */
    public function getRoomNumber(): int
    {
        return $this->roomNumber;
    }

    /**
     * @param int $roomNumber
     */
    public function setRoomNumber(int $roomNumber): void
    {
        $this->roomNumber = $roomNumber;
    }

    /**
     * @return int
     */
    public function getBathRoomNumber(): int
    {
        return $this->bathRoomNumber;
    }

    /**
     * @param int $bathRoomNumber
     */
    public function setBathRoomNumber(int $bathRoomNumber): void
    {
        $this->bathRoomNumber = $bathRoomNumber;
    }

    /**
     * @return int
     */
    public function getKitchenNumber(): int
    {
        return $this->kitchenNumber;
    }

    /**
     * @param int $kitchenNumber
     */
    public function setKitchenNumber(int $kitchenNumber): void
    {
        $this->kitchenNumber = $kitchenNumber;
    }

    /**
     * @return int
     */
    public function getLivingNumber(): int
    {
        return $this->livingNumber;
    }

    /**
     * @param int $livingNumber
     */
    public function setLivingNumber(int $livingNumber): void
    {
        $this->livingNumber = $livingNumber;
    }

    /**
     * @return string
     */
    public function getAccessType(): string
    {
        return $this->accessType;
    }

    /**
     * @param string $accessType
     */
    public function setAccessType(string $accessType): void
    {
        if (!in_array($accessType, [self::ACCESS_TYPE_I_AM_AT_HOME, self::ACCESS_TYPE_SPECS_IN_DESCRIPTION], true)) {
            throw new \RuntimeException("Access type $accessType is invalid");
        }
        $this->accessType = $accessType;
    }

    /**
     * @return string
     */
    public function getAccessDescription(): ?string
    {
        return $this->accessDescription;
    }

    /**
     * @param string $accessDescription
     */
    public function setAccessDescription(string $accessDescription): void
    {
        $this->accessDescription = $accessDescription;
    }
}