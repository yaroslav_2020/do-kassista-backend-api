<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OfferBillingRepository")
 */
class OfferBilling
{
    const PERSONAL_BILLING = "PERSONAL_BILLING";
    const COMPANY_BILLING = "COMPANY_BILLING";

    const ARRAY_BILLING_TYPE = [
        self::PERSONAL_BILLING,
        self::COMPANY_BILLING,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"show:offer:billing","offer:view"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"show:offer:billing","offer:view","client:create"})
     */
    private $billingType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Address", inversedBy="offerBillings")
     * @Groups({"show:offer:billing","offer:view","client:create"})
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="offerBillings")
     * @Groups({"show:offer:billing","offer:view","client:create"})
     */
    private $company;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBillingType(): ?string
    {
        return $this->billingType;
    }

    public function setBillingType(string $billingType): self
    {
        if (!in_array($billingType, self::ARRAY_BILLING_TYPE, true)) {
            throw new \RuntimeException("Invalid pet type set $billingType");
        }

        $this->billingType = $billingType;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }
    
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }
}
