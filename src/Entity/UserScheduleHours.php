<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserScheduleHoursRepository")
 */
class UserScheduleHours
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"user_schedule_hours"})
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\UserSchedule", inversedBy="userScheduleHours")
     */
    private $schedule;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"admin:view:acl", "user_schedule_hours", "user_schedule", "user_schedule:write", "user_schedule:update"})
     * @Assert\NotBlank()
     */
    private $startTime;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"admin:view:acl", "user_schedule_hours", "user_schedule", "user_schedule:write", "user_schedule:update"})
     * @Assert\NotBlank()
     */
    private $endTime;

    public function __construct()
    {
        $this->schedule = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|UserSchedule[]
     */
    public function getSchedule(): Collection
    {
        return $this->schedule;
    }

    public function addSchedule(UserSchedule $schedule): self
    {
        if (!$this->schedule->contains($schedule)) {
            $this->schedule[] = $schedule;
        }

        return $this;
    }

    public function removeSchedule(UserSchedule $schedule): self
    {
        if ($this->schedule->contains($schedule)) {
            $this->schedule->removeElement($schedule);
        }

        return $this;
    }

    public function getStartTime(): ?string
    {
        return $this->startTime;
    }

    public function setStartTime(string $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?string
    {
        return $this->endTime;
    }

    public function setEndTime(string $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }
}
