<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Validator\NotOverlapHours;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserScheduleRepository")
 * @ApiFilter(SearchFilter::class, properties={
 *     "user": "exact"
 * })
 * @UniqueEntity(
 *      fields={"user","day"},
 *      errorPath= "dayAlreadySelected",
 *      message= ""
 * )
 */
class UserSchedule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"admin:view:acl", "user_schedule"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userSchedules")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",nullable=false)
     * @Groups({"user_schedule:write"})
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"admin:view:acl","user_schedule", "user_schedule:write"})
     * @Assert\NotBlank()
     */
    private $day;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\UserScheduleHours", mappedBy="schedule", cascade={"persist"}, orphanRemoval=true)
     * @Groups({"admin:view:acl","user_schedule", "user_schedule:write", "user_schedule:update"})
     * @Assert\NotBlank()
     * @Assert\Valid()
     * @NotOverlapHours()
     */
    private $userScheduleHours;

    public function __construct()
    {
        $this->userScheduleHours = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDay(): ?string
    {
        return $this->day;
    }

    public function setDay(string $day): self
    {
        $this->day = $day;

        return $this;
    }

    /**
     * @return Collection|UserScheduleHours[]
     */
    public function getUserScheduleHours(): Collection
    {
        return $this->userScheduleHours;
    }

    public function addUserScheduleHour(UserScheduleHours $userScheduleHour): self
    {
        if (!$this->userScheduleHours->contains($userScheduleHour)) {
            $this->userScheduleHours[] = $userScheduleHour;
            $userScheduleHour->addSchedule($this);
        }

        return $this;
    }

    public function removeUserScheduleHour(UserScheduleHours $userScheduleHour): self
    {
        if ($this->userScheduleHours->contains($userScheduleHour)) {
            $this->userScheduleHours->removeElement($userScheduleHour);
            $userScheduleHour->removeSchedule($this);
        }

        return $this;
    }
}
