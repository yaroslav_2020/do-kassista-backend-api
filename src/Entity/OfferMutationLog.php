<?php

declare(strict_types=1);

namespace App\Entity;

use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\ORM\Mapping as ORM;

/**
 * Holds log list for all mutations done to an offer in order to reproduce all the offer steps
 *
 * Class OfferMutationLog
 *
 * @package App\Entity
 * @ORM\Table(name="offer_mutation_log")
 * @ORM\Entity()
 */
class OfferMutationLog
{
    use TraitTimestampableCreated, TraitTimestapableUpdated;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     * @var ?User
     *
     * Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(type="text", name="offer_mutation")
     */
    private $offerMutation;

    /**
     * OfferMutationLog constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getOfferMutation(): string
    {
        return $this->offerMutation;
    }

    /**
     * @param string $offerMutation
     */
    public function setOfferMutation(string $offerMutation): void
    {
        $this->offerMutation = $offerMutation;
    }
}