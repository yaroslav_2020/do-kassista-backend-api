<?php


declare(strict_types=1);

namespace App\Entity;

use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * Class Company
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @ORM\Id
     * @ApiProperty(writable=false)
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"admin:view:acl","show:offer:billing","offer:view","show:companies","user:offers"})
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"admin:view:acl","show:offer:billing","offer:view","show:companies","user:offers"})
     */
    protected $name = "";

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"admin:view:acl","show:offer:billing","offer:view","show:companies","user:offers"})
     */
    private $bankAccount;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"admin:view:acl","show:offer:billing","offer:view","show:companies","user:offers"})
     */
    private $bankName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"admin:view:acl","show:offer:billing","offer:view","show:companies","user:offers"})
     */
    private $swift;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"admin:view:acl","show:offer:billing","offer:view","show:companies","user:offers"})
     */
    private $cif;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"admin:view:acl","show:offer:billing","offer:view","show:companies","user:offers"})
     */
    private $regNumber;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="company")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer", mappedBy="company")
     */
    private $offers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OfferBilling", mappedBy="company")
     */
    private $offerBillings;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Address", inversedBy="company")
     * @Groups({"admin:view:acl","show:offer:billing","offer:view","show:companies","user:offers"})
     */
    private $address;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"admin:view:acl","show:offer:billing","offer:view","show:companies","user:offers"})
     */
    private $deleted = false;

    /**
     * Company constructor.
     */
    public function __construct()
    {
        $this->offers = new ArrayCollection();
        $this->offerBillings = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getBankAccount(): ?string
    {
        return $this->bankAccount;
    }

    public function setBankAccount(string $bankAccount): self
    {
        $this->bankAccount = $bankAccount;

        return $this;
    }

    public function getBankName(): ?string
    {
        return $this->bankName;
    }

    public function setBankName(string $bankName): self
    {
        $this->bankName = $bankName;

        return $this;
    }

    public function getSwift(): ?string
    {
        return $this->swift;
    }

    public function setSwift(string $swift): self
    {
        $this->swift = $swift;

        return $this;
    }

    public function getCif(): ?string
    {
        return $this->cif;
    }

    public function setCif(string $cif): self
    {
        $this->cif = $cif;

        return $this;
    }

    public function getRegNumber(): ?string
    {
        return $this->regNumber;
    }

    public function setRegNumber(string $regNumber): self
    {
        $this->regNumber = $regNumber;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
            $offer->setCompany($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): self
    {
        if ($this->offers->contains($offer)) {
            $this->offers->removeElement($offer);
            // set the owning side to null (unless already changed)
            if ($offer->getCompany() === $this) {
                $offer->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OfferBilling[]
     */
    public function getOfferBillings(): Collection
    {
        return $this->offerBillings;
    }

    public function addOfferBilling(OfferBilling $offerBilling): self
    {
        if (!$this->offerBillings->contains($offerBilling)) {
            $this->offerBillings[] = $offerBilling;
            $offerBilling->setCompany($this);
        }

        return $this;
    }

    public function removeOfferBilling(OfferBilling $offerBilling): self
    {
        if ($this->offerBillings->contains($offerBilling)) {
            $this->offerBillings->removeElement($offerBilling);
            // set the owning side to null (unless already changed)
            if ($offerBilling->getCompany() === $this) {
                $offerBilling->setCompany(null);
            }
        }

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }
}
