<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OfferFullScheduleRepository")
 */
class OfferFullSchedule
{

    /**
     * Status constants
     * pending - in progress - completed - canceled - with issues
     */
    const STATUS_PENDING = 'STATUS_PENDING';
    const STATUS_AMOUNT_BLOCKED = "STATUS_AMOUNT_BLOCKED";
    const STATUS_IN_PROGRESS = 'STATUS_IN_PROGRESS';
    const STATUS_COMPLETED = 'STATUS_COMPLETED';
    const STATUS_CANCELED = 'STATUS_CANCELED';
    const STATUS_WITH_ISSUES = 'STATUS_WITH_ISSUES';

    const ARRAY_STATUS = [
        self::STATUS_PENDING,
        self::STATUS_IN_PROGRESS,
        self::STATUS_COMPLETED,
        self::STATUS_CANCELED,
        self::STATUS_WITH_ISSUES,
        self::STATUS_AMOUNT_BLOCKED
    ];

    /**
     * Payment Status constants
     * pending - blokced - completed - amount block unsuccess - completed  unsuccess
     */
    const PAYMENT_STATUS_PENDING = 'PAYMENT_STATUS_PENDING';
    const PAYMENT_STATUS_AMOUNT_BLOCKED = "PAYMENT_STATUS_AMOUNT_BLOCKED";
    const PAYMENT_STATUS_COMPLETED = 'PAYMENT_STATUS_COMPLETED';
    const PAYMENT_STATUS_AMOUNT_BLOCK_UNSUCESS = 'PAYMENT_STATUS_AMOUNT_BLOCK_UNSUCESS';
    const PAYMENT_STATUS_COMPLETED_UNSUCESS = 'PAYMENT_STATUS_AMOUNT_COMPLETED_UNSUCESS';

    const ARRAY_PAYMENT_STATUS = [
        self::PAYMENT_STATUS_PENDING,
        self::PAYMENT_STATUS_AMOUNT_BLOCKED,
        self::PAYMENT_STATUS_COMPLETED,
        self::PAYMENT_STATUS_AMOUNT_BLOCK_UNSUCESS,
        self::PAYMENT_STATUS_COMPLETED_UNSUCESS,
    ];

    //create two more properties for dates: payment-blocked / payment-completed

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer", inversedBy="offerFullSchedules")
     */
    private $offer;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $workerOfferStatus; //initial is pending

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $clientOfferStatus; //initial is pending

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateTimeStart;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateTimeEnd;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Expression(
     *     "value.format('d/m/Y') == this.getDateTimeStart().format('d/m/Y')",
     *     message="The dates are not equal {{ value }}"
     * )
     */
    private $dateTimeUpdated;

    /**
     * @ORM\Column(type="float")
     */
    private $pricePerDay;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $paymentStatus;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $chargeId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateBlockedPayment;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datePaymentRelease;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    public function setOffer(?Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    public function getWorkerOfferStatus(): ?string
    {
        return $this->workerOfferStatus;
    }

    public function setWorkerOfferStatus(string $workerOfferStatus): self
    {
        if (!in_array($workerOfferStatus, self::ARRAY_STATUS, true)) {
            throw new \RuntimeException("Invalid status set $workerOfferStatus");
        }

        $this->workerOfferStatus = $workerOfferStatus;

        return $this;
    }

    public function getClientOfferStatus(): ?string
    {
        return $this->clientOfferStatus;
    }

    public function setClientOfferStatus(string $clientOfferStatus): self
    {
        if (!in_array($clientOfferStatus, self::ARRAY_STATUS, true)) {
            throw new \RuntimeException("Invalid status set $clientOfferStatus");
        }

        $this->clientOfferStatus = $clientOfferStatus;

        return $this;
    }

    public function getDateTimeStart(): ?\DateTimeInterface
    {
        return $this->dateTimeStart;
    }

    public function setDateTimeStart(\DateTimeInterface $dateTimeStart): self
    {
        $this->dateTimeStart = $dateTimeStart;

        return $this;
    }

    public function getDateTimeEnd(): ?\DateTimeInterface
    {
        return $this->dateTimeEnd;
    }

    public function setDateTimeEnd(\DateTimeInterface $dateTimeEnd): self
    {
        $this->dateTimeEnd = $dateTimeEnd;

        return $this;
    }

    public function getDateTimeUpdated(): ?\DateTimeInterface
    {
        return $this->dateTimeUpdated;
    }

    public function setDateTimeUpdated(\DateTimeInterface $dateTimeUpdated): self
    {
        $this->dateTimeUpdated = $dateTimeUpdated;

        return $this;
    }

    public function getPricePerDay(): ?float
    {
        return $this->pricePerDay;
    }

    public function setPricePerDay(float $pricePerDay): self
    {
        $this->pricePerDay = $pricePerDay;

        return $this;
    }

    public function getPaymentStatus(): ?string
    {
        return $this->paymentStatus;
    }

    public function setPaymentStatus(string $paymentStatus): self
    {

        if (!in_array($paymentStatus, self::ARRAY_PAYMENT_STATUS, true)) {
            throw new \RuntimeException("Invalid payment status type set $clientOfferStatus");
        }

        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    public function getChargeId(): ?string
    {
        return $this->chargeId;
    }

    public function setChargeId(?string $chargeId): self
    {
        $this->chargeId = $chargeId;

        return $this;
    }

    public function getDateBlockedPayment(): ?\DateTimeInterface
    {
        return $this->dateBlockedPayment;
    }

    public function setDateBlockedPayment(?\DateTimeInterface $dateBlockedPayment): self
    {
        $this->dateBlockedPayment = $dateBlockedPayment;

        return $this;
    }

    public function getDatePaymentRelease(): ?\DateTimeInterface
    {
        return $this->datePaymentRelease;
    }

    public function setDatePaymentRelease(\DateTimeInterface $datePaymentRelease): self
    {
        $this->datePaymentRelease = $datePaymentRelease;

        return $this;
    }
}
