<?php

namespace App\Entity;

use Symfony\Component\Serializer\Annotation\Groups;
use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * Class AclAction
 * @package App\Entity
 * @ORM\Table(name="acl_action", uniqueConstraints={@UniqueConstraint(name="action_id", columns={"action_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\AclActionRepository")
 */
#[ApiResource]
class AclAction
{
    use TraitTimestampableCreated, TraitTimestapableUpdated;

    /**
     * @ORM\Id
     * @ApiProperty(writable=false)
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"admin:view:acl", "admin:user:read"})
     */
    protected $id;

    /**
     * implemented optimistic offline locking
     * @var integer
     * @ORM\Version()
     * @ORM\Column(type="integer")
     * @Groups({"admin:view:acl", "admin:user:read"})
     */
    private $version = 0;

    /**
     * @var string
     * @orm\Column(type="string", length=255, name="category")
     * @Groups({"admin:view:acl", "admin:user:read"})
     */
    protected $category;

    /**
     * @var string
     * @orm\Column(type="string", length=255, name="action_id")
     * @Groups({"admin:view:acl", "admin:user:read"})
     */
    protected $actionId;

    /**
     * @var string
     * @orm\Column(type="string", length=255, name="action_pretty_name")
     * @Groups({"admin:view:acl", "admin:user:read"})
     */
    protected $actionPrettyName;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="App\Entity\UserAclAction", mappedBy="aclAction")
     */
    protected $userAclActions;

    /**
     * AclAction constructor.
     */
    public function __construct()
    {
        $this->userAclActions = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version): void
    {
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getActionId(): string
    {
        return $this->actionId;
    }

    /**
     * @param string $actionId
     */
    public function setActionId(string $actionId): void
    {
        $this->actionId = $actionId;
    }

    /**
     * @return string
     */
    public function getActionPrettyName(): string
    {
        return $this->actionPrettyName;
    }

    /**
     * @param string $actionPrettyName
     */
    public function setActionPrettyName(string $actionPrettyName): void
    {
        $this->actionPrettyName = $actionPrettyName;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category): void
    {
        $this->category = $category;
    }
}
