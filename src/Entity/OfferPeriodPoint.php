<?php

declare(strict_types=1);

namespace App\Entity;

use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class OfferPeriodPoint
 * @package App\Entity
 * @ORM\Table(name="offer_period_point")
 * @ORM\Entity()
 */
class OfferPeriodPoint
{
    use TraitTimestampableCreated, TraitTimestapableUpdated;

    /**
     * Defines constant used to know if the system should automatically assign a value for this point or already has
     * an user provided value which is immutable
     */
    const TYPE_VALUE_ANY = "value_any";

    /**
     * Defines a constant used to know the system should NOT specify any value, it has one, user provided
     */
    const TYPE_VALUE_USER_PROVIDED = "value_user_provided";

    /**
     * Defines a constant for each day of the week
     */
    const MONDAY = "MONDAY";
    const TUESDAY = "TUESDAY";
    const WEDNESDAY = "WEDNESDAY";
    const THURSDAY = "THURSDAY";
    const FRIDAY = "FRIDAY";
    const SATURDAY = "SATURDAY";
    const SUNDAY = "SUNDAY";

    const DAYS = [
        self::MONDAY,
        self::TUESDAY,
        self::WEDNESDAY,
        self::THURSDAY,
        self::FRIDAY,
        self::SATURDAY,
        self::SUNDAY
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"offer:view","user:offers"})
     */
    protected $id;

    /**
     * implemented optimistic offline locking
     * @var integer
     * @ORM\Version()
     * @ORM\Column(type="integer")
     * @Groups({"offer:view","user:offers"})
     */
    private $version = 0;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, name="value_type")
     * @Groups({"client:new", "offer:view","client:create","user:offers"})
     */
    private $valueType = self::TYPE_VALUE_ANY;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, name="value", nullable=true)
     * @Groups({"client:new", "offer:view","client:create","user:offers"})
     */
    private $value;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, name="start_hour", nullable=true)
     * @Groups({"client:new", "offer:view","client:create","user:offers"})
     */
    private $startHour;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client:create","offer:view","user:offers"})
     */
    private $day;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer", inversedBy="periodPoints",cascade={"persist"})
     * @Groups({"client:create"})
     */
    private $offer;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OfferPointService", mappedBy="offerPeriodPoint",cascade={"persist"})
     * @Groups({"offer:view","client:create","user:offers"})
     */
    private $pointServices;

    /**
     * @var float
     * @ORM\Column(type="float", name="duration_in_minutes", options={"default": 0})
     * @Groups({"offer:view","client:create","client:new","user:offers"})
     */
    private $durationInMinutes;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client:new", "offer:view","client:create","user:offers"})
     */
    private $endHour;

    /**
     * OfferPeriodPoint constructor.
     */
    public function __construct()
    {
        $this->pointServices = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version): void
    {
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getValueType(): ?string
    {
        return $this->valueType;
    }

    /**
     * @param string $valueType
     */
    public function setValueType(string $valueType): void
    {
        if (!in_array($valueType, [self::TYPE_VALUE_ANY, self::TYPE_VALUE_USER_PROVIDED], true)) {
            throw new \RuntimeException("Invalid value type set $valueType");
        }
        $this->valueType = $valueType;
    }

    /**
     * @return string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getStartHour(): ?string
    {
        return $this->startHour;
    }

    /**
     * @param string $startHour
     */
    public function setStartHour(string $startHour): void
    {
        $this->startHour = $startHour;
    }

    public function getDay(): ?string
    {
        return $this->day;
    }

    public function setDay(string $day): self
    {
        if (!in_array($day, self::DAYS, true)) {
            throw new \RuntimeException("Invalid value day set $day");
        }
        $this->day = $day;

        return $this;
    }

    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    public function setOffer(?Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * @return Collection|OfferPointService[]
     */
    public function getPointServices(): Collection
    {
        return $this->pointServices;
    }

    public function addPointService(OfferPointService $pointService): self
    {
        if (!$this->pointServices->contains($pointService)) {
            $this->pointServices[] = $pointService;
            $pointService->setOfferPeriodPoint($this);
        }

        return $this;
    }

    public function removePointService(OfferPointService $pointService): self
    {
        if ($this->pointServices->contains($pointService)) {
            $this->pointServices->removeElement($pointService);
            // set the owning side to null (unless already changed)
            if ($pointService->getOfferPeriodPoint() === $this) {
                $pointService->setOfferPeriodPoint(null);
            }
        }

        return $this;
    }

    public function getDurationInMinutes(): ?float
    {
        return $this->durationInMinutes;
    }

    public function setDurationInMinutes(float $durationInMinutes): self
    {
        if (!is_float($durationInMinutes) || $durationInMinutes <= 0) {
            throw new \RuntimeException("Invalid duration: $durationInMinutes");
        }
        $this->durationInMinutes = $durationInMinutes;        

        return $this;
    }

    public function getEndHour(): ?string
    {
        return $this->endHour;
    }

    public function setEndHour(string $endHour): self
    {
        $this->endHour = $endHour;

        return $this;
    }
}