<?php

declare(strict_types=1);

namespace App\Entity;

use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class Voucher
 * @package App\Entity
 * @ORM\Table(name="voucher", uniqueConstraints={@UniqueConstraint(name="unique_code", columns={"code"})})
 * @ORM\Entity(repositoryClass="App\Repository\VoucherRepository")
 * @Assert\Expression(
 *     "(this.getCanBeUsedOnlyOneTime() && (this.getUsageCount() > 0)) ? false : true ",
 *     message="This voucher is already spent"
 * )
 */
#[ApiResource]
class Voucher
{
    use TraitTimestampableCreated, TraitTimestapableUpdated;

    /**
     * Voucher types
     */
    const TYPE_FIXED = "fixed";
    const TYPE_PERCENT = "percent";

    /**
     * Voucher stauts
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * Voucher usages types
     */
    const USAGE_TYPE_BY_ANY_CLIENT_ANY_TIME = "USAGE_TYPE_BY_ANY_CLIENT_ANY_TIME";
    const USAGE_ONE_TIME_BY_FIRST_CLIENT = "USAGE_ONE_TIME_BY_FIRST_CLIENT";
    const USAGE_ONE_TIME_BY_SPECIFIC_CLIENT = "USAGE_ONE_TIME_BY_SPECIFIC_CLIENT";

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"admin:read", "offer:view","user:offers"})
     */
    protected $id;

    /**
     * implemented optimistic offline locking
     * @var integer
     * @ORM\Version()
     * @ORM\Column(type="integer")
     * @Groups({"admin:read", "offer:view","user:offers"})
     */
    private $version = 0;

    /**
     *
     * @orm\Column(type="string", length=255, name="title")
     * @Groups({"admin:create", "admin:read", "admin:update", "offer:view","user:offers"})
     */
    protected $title;

    /**
     *
     * @orm\Column(type="string", length=255, name="code")
     * @Groups({"admin:read", "offer:view","user:offers", "admin:update"})
     */
    protected $code;

    /**
     *
     * @orm\Column(type="string", length=20, name="type")
     * @Groups({"admin:create","admin:read", "admin:update", "offer:view","user:offers"})
     */
    protected $type = self::TYPE_FIXED;

    /**
     *
     * @orm\Column(type="integer", name="active_status")
     * @Groups({"admin:create", "admin:read", "admin:update", "offer:view","user:offers"})
     */
    protected $activeStatus = self::STATUS_ACTIVE;

    /**
     * @var string
     * @orm\Column(type="string", length=50, name="usage_type")
     * @Groups({"admin:create", "admin:read", "admin:update", "offer:view","user:offers"})
     */
    protected $usageType = self::USAGE_TYPE_BY_ANY_CLIENT_ANY_TIME;

    /**
     * @var int
     * @orm\Column(type="integer", name="usage_count")
     * @Groups({"admin:read", "admin:update", "offer:view","user:offers"})
     */
    protected $usageCount = 0;

    /**
     * @var \DateTime
     * @orm\Column(type="datetime", name="start_date", nullable=true)
     * @Groups({"admin:create", "admin:read", "admin:update", "offer:view","user:offers"})
     */
    protected $startDate;

    /**
     * @var \DateTime
     * @orm\Column(type="datetime", name="end_date", nullable=true)
     * @Groups({"admin:create", "admin:read", "admin:update", "offer:view","user:offers"})
     */
    protected $endDate;

    /**
     * @var
     * @ORM\ManyToMany(targetEntity="App\Entity\Offer", mappedBy="vouchers", cascade={"persist"})
     * @Groups({"admin:read","admin:update"})
     */
    protected $offers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="vouchers")
     * @Groups({"admin:create","admin:read","admin:update"})
     */
    private $user;

    /**
     * @ORM\Column(type="float")
     * @Groups({"admin:create", "admin:read", "admin:update", "offer:view","user:offers"})
     */
    private $discount;

    /**
     * Voucher constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->offers = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        if (!in_array($type, [self::TYPE_PERCENT, self::TYPE_FIXED])) {
            throw new \InvalidArgumentException("Type is not valid");
        }
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getActiveStatus()
    {
        return $this->activeStatus;
    }

    /**
     * @param mixed $activeStatus
     */
    public function setActiveStatus($activeStatus): void
    {
        if (!in_array($activeStatus, [self::STATUS_INACTIVE, self::STATUS_ACTIVE])) {
            throw new \InvalidArgumentException("Active status is not valid");
        }
        $this->activeStatus = $activeStatus;
    }

    /**
     * @return string
     */
    public function getUsageType(): string
    {
        return $this->usageType;
    }

    /**
     * @param string $usageType
     */
    public function setUsageType(string $usageType): void
    {
        if (!in_array($usageType, [self::USAGE_TYPE_BY_ANY_CLIENT_ANY_TIME, self::USAGE_ONE_TIME_BY_FIRST_CLIENT, self::USAGE_ONE_TIME_BY_SPECIFIC_CLIENT])) {
            throw new \InvalidArgumentException("Usage type is not valid");
        }
        $this->usageType = $usageType;
    }

    /**
     * @return int
     */
    public function getUsageCount(): int
    {
        return $this->usageCount;
    }

    /**
     * @param int $usageCount
     */
    public function setUsageCount(int $usageCount): void
    {
        $this->usageCount = $usageCount;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): ?\DateTime
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     */
    public function setStartDate(\DateTime $startDate): void
    {
        $this->startDate = clone $startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     */
    public function setEndDate(\DateTime $endDate): void
    {
        $this->endDate = clone $endDate;
    }

    /**
     * @return bool
     * @throws \Exception
     * @Groups("admin:read")
     */
    public function isReadyToBeUsed(): bool
    {
        //validate active by dates
        if (!($this->getStartDate() instanceof \DateTime)) {
            return false;
        }

        if (!($this->getEndDate() instanceof \DateTime)) {
            return false;
        }

        $nowDate = new \DateTime('now', new \DateTimeZone('UTC'));
        if (!($nowDate >= $this->getStartDate() && $nowDate <= $this->getEndDate()) && !$this->getCanBeUsedByClientAnyTime()) {
            return false;
        }

        //validate active by type and usages
        if ($this->getCanBeUsedOnlyOneTime() && $this->isUsedAtLeastOneTime()) {
            return false;
        }

        //validate active by active status
        if ($this->isNotActive()) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     * @Groups("admin:read")
     */
    public function getCanBeUsedByClientAnyTime(): bool
    {
        return $this->getUsageType() === self::USAGE_TYPE_BY_ANY_CLIENT_ANY_TIME;
    }

    /**
     * @return bool
     * @Groups("admin:read")
     */
    public function getCanBeUsedOnlyOneTime(): bool
    {
        return ($this->getUsageType() === self::USAGE_ONE_TIME_BY_FIRST_CLIENT ||
            $this->getUsageType() === self::USAGE_ONE_TIME_BY_SPECIFIC_CLIENT
        );
    }

    /**
     * @return bool
     * @Groups("admin:read")
     */
    public function isUsedAtLeastOneTime(): bool
    {
        return $this->usageCount > 0;
    }

    /**
     * @return bool
     * @Groups("admin:read")
     */
    public function isActive(): bool
    {
        return $this->getActiveStatus() === self::STATUS_ACTIVE;
    }

    /**
     * @return bool
     * @Groups("admin:read")
     */
    public function isNotActive(): bool
    {
        return $this->getActiveStatus() === self::STATUS_INACTIVE;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version): void
    {
        $this->version = $version;
    }

    /**
     * @return mixed
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @param mixed $offers
     */
    public function setOffers($offers): void
    {
        $this->offers = $offers;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(float $discount): self
    {
        $this->discount = $discount;

        return $this;
    }
}
