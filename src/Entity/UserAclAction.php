<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class UserAclAction
 * @package App\Entity
 * @ORM\Table(name="user_acl_action")
 * @ORM\Entity(repositoryClass="App\Repository\UserAclActionRepository")
 */
class UserAclAction
{
    use TraitTimestampableCreated, TraitTimestapableUpdated;

    /**
     * @ORM\Id
     * @ApiProperty(writable=false)
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"admin:user:read"})
     */
    protected $id;

    /**
     * implemented optimistic offline locking
     * @var integer
     * @ORM\Version()
     * @ORM\Column(type="integer")
     * @Groups({"admin:user:read"})
     */
    private $version = 0;

    /**
     * @var User
     *
     * Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userAclActions")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var AclAction
     *
     * Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="App\Entity\AclAction", inversedBy="userAclActions")
     * @ORM\JoinColumn(name="acl_action_id", referencedColumnName="id")
     * @Groups({"admin:view:acl", "admin:user:read"})
     */
    private $aclAction;

    /**
     * UserAclAction constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version): void
    {
        $this->version = $version;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return AclAction
     */
    public function getAclAction(): AclAction
    {
        return $this->aclAction;
    }

    /**
     * @param AclAction $aclAction
     */
    public function setAclAction(AclAction $aclAction): void
    {
        $this->aclAction = $aclAction;
    }
}
