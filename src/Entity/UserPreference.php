<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\UserPreferenceRepository")
 */
class UserPreference
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userPreferences")
     * @Groups({"user_preference:write"})
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"user_preference:write", "user_preference:update"})
     */
    private $petPresence = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ExtraService", inversedBy="userPreferences", cascade={"persist"})
     * @Groups({"user_preference:write", "user_preference:update"})
     */
    private $extraService;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPetPresence(): ?bool
    {
        return $this->petPresence;
    }

    public function setPetPresence(bool $petPresence): self
    {
        $this->petPresence = $petPresence;

        return $this;
    }

    public function getExtraService(): ?ExtraService
    {
        return $this->extraService;
    }

    public function setExtraService(?ExtraService $extraService): self
    {
        $this->extraService = $extraService;

        return $this;
    }

}
