<?php

declare(strict_types=1);

namespace App\Entity;


use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * Class KnowledgeBase
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\KnowledgeBaseRepository")
 */
class KnowledgeBase implements EntityInterface
{
    use TraitTimestampableCreated, TraitTimestapableUpdated;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $content;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $category;

    /**
     * Faq constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @param int $id
     * @param string $question
     * @param string $answer
     * @param string $category
     * @return KnowledgeBase
     */
    public static function create(int $id, string $question, string $answer, string $category): KnowledgeBase
    {
        $knowledgeBase = new KnowledgeBase();
        $knowledgeBase->setId($id);
        $knowledgeBase->setContent($answer);
        $knowledgeBase->setTitle($question);
        $knowledgeBase->setCategory($category);

        return $knowledgeBase;
    }
}
