<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaymentRepository")
 */
class Payment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client:create"})
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client:create"})
     */
    private $status;



    /**
     * @ORM\Column(type="float")
     * @Groups({"client:create"})
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer", inversedBy="payments")
     * @Groups({"client:create"})
     */
    private $offer;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"client:create"})
     */
    private $lastTransaction;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client:create"})
     */
    private $paymentUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"client:create"})
     */
    private $errorMessage;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client:create"})
     */
    private $currency;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client:create"})
     */
    private $cardToken;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client:create"})
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }



    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    public function setOffer(?Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    public function getLastTransaction(): ?bool
    {
        return $this->lastTransaction;
    }

    public function setLastTransaction(bool $lastTransaction): self
    {
        $this->lastTransaction = $lastTransaction;

        return $this;
    }

    public function getPaymentUrl(): ?string
    {
        return $this->paymentUrl;
    }

    public function setPaymentUrl(string $paymentUrl): self
    {
        $this->paymentUrl = $paymentUrl;

        return $this;
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    public function setErrorMessage(?string $errorMessage): self
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCardToken(): ?string
    {
        return $this->cardToken;
    }

    public function setCardToken(string $cardToken): self
    {
        $this->cardToken = $cardToken;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
