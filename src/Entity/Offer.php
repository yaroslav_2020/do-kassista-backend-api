<?php

declare(strict_types=1);

namespace App\Entity;


use App\EntityTrait\TraitTimestampableCreated;
use App\EntityTrait\TraitTimestapableUpdated;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Annotation\ApiSubresource;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * Class Offer
 * @package App\Entity
 * @ORM\Table(name="offer")
 * 
 * @ORM\Entity(repositoryClass="App\Repository\OfferRepository")
 */
// #[ApiFilter(SearchFilter::class, properties: ['assignedWorker.id' => 'exact'])]
class Offer
{
    use TraitTimestampableCreated, TraitTimestapableUpdated;

    /**
     * Define period types
     */
    const PERIOD_SERVERAL_TIMES = "several_times_a_week";
    const PERIOD_TYPE_WEEKLY = "each_1_week";
    const PERIOD_TYPE_TWO_WEEKS = "each_2_weeks";
    const PERIOD_TYPE_THREE_WEEKS = "each_3_weeks";
    const PERIOD_TYPE_FOUR_WEEKS = "each_4_weeks";
    const PERIOD_TYPE_MONTHLY = "each_1_month";
    const PERIOD_ONLY_ONCE = "only_once";

    const ARRAY_PERIOD = [
        self::PERIOD_SERVERAL_TIMES,
        self::PERIOD_TYPE_WEEKLY,
        self::PERIOD_TYPE_TWO_WEEKS,
        self::PERIOD_TYPE_THREE_WEEKS,
        self::PERIOD_TYPE_FOUR_WEEKS,
        self::PERIOD_TYPE_MONTHLY,
        self::PERIOD_ONLY_ONCE,
    ];

    /**
     * Defines offer statuses
     */
    const STATUS_ASSIGNED_TO_INDEPENDENT = "assigned_to_independent";
    const STATUS_LOST = "lost";
    const STATUS_IN_NEGOTIATION = "in_negotiation";
    const STATUS_CANCELED = "canceled";
    const STATUS_NO_INTEREST = "no_interest";
    const STATUS_PENDING = 'pending';
    const STATUS_COMPLETED = 'completed';
    

    //pending - asigned - in_negotiation

    const ARRAY_STATUS = [
        self::STATUS_ASSIGNED_TO_INDEPENDENT,
        self::STATUS_LOST,
        self::STATUS_IN_NEGOTIATION,
        self::STATUS_CANCELED,
        self::STATUS_NO_INTEREST,
        self::STATUS_PENDING,
        self::STATUS_COMPLETED,
    ];

    /**
     * Define pet Constants
     */
    const PET_NOT = "PET_NOT";
    const PET_CAT = "PET_CAT";
    const PET_DOG = "PET_DOG";
    const PET_OTHER = "PET_OTHER";

    const ARRAY_PET = [
        self::PET_NOT,
        self::PET_CAT,
        self::PET_DOG,
        self::PET_OTHER,
    ];


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"offer:view","user:offers"})
     *
     */
    protected $id;

    /**
     * implemented optimistic offline locking
     * @var integer
     * @ORM\Version()
     * @ORM\Column(type="integer")
     * @Groups({"offer:view","user:offers"})
     *
     */
    private $version = 0;

    /**
     *
     * @ORM\Column(type="string", length=50, name="status", nullable=false)
     * @Groups({"offer:view","user:offers"})
     */
    private $status = self::STATUS_PENDING;

    /**
     *
     * @ORM\Column(type="string", length=255, name="created_from_ip")
     * @Groups({"offer:view","user:offers"})
     */
    private $createdFromIp = "127.0.0.1-default";

    /**
     *
     * @ORM\Column(type="string", length=255, name="last_changed_ip")
     */
    private $lastChangedIp = "127.0.0.1-default";

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="start_from_date", options={"defaults": "CURRENT_TIMESTAMP"})
     * @Groups({"client:create", "offer:view","user:offers"})
     */
    private $startFromDate;

    /**
     * Hold the period used, how often the cycle of services is repeated. For example, if all services are repeated
     * one per week, it means every week is the period, so it weekly. It does not matter how many days in that week,
     * it just matters how long is the cycle
     *
     * @var string
     * @ORM\Column(type="string", length=50, name="period", nullable=false)
     * @Groups({"client:create", "offer:view","user:offers"})
     */
    private $period = self::PERIOD_TYPE_WEEKLY;

    /**
     *
     * @ORM\Column(type="text", name="other_details", nullable=true)
     * @Groups({"client:create", "offer:view","user:offers"})
     */
    private $otherDetails;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\Voucher", inversedBy="offers", cascade={"persist"})
     * @ORM\JoinTable(name="offer_vouchers")
     * @Groups({"client:create", "offer:view", "user:offers"})
     */
    private $vouchers;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="offerComercial", cascade={"persist"})
     * @ORM\JoinColumn(name="user_as_assigned_commercial_id", referencedColumnName="id", nullable=true)
     * @Groups({"offer:view"})
     */
    private $comercial;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="assignedWorkerOffer", cascade={"persist"})
     * @ORM\JoinColumn(name="user_as_assigned_worker_id", referencedColumnName="id", nullable=true)
     * @Groups({"offer:view","user:offers"})
     */
    private $assignedWorker;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OfferBuilding", cascade={"persist", "remove"})
     * @Groups({"client:create", "offer:view","user:offers"})
     */
    private $offerBuilding;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\OfferPeriodPoint", mappedBy="offer", cascade={"persist"})
     * @Groups({"client:create", "offer:view","user:offers"})
     */
    private $periodPoints;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="offers")
     * @Groups({"client:create", "offer:view"})
     */
    private $client;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client:create", "offer:view","user:offers"})
     */
    private $pet;

    /**
     * @ORM\Column(type="boolean",options={"default": false})
     * @Groups({"offer:view","user:offers"})
     */
    private $deleted = false;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $offerHash;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\EmailConfirmation", mappedBy="offer")
     */
    private $emailConfirmations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="offers",cascade={"persist", "remove"})
     * @Groups({"client:create","offer:view","user:offers"})
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Address", inversedBy="offers",cascade={"persist", "remove"})
     * @Groups({"offer:view", "client:create","user:offers"})
     */
    private $serviceAddress;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Payment", mappedBy="offer",cascade={"persist"})
     * @Groups({"offer:view", "client:create","user:offers"})
     */
    private $payments;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"client:create", "offer:view","user:offers"})
     */
    private $otherPet;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"client:create", "offer:view","user:offers"})
     */
    private $cleanningTools;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OfferBilling", cascade={"persist", "remove"})
     * @Groups({"client:create", "offer:view","user:offers"})
     */
    private $offerBilling;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"client:create", "offer:view","user:offers"})
     */
    private $endDate;

    /**
     * @ORM\Column(type="float")
     * @Groups({"client:create", "offer:view","user:offers"})
     */
    private $pricePerService;

    /**
     * @ORM\Column(type="float")
     * @Groups({"client:create", "offer:view","user:offers"})
     */
    private $pricePerDay;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"client:create", "offer:view","user:offers"})
     */
    private $cardHolderName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OfferFullSchedule", mappedBy="offer")
     */
    private $offerFullSchedules;

    /**
     * Offer constructor.
     */
    public function __construct()
    {
        $this->vouchers = new ArrayCollection();
        $this->startFromDate = new \DateTime('now', new \DateTimeZone("UTC"));
        $this->periodPoints = new ArrayCollection();
        $this->client = new ArrayCollection();
        $this->emailConfirmations = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->offerFullSchedules = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version): void
    {
        $this->version = $version;
    }

    /**
     * @return mixed
     */
    public function getCreatedFromIp()
    {
        return $this->createdFromIp;
    }

    /**
     * @param mixed $createdFromIp
     */
    public function setCreatedFromIp($createdFromIp): void
    {
        $this->createdFromIp = $createdFromIp;
    }

    /**
     * @return mixed
     */
    public function getLastChangedIp()
    {
        return $this->lastChangedIp;
    }

    /**
     * @param mixed $lastChangedIp
     */
    public function setLastChangedIp($lastChangedIp): void
    {
        $this->lastChangedIp = $lastChangedIp;
    }

    /**
     * @return ArrayCollection
     */
    public function getVouchers()
    {
        return $this->vouchers;
    }

    /**
     * @param Array $vouchers
     */
    public function setVouchers(?Array $vouchers): void
    {
        $this->vouchers = $vouchers;
    }

    /**
     * @return \DateTime
     */
    public function getStartFromDate(): \DateTime
    {
        return $this->startFromDate;
    }

    /**
     * @param \DateTime $startFromDate
     */
    public function setStartFromDate(\DateTime $startFromDate): void
    {
        $this->startFromDate = $startFromDate;
    }

    /**
     * @return string
     */
    public function getPeriod(): string
    {
        return $this->period;
    }

    /**
     * @param string $period
     */
    public function setPeriod(string $period): void
    {
        if (!in_array($period, self::ARRAY_PERIOD, true)) {
            throw new \RuntimeException("Invalid period type set $period");
        }
        $this->period = $period;
    }

    /**
     * @return mixed
     */
    public function getOtherDetails()
    {
        return $this->otherDetails;
    }

    /**
     * @param mixed $otherDetails
     */
    public function setOtherDetails($otherDetails): void
    {
        $this->otherDetails = $otherDetails;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        if (!in_array($status, self::ARRAY_STATUS, true)) {
            throw new \RuntimeException("Invalid status type set $status");
        }
        $this->status = $status;
    }

    public function getComercial(): ?User
    {
        return $this->comercial;
    }

    public function setComercial(?User $comercial): self
    {
        $this->comercial = $comercial;

        return $this;
    }

    public function getAssignedWorker(): ?User
    {
        return $this->assignedWorker;
    }

    public function setAssignedWorker(?User $assignedWorker): self
    {
        $this->assignedWorker = $assignedWorker;

        return $this;
    }

    public function getOfferBuilding(): ?OfferBuilding
    {
        return $this->offerBuilding;
    }

    public function setOfferBuilding(?OfferBuilding $offerBuilding): self
    {
        $this->offerBuilding = $offerBuilding;

        return $this;
    }

    /**
     * @return Collection|OfferPeriodPoint[]
     */
    public function getPeriodPoints(): ?Collection
    {
        return $this->periodPoints;
    }

    public function addPeriodPoint(OfferPeriodPoint $periodPoint): self
    {
        if (!$this->periodPoints->contains($periodPoint)) {
            $this->periodPoints[] = $periodPoint;
            $periodPoint->setOffer($this);
        }

        return $this;
    }

    public function removePeriodPoint(OfferPeriodPoint $periodPoint): self
    {
        if ($this->periodPoints->contains($periodPoint)) {
            $this->periodPoints->removeElement($periodPoint);
            // set the owning side to null (unless already changed)
            if ($periodPoint->getOffer() === $this) {
                $periodPoint->setOffer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getClient(): Collection
    {
        return $this->client;
    }

    public function addClient(User $client): self
    {
        if (!$this->client->contains($client)) {
            $this->client[] = $client;
        }

        return $this;
    }

    public function removeClient(User $client): self
    {
        if ($this->client->contains($client)) {
            $this->client->removeElement($client);
        }

        return $this;
    }

    public function getPet(): ?string
    {
        return $this->pet;
    }

    public function setPet(string $pet): self
    {

        if (!in_array($pet, self::ARRAY_PET, true)) {
            throw new \RuntimeException("Invalid pet type set $pet");
        }
        $this->pet = $pet;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getOfferHash(): ?string
    {
        return $this->offerHash;
    }

    public function setOfferHash(): self
    {

        $offerHash = hash('sha256',$this->client[0]->getEmail() . $this->startFromDate->format('Y-m-d H:i:s') . $this->status . rand());
        $this->offerHash = $offerHash;

        return $this;
    }

    /**
     * @return Collection|EmailConfirmation[]
     */
    public function getEmailConfirmations(): Collection
    {
        return $this->emailConfirmations;
    }

    public function addEmailConfirmation(EmailConfirmation $emailConfirmation): self
    {
        if (!$this->emailConfirmations->contains($emailConfirmation)) {
            $this->emailConfirmations[] = $emailConfirmation;
            $emailConfirmation->addOffer($this);
        }

        return $this;
    }

    public function removeEmailConfirmation(EmailConfirmation $emailConfirmation): self
    {
        if ($this->emailConfirmations->contains($emailConfirmation)) {
            $this->emailConfirmations->removeElement($emailConfirmation);
            $emailConfirmation->removeOffer($this);
        }

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getServiceAddress(): ?Address
    {
        return $this->serviceAddress;
    }

    public function setServiceAddress(?Address $serviceAddress): self
    {
        $this->serviceAddress = $serviceAddress;

        return $this;
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setOffer($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->contains($payment)) {
            $this->payments->removeElement($payment);
            // set the owning side to null (unless already changed)
            if ($payment->getOffer() === $this) {
                $payment->setOffer(NULL);
            }
        }

        return $this;
    }

    public function getOtherPet(): ?string
    {
        return $this->otherPet;
    }

    public function setOtherPet(?string $otherPet): self
    {
        $this->otherPet = $otherPet;

        return $this;
    }

    public function getCleanningTools(): ?bool
    {
        return $this->cleanningTools;
    }

    public function setCleanningTools(bool $cleanningTools): self
    {
        $this->cleanningTools = $cleanningTools;

        return $this;
    }

    public function getOfferBilling(): ?OfferBilling
    {
        return $this->offerBilling;
    }

    public function setOfferBilling(?OfferBilling $offerBilling): self
    {
        $this->offerBilling = $offerBilling;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getPricePerService(): ?float
    {
        return $this->pricePerService;
    }

    public function setPricePerService(float $pricePerService): self
    {
        $this->pricePerService = $pricePerService;

        return $this;
    }

    public function getPricePerDay(): ?float
    {
        return $this->pricePerDay;
    }

    public function setPricePerDay(float $pricePerDay): self
    {
        $this->pricePerDay = $pricePerDay;

        return $this;
    }

    public function getCardHolderName(): ?string
    {
        return $this->cardHolderName;
    }

    public function setCardHolderName(string $cardHolderName): self
    {
        $this->cardHolderName = $cardHolderName;

        return $this;
    }

    /**
     * @return Collection|OfferFullSchedule[]
     */
    public function getOfferFullSchedules(): Collection
    {
        return $this->offerFullSchedules;
    }

    public function addOfferFullSchedule(OfferFullSchedule $offerFullSchedule): self
    {
        if (!$this->offerFullSchedules->contains($offerFullSchedule)) {
            $this->offerFullSchedules[] = $offerFullSchedule;
            $offerFullSchedule->setOffer($this);
        }

        return $this;
    }

    public function removeOfferFullSchedule(OfferFullSchedule $offerFullSchedule): self
    {
        if ($this->offerFullSchedules->contains($offerFullSchedule)) {
            $this->offerFullSchedules->removeElement($offerFullSchedule);
            // set the owning side to null (unless already changed)
            if ($offerFullSchedule->getOffer() === $this) {
                $offerFullSchedule->setOffer(null);
            }
        }

        return $this;
    }
}