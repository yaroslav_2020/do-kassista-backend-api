<?php

declare(strict_types=1);

namespace App\Exception\AddressTypeAssert;

/**
 * Class AddressTypeAssertException
 * @package App\Exception\AddressTypeAssert
 */
class AddressTypeAssertException extends \Exception
{

}