<?php

declare(strict_types=1);

namespace App\Exception\ArrayAssert;

/**
 * Class ArrayAssertExecption
 * @package App\Exception\ArrayAssert
 */
class ArrayAssertException extends \Exception
{

}