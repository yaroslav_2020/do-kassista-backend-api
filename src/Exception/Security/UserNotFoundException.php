<?php

declare(strict_types=1);

namespace App\Exception\Security;

/**
 * Class UserNotFoundException
 * @package App\Exception\Security
 */
class UserNotFoundException extends \Exception
{

}