<?php

declare(strict_types=1);

namespace App\Exception\Security;

/**
 * Class ExistingUserException
 * @package App\Exception\Register
 */
class ExistingUserException extends \Exception
{

}