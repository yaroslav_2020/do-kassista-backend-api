<?php

declare(strict_types=1);

namespace App\Exception\Security;

/**
 * Class PasswordNotMatchedException
 * @package App\Exception\Security
 */
class PasswordNotMatchedException extends \Exception
{

}