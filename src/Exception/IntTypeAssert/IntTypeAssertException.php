<?php

declare(strict_types=1);

namespace App\Exception\IntTypeAssert;

/**
 * Class IntTypeAssertException
 * @package App\Exception\IntTypeAssert
 */
class IntTypeAssertException extends \Exception
{

}