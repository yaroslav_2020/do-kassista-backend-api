<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


class NotOverlapHoursValidator extends ConstraintValidator
{

    private function isOverlap($intervals) {


    }

    public function validate($value, Constraint $constraint)
    {

        /* @var $constraint \App\Validator\NotOverlapHours */


        if (null === $value || '' === $value) {
            return;
        }

        // TODO: implement the validation here
        
        $intervals = $value->toArray();
        $utc = new \DateTimeZone('UTC');

        if (count($intervals) == 1) {
            // throw new \Exception($value->getValues()[0]->getStartTime());


            $startInterval = new \DateTime($value->getValues()[0]->getStartTime(),$utc);
            $endInterval = new \DateTime($value->getValues()[0]->getEndTime(),$utc);            
            
            if($endInterval < $startInterval)
                throw new \Exception('Range is negative.');

            return;
        }



        for ($i=0; $i < (count($intervals) - 1); $i++) { 

            $currentStartInterval = new \DateTime($value->getValues()[$i]->getStartTime(),$utc);
            $currentEndInterval = new \DateTime($value->getValues()[$i]->getEndTime(),$utc);

            if($currentEndInterval < $currentStartInterval)
                throw new \Exception('Range is negative.');
        
            
            for ($j=($i+1); $j < count($intervals); $j++) { 
                # code...
                $nextStartInterval = new \DateTime($value->getValues()[$j]->getStartTime(),$utc);
                $nextEndInterval = new \DateTime($value->getValues()[$j]->getEndTime(),$utc);
                if($nextEndInterval < $nextStartInterval)
                    throw new \Exception('Range is negative.');

                if (($currentEndInterval <= $nextStartInterval) || ($nextEndInterval <= $currentStartInterval)) {
                    # code...
                } else {
                    $this->context->buildViolation($constraint->message)
                        ->setParameter('{{ value }}', (
                            "(".$currentStartInterval->format('H:i')." - ".$currentEndInterval->format('H:i').") (".
                            $nextStartInterval->format('H:i')." - ".$nextEndInterval->format('H:i')."): "
                        ))
                        ->addViolation();
                    }
                    
                }
        }


        // $currentStartInterval = new \DateTime($intervals[$i]->getStartTime(),$utc);
        // $currentEndInterval = new \DateTime($intervals[$i]->getEndTime(),$utc);

        // if($currentEndInterval < $currentStartInterval)
        //     throw new \Exception('Range is negative.');
    
        
        // for ($j=($i+1); $j < count($intervals); $j++) { 
        //     # code...
        //     $nextStartInterval = new \DateTime($intervals[$j]->getStartTime(),$utc);
        //     $nextEndInterval = new \DateTime($intervals[$j]->getEndTime(),$utc);
        //     if($nextEndInterval < $nextStartInterval)
        //         throw new \Exception('Range is negative.');

        //     if (($currentEndInterval <= $nextStartInterval) || ($nextEndInterval <= $currentStartInterval)) {
        //         # code...
        //     } else {
        //         $this->context->buildViolation($constraint->message)
        //             ->setParameter('{{ value }}', (
        //                 $currentStartInterval->format('H:i')."-".$currentEndInterval->format('H:i')." with ".
        //                 $nextStartInterval->format('H:i')."-".$nextEndInterval->format('H:i')
        //             ))
        //             ->addViolation();
        //     }

        // }
    }
}
