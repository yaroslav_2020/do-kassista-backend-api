<?php


namespace App\Repository;


use App\Entity\Offer;
use App\Entity\User;
use App\Entity\PostalCodeGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class OfferRepository
 * @package App\Repository
 */
class OfferRepository extends ServiceEntityRepository
{
    /** EntityManager $manager */
    private $manager;

    /**
     * UsersRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        
        parent::__construct($registry, Offer::class);

        $this->manager = $registry->getEntityManager();
    }

    public function update(Offer $offer){
        $this->manager->persist($offer);
        $this->manager->flush();
    }

    public function getOffersByStatus(String $status)
    {
        return $this->manager
                    ->createQuery(
                        'SELECT o 
                        FROM App\Entity\Offer o 
                        WHERE o.status=:status'
                    )
                    ->setParameter('status',$status)
                    ->getResult();
    }

    public function updateStatusCompleted(Offer $offer)
    {
        $this->manager
                    ->createQuery(
                        'UPDATE App\Entity\Offer o 
                        SET o.status=:status 
                        WHERE o.id=:offer_id'
                    )
                    ->setParameter('status', Offer::STATUS_COMPLETED)
                    ->setParameter('offer_id',$offer->getId())
                    ->getResult();
    }
}