<?php

declare(strict_types=1);

namespace App\Repository;


use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
//use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

use FOS\UserBundle\Model\UserInterface;

use Symfony\Bridge\Doctrine\RegistryInterface;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;



/**
 * Class UsersRepository
 * @package App\Repository
 */
class UsersRepository extends ServiceEntityRepository
{
    /** EntityManager $manager */
    private $manager;

    /** UserPasswordEncoderInterface $encoder */
    private $encoder;

    /**
     * UsersRepository constructor.
     * @param RegistryInterface $registry
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(RegistryInterface $registry, UserPasswordEncoderInterface $encoder)
    {

        parent::__construct($registry, User::class);


        $this->manager = $registry->getEntityManager();
        $this->encoder = $encoder;
    }

    /**
     * Create a new user
     * @param User $user
     * @return User
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(User $user)
    {

        $this->manager->persist($user);
        $this->manager->flush();

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function findUserByConfirmationToken($token)
    {
        return $this->findOneBy(['confirmationToken' => $token]);
    }

    /**
     * @param string $email
     * @return object|null
     */
    public function findUserByEmail(string $email)
    {
        return $this->findOneBy(['email' => $email]);
    }

    public function findAll()
    {
        $query = $this->manager->createQuery("
            SELECT 
            user.email
            FROM 
            App\Entity\User user
            WHERE 
            user.roles like '%ROLE_EMPLOYEE_WORKER%'
            or user.roles like '%ROLE_INDEPENDENT_WORKER%'
        ");
        return $query->getResult();
    }

    public function findWorkers()
    {
        $query = $this->manager->createQuery("
            SELECT 
            user
            FROM 
            App\Entity\User user
            WHERE 
            user.roles like '%ROLE_EMPLOYEE_WORKER%'
            or user.roles like '%ROLE_INDEPENDENT_WORKER%'
        ");
        return $query->getResult();
    }

    public function updateCustomerId($user_id, $customer_id)
    {
    }
}
