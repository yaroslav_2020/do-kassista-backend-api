<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\PostalCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class PostalCodeRepository
 * @package App\Repository
 */
class PostalCodeRepository extends ServiceEntityRepository
{
    /** EntityManager $manager */
    private $manager;

    /**
     * UsersRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PostalCode::class);

        $this->manager = $registry->getEntityManager();
    }


    /**
     * Create a new postalCode
     * @param PostalCode $postalCode
     * @return 
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(PostalCode $postalCode){
        $this->manager->persist($postalCode);
        $this->manager->flush();

        return $postalCode; 
    }


    /**
     * Remove a postalCode
     * @param PostalCode $postalCode
     * @return 
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(PostalCode $postalCode){


        $this->manager->remove($postalCode);
        $this->manager->flush();

        return $postalCode; 
    }

    /**
     * @param string $code
     * @return object|null
     */
    public function findUsingCode(string $code): ?PostalCode
    {
        return $this->findOneBy([
            'code' => $code
        ]);
    }

    /**
     * @param int $id
     * @return object|null
     */
    public function findUsingId(int $id): ?PostalCode
    {
        return $this->findOneBy([
            'id' => $id
        ]);
    }
    
    /**
     * @param int $id
     * @return array[]|null
     */    
    public function findAllUsingId(int $id){
        return $this->manager->createQuery(
            'SELECT p FROM App\Entity\PostalCode p WHERE p.id=:id'
        )
        ->setParameter('id',$id)
        ->getResult();

        // var_dump($var['title']);
        // die();

    }


    
}