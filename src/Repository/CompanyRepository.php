<?php


declare(strict_types=1);

namespace App\Repository;

use App\Entity\Company;
use App\Entity\ExtraService;
use App\Entity\Voucher;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class CompanyRepository
 * @package App\Repository
 */
class CompanyRepository extends ServiceEntityRepository
{
    /** EntityManager $manager */
    private $manager;

    /**
     * UsersRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Company::class);

        $this->manager = $registry->getEntityManager();
    }
}