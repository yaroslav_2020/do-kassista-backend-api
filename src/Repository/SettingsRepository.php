<?php


declare(strict_types=1);

namespace App\Repository;

use App\Entity\Settings;
use App\Entity\User;
use App\Entity\Voucher;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class SettingsRepository
 * @package App\Repository
 */
class SettingsRepository extends ServiceEntityRepository
{
    /** EntityManager $manager */
    private $manager;

    /**
     * UsersRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Settings::class);

        $this->manager = $registry->getManager();
    }

    /**
     * @param string $key
     * @return object|null
     */
    public function getByKey(string $key): ?Settings
    {

        return $this->findOneBy(['key' => $key]);
    }

    public function getSettingsForOffferFullSchedule()
    {

        return $this->manager
            ->createQuery(
                'SELECT s 
                        FROM App\Entity\Settings s 
                        WHERE s.name=:startLabor or s.name=:endLabor or s.name=:hourBuffer'
            )
            ->setParameter('startLabor', Settings::START_LABOR_SCHEDULE)
            ->setParameter('endLabor', Settings::END_LABOR_SCHEDULE)
            ->setParameter('hourBuffer', Settings::HOURS_BUFFER)
            ->getResult();
    }

    public function getSetting($setting)
    {

        // return $this->manager
        //     ->createQuery(
        //         'SELECT s 
        //                  FROM App\Entity\Settings s 
        //                  WHERE s.name=:setting'
        //     )
        //     ->setParameter('setting', $setting)
        //     ->getOneOrNullResult();

        $entityManager = $this->manager;

        $query = $entityManager
            ->createQuery(
                'SELECT s 
                         FROM App\Entity\Settings s 
                         WHERE s.name=:setting'
            )
            ->setParameter('setting', $setting);
        return $query->getOneOrNullResult();
    }
}
