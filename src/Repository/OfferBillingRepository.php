<?php

namespace App\Repository;

use App\Entity\OfferBilling;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method OfferBilling|null find($id, $lockMode = null, $lockVersion = null)
 * @method OfferBilling|null findOneBy(array $criteria, array $orderBy = null)
 * @method OfferBilling[]    findAll()
 * @method OfferBilling[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferBillingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OfferBilling::class);
    }

    // /**
    //  * @return OfferBilling[] Returns an array of OfferBilling objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OfferBilling
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
