<?php

namespace App\Repository;

use App\Entity\EndDate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method EndDate|null find($id, $lockMode = null, $lockVersion = null)
 * @method EndDate|null findOneBy(array $criteria, array $orderBy = null)
 * @method EndDate[]    findAll()
 * @method EndDate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EndDateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EndDate::class);
    }

    // /**
    //  * @return EndDate[] Returns an array of EndDate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EndDate
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
