<?php

namespace App\Repository;

use App\Entity\Payment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Serializer\SerializerInterface;
use App\Entity\Offer;

/**
 * @method Payment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Payment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Payment[]    findAll()
 * @method Payment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentRepository extends ServiceEntityRepository
{
    private $manager;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Payment::class);

        $this->manager = $registry->getEntityManager();
    }

    public function savePaymentDetails($offer, $data)
    {
        $payment = new Payment();
        $payment->setOffer($offer);
        $payment->setStatus($data["status"]);
        $payment->setDate($data["date"]);
        $payment->setCode($data["code"]);
        $payment->setAmount($data["amount"]);
        $payment->setPaymentUrl($data["payment_url"]);
        $payment->setCurrency($data["currency"]);
        $payment->setDescription($data["description"]);
        $payment->setLastTransaction($data["last_transaction"]);
        $payment->setCardToken($data["card_token"]);
        $this->manager->persist($payment);
        $this->manager->flush();
    }

    public function setDownLastTransactionField($offer)
    {
    }

    public function findTheFirstPayment($data)
    {
        try {
            return $this->manager->createQuery('
                                SELECT p 
                                from App\Entity\Payment p
                                where IDENTITY(p.offer) = :offer_id and p.lastTransaction = 1
                            ')
                ->setParameter('offer_id', $data['offer_id'])
                ->getOneOrNullResult();
        } catch (Exception $error) {
        }
    }

    public function savePaymentCreatingOffer($data)
    {
        try {
            if ($data['offer'] instanceof Offer) {

                $payment = $this->manager->createQuery('
                                SELECT p 
                                from App\Entity\Payment p
                                where IDENTITY(p.offer) = :offer_id and p.lastTransaction = 1
                            ')
                    ->setParameter('offer_id', $data['offer']->getId())
                    ->getOneOrNullResult();

                $payment->setCode($data['code']);
                $payment->setStatus($data['status']);
                $payment->setDate($data['date']);
                $payment->setAmount($data['amount']);
                $payment->setDescription($data['description']);
                $payment->setPaymentUrl($data['paymentUrl']);
                $payment->setErrorMessage($data['errorMessage']);

                // $this->manager->persist($payment);
                $this->manager->flush();
            }
        } catch (Exception $error) {
        }
    }

    public function savePaymentCreatingOfferError($data)
    {
        try {
            if ($data['offer'] instanceof Offer) {

                $payment = $this->manager->createQuery('
                                SELECT p 
                                from App\Entity\Payment p
                                where IDENTITY(p.offer) = :offer_id and p.lastTransaction = 1
                            ')
                    ->setParameter('offer_id', $data['offer']->getId())
                    ->getOneOrNullResult();

                $payment->setStatus($data['status']);
                $payment->setErrorMessage($data['errorMessage']);

                // $this->manager->persist($payment);
                $this->manager->flush();
            }
        } catch (Exception $error) {
        }
    }

    // /**
    //  * @return Payment[] Returns an array of Payment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Payment
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
