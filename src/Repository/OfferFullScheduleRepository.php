<?php

namespace App\Repository;

use App\Entity\OfferFullSchedule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Entity\Offer;

/**
 * @method OfferFullSchedule|null find($id, $lockMode = null, $lockVersion = null)
 * @method OfferFullSchedule|null findOneBy(array $criteria, array $orderBy = null)
 * @method OfferFullSchedule[]    findAll()
 * @method OfferFullSchedule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferFullScheduleRepository extends ServiceEntityRepository
{
    private $manager;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OfferFullSchedule::class);
        $this->manager = $registry->getEntityManager();
    }

    public function insert(OfferFullSchedule $offerFullSchedule)
    {
        $this->manager->persist($offerFullSchedule);
        $this->manager->flush();
    }

    public function getDaysToPay(Offer $offer)
    {
        return $this->manager
                    ->createQuery(
                        'SELECT s 
                         FROM App\Entity\OfferFullSchedule s 
                         WHERE IDENTITY(s.offer)=:offer AND s.paymentStatus=:payment_status AND s.workerOfferStatus=:worker_status AND s.clientOfferStatus=:client_status'
                    )
                    ->setParameter('offer',$offer->getId())
                    ->setParameter('worker_status',OfferFullSchedule::STATUS_COMPLETED)
                    ->setParameter('client_status',OfferFullSchedule::STATUS_COMPLETED)
                    ->setParameter('payment_status',OfferFullSchedule::PAYMENT_STATUS_AMOUNT_BLOCKED)
                    ->getResult();
    }

    public function getDaysToBlockPayment(Offer $offer)
    {
        return $this->manager
                    ->createQuery(
                        'SELECT s
                                 FROM App\Entity\OfferFullSchedule s
                                 JOIN s.offer o WITH o.assignedWorker IS NOT NULL WHERE s.offer=:offer AND s.paymentStatus=:payment_status AND s.chargeId IS NULL'
                    )
                    ->setParameter('offer',$offer->getId())
                    ->setParameter('payment_status',OfferFullSchedule::PAYMENT_STATUS_PENDING)
                    ->getResult();
//                    ->createQuery(
//                        'SELECT s
//                         FROM App\Entity\OfferFullSchedule s
//                         WHERE IDENTITY(s.offer)=:offer AND s.assignedWorker is not Null AND s.paymentStatus=:payment_status AND s.chargeId is Null'
//                    )

    }

    public function updateBlockedMoneyStatus(OfferFullSchedule $offerFullSchedule,$charge_id,$error = false)
    {
        $status = $error ? OfferFullSchedule::PAYMENT_STATUS_AMOUNT_BLOCK_UNSUCESS : OfferFullSchedule::PAYMENT_STATUS_AMOUNT_BLOCKED;

        $chargeId = $charge_id === null ? null : $charge_id->id;

        return $this->manager
                    ->createQuery(
                        'UPDATE App\Entity\OfferFullSchedule s
                         SET s.chargeId=:charge_id , s.paymentStatus=:payment_status, s.dateBlockedPayment=:blocked_payment 
                         WHERE s.id=:offer_full_schedule'
                    )
                    ->setParameter('charge_id',$chargeId)
                    ->setParameter('payment_status',$status)
                    ->setParameter('offer_full_schedule',$offerFullSchedule->getId())
                    ->setParameter('blocked_payment',new \DateTime('now', new \DateTimeZone("UTC")))
                    ->getResult();
    }

    public function updateCompletedStatus(OfferFullSchedule $offerFullSchedule,$error = false)
    {
        $status = $error ? OfferFullSchedule::PAYMENT_STATUS_COMPLETED_UNSUCESS : OfferFullSchedule::PAYMENT_STATUS_COMPLETED ;

        return $this->manager
                    ->createQuery(
                        'UPDATE App\Entity\OfferFullSchedule s
                         SET s.paymentStatus=:payment_status, s.datePaymentRelease=:payment_release 
                         WHERE s.id=:offer_full_schedule'
                    )
                    ->setParameter('payment_status',$status)
                    ->setParameter('offer_full_schedule',$offerFullSchedule->getId())
                    ->setParameter('payment_release', new \DateTime('now', new \DateTimeZone("UTC")))
                    ->getResult();
    }

    // /**
    //  * @return OfferFullSchedule[] Returns an array of OfferFullSchedule objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OfferFullSchedule
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
