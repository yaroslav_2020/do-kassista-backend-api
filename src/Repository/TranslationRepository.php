<?php

declare(strict_types=1);

namespace App\Repository;


use App\Entity\EntityInterface;
use App\Entity\Translation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class TranslationRepository
 * @package App\Repository
 */
class TranslationRepository extends ServiceEntityRepository
{
    /**
     * TranslationRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Translation::class);
    }
    /**
     * @param string $domain
     * @param string $locale
     *
     * @return array|Translation[]
     */
    public function findByDomainAndLocale(string $domain, string $locale)
    {
        return $this->createQueryBuilder('t', 't.source')
            ->where('t.domain = :domain')
            ->andWhere('t.locale = :locale')
            ->setParameter('domain', $domain)
            ->setParameter('locale', $locale)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $domain
     * @param string $locale
     * @param string $source
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function findByDomainLocaleAndSource(string $domain, string $locale, string $source)
    {
        return $this->createQueryBuilder('t', 't.source')
            ->where('t.domain = :domain')
            ->andWhere('t.locale = :locale')
            ->andWhere("t.source = :source")
            ->setParameter('domain', $domain)
            ->setParameter('locale', $locale)
            ->setParameter("source", $source)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}