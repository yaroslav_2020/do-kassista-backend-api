<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\User;
use App\Entity\Voucher;
use App\Entity\Offer;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;

/**
 * Class VoucherRepository
 * @package App\Repository
 */
class VoucherRepository extends ServiceEntityRepository
{
    /** EntityManager $manager */
    private $manager;

    /**
     * UsersRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Voucher::class);

        $this->manager = $registry->getEntityManager();
    }

    /**
     * @param string $code
     * @return object|null
     */
    public function findUsingCode(string $code): ?Voucher
    {
        return $this->findOneBy([
            'code' => $code
        ]);
    }

    public function incrementUsageCount($id){
        $voucher = $this->findOneBy(["id" => $id]);
        
        if(!$voucher instanceof Voucher)
            return false;
        
        $voucher->setUsageCount($voucher->getUsageCount() + 1);

        $this->manager->persist($voucher);
        $this->manager->flush();
    }
}