<?php

declare(strict_types=1);

namespace App\Repository;


use App\Entity\AclAction;
use App\Entity\Voucher;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use function Doctrine\ORM\QueryBuilder;

/**
 * Class AclActionRepository
 * @package App\Repository
 */
class AclActionRepository extends ServiceEntityRepository
{
    /** EntityManager $manager */
    private $manager;

    /**
     * UsersRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AclAction::class);

        $this->manager = $registry->getEntityManager();
    }

    /**
     * @param array $ids
     * @return mixed
     */
    public function findAllUsingIds(array $ids)
    {
        if (empty($ids)) {
            return [];
        }
        $qb = $this->createQueryBuilder("a");
        return $qb->select("a")
            ->where($qb->expr()->in("a.id", $ids))
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(false)
            ->getResult();
    }
}