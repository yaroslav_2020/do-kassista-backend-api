<?php

namespace App\Repository;

use App\Entity\UserScheduleHours;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UserScheduleHours|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserScheduleHours|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserScheduleHours[]    findAll()
 * @method UserScheduleHours[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserScheduleHoursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserScheduleHours::class);
    }

    // /**
    //  * @return UserScheduleHours[] Returns an array of UserScheduleHours objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserScheduleHours
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
