<?php


namespace App\Repository;

use App\Entity\User;
use App\Entity\UserAclAction;
use App\Entity\Voucher;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class UserAclActionRepository
 * @package App\Repository
 */
class UserAclActionRepository extends ServiceEntityRepository
{
    /** EntityManager $manager */
    private $manager;

    /**
     * UsersRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserAclAction::class);

        $this->manager = $registry->getEntityManager();
    }

    /**
     * @param User $user
     */
    public function deleteAllForUser(User $user): void
    {
        $qb = $this->createQueryBuilder("a");
        $qb->delete(null, "a")
            ->where("a.user = :user")
            ->setParameter(":user", $user->getId())
            ->getQuery()
            ->execute();
    }
}