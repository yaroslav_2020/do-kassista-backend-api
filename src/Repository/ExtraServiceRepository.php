<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ExtraService;
use App\Entity\Voucher;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class ExtraServiceRepository
 * @package App\Repository
 */
class ExtraServiceRepository extends ServiceEntityRepository
{
    /** EntityManager $manager */
    private $manager;

    /**
     * UsersRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ExtraService::class);

        $this->manager = $registry->getEntityManager();
    }
}