<?php


declare(strict_types=1);

namespace App\Repository;

use App\Entity\PostalCodeGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class PostalCodeGroupRepository
 * @package App\Repository
 */
class PostalCodeGroupRepository extends ServiceEntityRepository
{
    /** EntityManager $manager */
    private $manager;

    /**
     * UsersRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PostalCodeGroup::class);

        $this->manager = $registry->getEntityManager();
    }
}