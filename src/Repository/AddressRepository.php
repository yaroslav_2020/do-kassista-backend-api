<?php

declare(strict_types=1);

namespace App\Repository;


use App\Entity\Address;
use App\Entity\ExtraService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class AddressRepository
 * @package App\Repository
 */
class AddressRepository extends ServiceEntityRepository
{
    /** EntityManager $manager */
    private $manager;

    /**
     * UsersRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Address::class);

        $this->manager = $registry->getEntityManager();
    }
}