<?php

namespace App\Repository;

use App\Entity\EmailConfirmation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Entity\User;
use App\Entity\Offer;

/**
 * @method EmailConfirmation|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmailConfirmation|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmailConfirmation[]    findAll()
 * @method EmailConfirmation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmailConfirmationRepository extends ServiceEntityRepository
{

    /** EntityManager $manager */
    private $manager;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmailConfirmation::class);
        $this->manager = $registry->getEntityManager();
    }

    public function save($offer,$user,$type,$status){
        try{
            if(!($offer instanceof Offer))
                throw new Exception("Invalid Offer");

            if(!($user instanceof User))
                throw new Exception("Invalid User");

            $registrationDate = new \DateTime("now");

            $emailConfirmation = new EmailConfirmation();
            $emailConfirmation->addOffer($offer);
            $emailConfirmation->addUser($user);
            $emailConfirmation->setStatus($status);
            $emailConfirmation->setType($type);
            $emailConfirmation->setRegistrationDate($registrationDate);

            $this->manager->persist($emailConfirmation);
            $this->manager->flush();

        }catch(Exception $error){
            $this->logger->error("Something Wrong: " . $error->getMessage());
        }
    }

    // /**
    //  * @return EmailConfirmation[] Returns an array of EmailConfirmation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EmailConfirmation
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
