<?php

declare(strict_types=1);

namespace App\Helpers;


use App\Exception\AddressTypeAssert\AddressTypeAssertException;
use App\Entity\Address;

/**
 * Class AddressTypeAssert
 * @package App\Helpers
 */
class AddressTypeAssert
{
    private $assertResult;

    /** @var string */
    private $lastCheckMessage;

    /**
     * AddressTypeAssert constructor.
     * @param string $address_type
     * @throws AddressTypeAssertException
     */
    private function __construct($address_type)
    {
        $this->lastCheckMessage = "checking is integer";
        if (!in_array($address_type, [Address::SERVICE_ADDRESS, Address::COMPANY_ADDRESS,Address::BILLING_ADDRESS], true)) {
            $this->assertResult = false;
            $this->throwExceptionIfFalse();  
        }
    }

    /**
     * @param $arg
     * @return AddressTypeAssert
     * @throws AddressTypeAssertException
     */
    public static function validateOrException($arg)
    {
        return new AddressTypeAssert($arg);
    }

    /**
     * @throws AddressTypeAssertException
     */
    public function throwExceptionIfFalse()
    {
        if ($this->assertResult === false) {
            throw new AddressTypeAssertException($this->formatException("Failed to assert"));
        }
    }

    /**
     * @param string $message
     * @return string
     */
    public function formatException(string $message)
    {
        return $message . " while " . $this->lastCheckMessage;
    }
}