<?php

declare(strict_types=1);

namespace App\Helpers;


use App\Exception\ArrayAssert\ArrayAssertException;

/**
 * Class ArrayAssert
 * @package App\Helpers
 */
class ArrayAssert
{
    private $assertResult;

    /** @var array */
    private $workingArray;

    /** @var string */
    private $lastCheckMessage;

    /**
     * ArrayAssert constructor.
     * @param array $array
     * @throws ArrayAssertException
     */
    private function __construct($array)
    {
        $this->lastCheckMessage = "checking is array";
        if (!is_array($array)) {
            $this->assertResult = false;
            $this->throwExceptionIfFalse();
        }
        $this->workingArray = $array;
    }

    /**
     * @param $arg
     * @return ArrayAssert
     * @throws ArrayAssertException
     */
    public static function validateOrException($arg)
    {
        return new ArrayAssert($arg);
    }

    /**
     * @param $key
     * @return $this
     * @throws ArrayAssertException
     */
    public function hasNonEmptyKey($key)
    {
        $this->lastCheckMessage = "checking contains key $key";
        $this->assertResult = !empty($this->workingArray[$key]);
        $this->throwExceptionIfFalse();

        return $this;
    }

    /**
     * @param $key
     * @return $this
     * @throws ArrayAssertException
     */
    public function hasKey($key)
    {
        $this->lastCheckMessage = "checking contains key $key";
        $this->assertResult = ((!empty($this->workingArray[$key])) || ($this->workingArray[$key] == 0 && is_int($this->workingArray[$key]))) ? true : false;
        $this->throwExceptionIfFalse();

        return $this;
    }

    /**
     * @param $key
     * @return $this
     * @throws ArrayAssertException
     */
    public function hasBooleanKey($key)
    {
        $this->lastCheckMessage = "checking contains key $key";
        $this->assertResult = ((!empty($this->workingArray[$key])) || ($this->workingArray[$key] == false && is_bool($this->workingArray[$key]))) ? true : false;
        $this->throwExceptionIfFalse();

        return $this;
    }

    /**
     * @throws ArrayAssertException
     */
    public function throwExceptionIfFalse()
    {
        if ($this->assertResult === false) {
            throw new ArrayAssertException($this->formatException("Failed to assert"));
        }
    }

    /**
     * @param string $message
     * @return string
     */
    public function formatException(string $message)
    {
        return $message . " while " . $this->lastCheckMessage;
    }
}