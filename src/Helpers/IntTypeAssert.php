<?php

declare(strict_types=1);

namespace App\Helpers;


use App\Exception\IntTypeAssert\IntTypeAssertException;

/**
 * Class IntTypeAssert
 * @package App\Helpers
 */
class IntTypeAssert
{
    private $assertResult;

    /** @var string */
    private $lastCheckMessage;

    /**
     * IntTypeAssert constructor.
     * @param int $integer
     * @throws IntTypeAssertException
     */
    private function __construct($integer)
    {
        $this->lastCheckMessage = "checking is integer";
        if (!is_int($integer)) {
            $this->assertResult = false;
            $this->throwExceptionIfFalse();            
        }
    }

    /**
     * @param $arg
     * @return IntTypeAssert
     * @throws IntTypeAssertException
     */
    public static function validateOrException($arg)
    {
        return new IntTypeAssert($arg);
    }

    /**
     * @throws IntTypeAssertException
     */
    public function throwExceptionIfFalse()
    {
        if ($this->assertResult === false) {
            throw new IntTypeAssertException($this->formatException("Failed to assert"));
        }
    }

    /**
     * @param string $message
     * @return string
     */
    public function formatException(string $message)
    {
        return $message . " while " . $this->lastCheckMessage;
    }
}