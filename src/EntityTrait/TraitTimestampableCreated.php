<?php


namespace App\EntityTrait;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class TraitTimestampableCreated
 * @package App\EntityTrait
 */
trait TraitTimestampableCreated
{
    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", name="timestampable_created_at", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     * @Groups({"offer:view", "client:create","user:offers"})
     */
    protected $timestapableCreated;

    /**
     * @return \DateTime
     */
    public function getTimestapableCreated(): \DateTime
    {
        return $this->timestapableCreated;
    }

    /**
     * @param \DateTime $timestapableCreated
     */
    public function setTimestapableCreated(\DateTime $timestapableCreated)
    {
        $this->timestapableCreated = $timestapableCreated;
    }
}