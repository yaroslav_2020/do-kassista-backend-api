<?php


namespace App\EntityTrait;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class TraitTimestapableUpdated
 * @package App\EntityTrait
 */
trait TraitTimestapableUpdated
{
    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", name="timestampable_update_at", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    protected $timestampableUpdated;

    /**
     * @return \DateTime
     */
    public function getTimestampableUpdated(): \DateTime
    {
        return $this->timestampableUpdated;
    }

    /**
     * @param \DateTime $timestampableUpdated
     */
    public function setTimestampableUpdated(\DateTime $timestampableUpdated)
    {
        $this->timestampableUpdated = $timestampableUpdated;
    }
}