<?php

declare(strict_types=1);

namespace App\Normalizer;


use App\Entity\AclAction;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;

/**
 * Class AclActionNormalizer
 * @package App\Normalizer
 */
class AclActionNormalizer  implements ContextAwareNormalizerInterface
{
    /**
     * UserDataNormalizer constructor.
     */
    public function __construct()
    {
    }

    /**
     * {@inheritdoc}
     *
     * @param array $context options that normalizers have access to
     */
    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return is_object($data) && ($data instanceof AclAction) && in_array("SymfonyController", $context, true);
    }

    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param AclAction $object Object to normalize
     * @param string $format Format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|string|int|float|bool
     *
     * @throws InvalidArgumentException   Occurs when the object given is not an attempted type for the normalizer
     * @throws CircularReferenceException Occurs when the normalizer detects a circular reference when no circular
     *                                    reference handler can fix it
     * @throws LogicException             Occurs when the normalizer is not called in an expected context
     * @throws ExceptionInterface         Occurs for all the other cases of errors
     * @throws \Exception
     */
    public function normalize($object, $format = null, array $context = [])
    {
        if (!($object instanceof AclAction)) {
            throw new \InvalidArgumentException("Invalid type, must be instnace of " . AclAction::class);
        }

        return [
            "id" => $object->getId(),
            "action_id" => $object->getActionId(),
            "action_pretty_name" => $object->getActionPrettyName(),
            "category" => $object->getCategory()
        ];
    }
}