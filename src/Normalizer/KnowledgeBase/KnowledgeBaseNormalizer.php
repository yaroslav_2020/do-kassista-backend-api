<?php


namespace App\Normalizer\KnowledgeBase;


use App\Application\Translation\DbTranslator;
use App\Entity\Faq;
use App\Entity\KnowledgeBase;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

/**
 * Class KnowledgeBaseNormalizer
 * @package App\Normalizer\KnowledgeBase
 */
class KnowledgeBaseNormalizer implements ContextAwareNormalizerInterface
{
    /** @var DbTranslator */
    private $dbTranslator;

    /**
     * FaqNormalizer constructor.
     * @param DbTranslator $dbTranslator
     */
    public function __construct(DbTranslator $dbTranslator)
    {
        $this->dbTranslator = $dbTranslator;
    }

    /**
     * {@inheritdoc}
     *
     * @param array $context options that normalizers have access to
     */
    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return is_object($data) && ($data instanceof KnowledgeBase);
    }

    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param KnowledgeBase $object Object to normalize
     * @param string $format Format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|string|int|float|bool
     *
     * @throws InvalidArgumentException   Occurs when the object given is not an attempted type for the normalizer
     * @throws CircularReferenceException Occurs when the normalizer detects a circular reference when no circular
     *                                    reference handler can fix it
     * @throws LogicException             Occurs when the normalizer is not called in an expected context
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            "title" => $this->dbTranslator->trans($object->getTitle()),
            "content" => $this->dbTranslator->trans($object->getContent()),
        ];
    }
}