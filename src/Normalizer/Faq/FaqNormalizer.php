<?php


namespace App\Normalizer\Faq;

use App\Application\Translation\DbTranslator;
use App\Entity\Faq;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

/**
 * Class FaqNormalizer
 * @package App\Normalizer\Faq
 */
class FaqNormalizer implements ContextAwareNormalizerInterface
{
    /** @var DbTranslator */
    private $dbTranslator;

    /**
     * FaqNormalizer constructor.
     * @param DbTranslator $dbTranslator
     */
    public function __construct(DbTranslator $dbTranslator)
    {
        $this->dbTranslator = $dbTranslator;
    }

    /**
     * {@inheritdoc}
     *
     * @param array $context options that normalizers have access to
     */
    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return is_object($data) && ($data instanceof Faq);
    }

    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param Faq $object Object to normalize
     * @param string $format Format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|string|int|float|bool
     *
     * @throws InvalidArgumentException   Occurs when the object given is not an attempted type for the normalizer
     * @throws CircularReferenceException Occurs when the normalizer detects a circular reference when no circular
     *                                    reference handler can fix it
     * @throws LogicException             Occurs when the normalizer is not called in an expected context
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            "question" => $this->dbTranslator->trans($object->getQuestion()),
            "answer" => $this->dbTranslator->trans($object->getAnswer()),
        ];
    }
}