<?php

declare(strict_types=1);

namespace App\Normalizer\User;


use App\Entity\User;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

/**
 * Normalizer created for user basic data, in order to be used in more places
 *
 * Class UserDataNormalizer
 * @package App\Normalizer\User
 */
class UserDataNormalizer implements ContextAwareNormalizerInterface
{

    /**
     * UserDataNormalizer constructor.
     */
    public function __construct()
    {
    }

    /**
     * {@inheritdoc}
     *
     * @param array $context options that normalizers have access to
     */
    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return is_object($data) && ($data instanceof User) && in_array("SymfonyController", $context, true);
    }

    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param mixed $object Object to normalize
     * @param string $format Format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|string|int|float|bool
     *
     * @throws InvalidArgumentException   Occurs when the object given is not an attempted type for the normalizer
     * @throws CircularReferenceException Occurs when the normalizer detects a circular reference when no circular
     *                                    reference handler can fix it
     * @throws LogicException             Occurs when the normalizer is not called in an expected context
     * @throws ExceptionInterface         Occurs for all the other cases of errors
     * @throws \Exception
     */
    public function normalize($object, $format = null, array $context = [])
    {
//        debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
//        die('now');
        $now = new \DateTime('now', new \DateTimeZone('UTC'));
        return [
            "role" => $object->getRoles(),
            "from" =>  "jwt",
            "time" => $now->format("Y-m-d H:i:s"),
            "data" =>  [
                "iri" => "/api/users/" . $object->getId(),
                "id" => $object->getId(),
                "displayName" => $object->getName(),
                "photoURL" => "https://png.pngtree.com/element_our/png/20181206/users-vector-icon-png_260862.jpg",
                "email" =>  $object->getEmail(),
                "settings" => [],
                "shortcuts" => []
            ]
        ];
    }
}