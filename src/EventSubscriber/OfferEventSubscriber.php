<?php
namespace App\EventSubscriber;

use App\Entity\OfferPointService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Product;
use App\Entity\Offer;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Serializer\SerializerInterface;
use App\Application\Mailer\AuthMailer;
use App\Application\Security\SecurityService;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelEvents;
use FOS\UserBundle\Mailer\MailerInterface;
use App\Entity\User;
use Symfony\Component\Config\Definition\Exception\Exception;
use App\Helpers\ArrayAssert;
use App\Exception\ArrayAssert\ArrayAssertException;
use App\Entity\EmailConfirmation;
use App\Controller\StripeController;
use App\Controller\OfferFullScheduleController;

class OfferEventSubscriber extends AbstractController implements EventSubscriberInterface
{

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var SecurityService
     */
    private $securityService;

    private $users_availables;

    private $stripe;

    private $params;

    private $offerFullScheduleController;

    public function __construct(
        LoggerInterface $logger,
        SerializerInterface $serializer,
        AuthMailer $mailer,
        SecurityService $securityService,
        StripeController $stripe,
        OfferFullScheduleController $offerFullScheduleController,
        ParameterBagInterface $params
    )
    {
        $this->logger = $logger;
        $this->mailer = $mailer;
        $this->serializer = $serializer;
        $this->securityService = $securityService;
        $this->users_availables = [];
        $this->stripe = $stripe;
        $this->offerFullScheduleController = $offerFullScheduleController;
        $this->params = $params;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['postPersist', EventPriorities::POST_WRITE], //used for post and put
        ];
    }

        public function postPersist(ViewEvent $event)
    {
        try{
            /** @var Offer $offer */
            $offer = $event->getControllerResult();

            if (!$this->supportsEntity($offer) || !$this->supportsOperation($event)) {
                $this->logger->debug("[Post write NEW OFFER]-------------NO-SUPPORT...");
                return;
            }

            $this->logger->debug("[Post write NEW OFFER::START]-------------Handle post write...");

            $this->stripe->setStripeKey($this->params->get('stripe_key') );

            $this->offerFullScheduleController->createOfferFullSchedule($offer);

            $client = $offer->getClient()[0];

            $status = $this->mailer->sendNewOfferInfo($offer,$client->getEmail());

            $this->getDoctrine()->getRepository(EmailConfirmation::class)->save($offer,$client,"OFFER_CLIENT",$status);

            $users = $this->getDoctrine()->getRepository(User::class)->findWorkers();

            if(!empty($users)){
                foreach($users as $user){
                    if($user instanceof User){
                        if(in_array(User::ROLE_EMPLOYEE_WORKER,$user->getRoles()) || in_array(User::ROLE_INDEPENDENT_WORKER,$user->getRoles())){
                            // DOES WORKER MATCH PET CONDITION
                            $userMatchPet = $this->verifyPetCondition($user, $offer);
                            if ($userMatchPet) {
                                // DOES WORKER MATCH SKILLS CONDITION
                                $userMatchSkills = $this->verifyUserPreference($user, $offer);
                                if ($userMatchSkills) {
                                    // RETURN USER IF SCHEDULE MATCH CONDITION
                                    $result = $this->verifyUserSchedule($user,$offer);
                                    if($result instanceof User){
                                        $this->logger->debug("[Post write NEW OFFER::MATCH]---------------------> USER DO MATCH OFFER REQUIREMENTS");

                                        $this->users_availables[] = $result;
                                    }
                                }
                            }
                        }
                    }
                }

                if(!empty($this->users_availables))
                    foreach($this->users_availables as $user)
                        if($user instanceof User)
                            $this->sendEmailToWorker($user,$offer);                
            }else{
                throw new Exception("There aren't any users");
            }

        }catch (ArrayAssertException $arrayAssertException) {
            $this->logger->error("Error while registering new user, invalid data: " . $arrayAssertException->getMessage());
            $this->logger->error("something wrong");
        }catch(Exception $error){
            $this->logger->error('Something Wrong: '.$error->getMessage());
        }
    }

    public function verifyPetCondition (User $user, Offer $offer): bool
    {
        try {
            if ($user->getUserPreferences()->isEmpty())
                throw new Exception('User does not have setting preferences');

            $petPreference = array_filter(
                $user->getUserPreferences()->toArray(),
                function ($preference)
                {
                    // IF SERVICE IS NULL, THEN IS PET PRESENCE ROW
                    return $preference->getExtraService() === null;
                }
            );

            $isPetAllowOnPreference = $petPreference[0]->getPetPresence();
            $isPetOnOffer = !($offer->getPet() === Offer::PET_NOT);

            // DO PET MATCH ON THE OFFER AND USER PREFERENCE
            return (!$isPetOnOffer || $isPetAllowOnPreference);

        } catch (Exception $error){
            $this->logger->error('Something Wrong: ' . $error->getMessage());
            return false;
        }
    }


    public function verifyUserPreference (User $user, Offer $offer){
        try {
            if ($user->getUserPreferences()->isEmpty())
                throw new Exception('User does not have setting preferences');

            $userPreferences = array_map(
                function ($preference) use ($offer)
                {
                    $userPreference = $preference->getExtraService();
                    // IF $userPreference NOT NULL, THEN IS A SERVICE
                    if($userPreference !== null) {
                        return $userPreference->getId();
                    }
                    return null;
                },
                $user->getUserPreferences()->toArray()
            );

            if($offer->getPeriodPoints()->isEmpty())
                throw new Exception('Offer does not have period points');

            // WE ITERATE THROUGH ONLY THE FIRST DAY (PERIOD POINT)
            // BECAUSE THE SERVICES CHOSEN FOR THE OFFER
            // ARE THE SAME FOR ALL DAYS
            // *** THE PERIOD POINT FUNCTIONALITY WAS INTENDED TO HAVE DIFFERENT SERVICES ON DIFFERENT DAYS ***
            $firstDayPeriodPoint = $offer->getPeriodPoints()[0];

            $firstDayServices = $firstDayPeriodPoint->getPointServices();

            if($firstDayServices->isEmpty())
                throw new Exception('Offer Point Services do not have point services');

            $servicesMatch = true;

            if(!empty($firstDayServices)){
                foreach($firstDayServices as $offerService){
                    if($offerService instanceof OfferPointService){
                        // INSIDE EACH OFFER EXTRA SERVICE
                        // IF $offerService IS NOT ON $userPreferences
                        // THEN $servicesMatch BECOMES FALSE
                        if ( !in_array($offerService->getExtraService()->getId(), $userPreferences) ) {
                            $servicesMatch = false;
                            break;
                        }
                    }
                }
                return $servicesMatch;
            }
        } catch (Exception $error){
            $this->logger->error('Something Wrong: ' . $error->getMessage());
            return false;
        }
    }

    public function verifyUserSchedule (User $user,Offer $offer){
        try{
            if($user->getUserSchedules()->isEmpty())
                throw new Exception('User does not have a schedule');

            $userSchedule = $user->getUserSchedules();
    
            $userSchedule = $this->refactorUserScheduleStructure($userSchedule);
    
            if($offer->getPeriodPoints()->isEmpty())
                throw new Exception('Offer does not have period points');

            $offerPeriodPoints = $offer->getPeriodPoints();

            if(!empty($offerPeriodPoints)){
                $able = [];
                foreach($offerPeriodPoints as $periodPoint){
                    $startHour = $this->timeStringToDateTime($periodPoint->getStartHour());
                    $endHour = $this->timeStringToDateTime($periodPoint->getEndHour());

                    if(!($startHour instanceof \DateTime) || !($endHour instanceof \DateTime))
                        throw new Exception('Start or end time is not in DateTime format');

                    ArrayAssert::validateOrException($userSchedule)
                                ->hasNonEmptyKey($periodPoint->getDay());
                    
                    if($userSchedule[$periodPoint->getDay()]->isEmpty())
                        throw new Exception("User doesn't have intervals time in " . $periodPoint->getDay());

                    foreach($userSchedule[$periodPoint->getDay()] as $user_period){
                        $userStartTime = $this->timeStringToDateTime($user_period->getStartTime());
                        $userEndTime = $this->timeStringToDateTime($user_period->getEndTime());
                        
                        if(!($userStartTime instanceof \DateTime) || !($userEndTime instanceof \DateTime))
                            throw new Exception('userStartTime or userEndTime is not in DateTime format');
                        
                        if($userStartTime <= $startHour && $userEndTime >= $endHour){
                            $able[$periodPoint->getDay()] = true;
                            break;
                        }else{
                            $able[$periodPoint->getDay()] = false;
                        }                            
                    }
                }
                $flag = true;
                foreach($able as $day)
                    if(!$day)
                        $flag = false;

                if($flag){
                    return $user;
                }else{
                    return false;
                }
            }

        }catch (ArrayAssertException $arrayAssertException) {
            $this->logger->error("Error while verify user schedule, user does not work in that day: " . $arrayAssertException->getMessage());
            return false;            
        }catch(Exception $error){
            $this->logger->error('Something Wrong: ' . $error->getMessage());
            return false;
        }
    }

    public function refactorUserScheduleStructure($userSchedule){
        $userSchedule2 = [];
        foreach($userSchedule as $userScheduleDay){
            $userSchedule2[$userScheduleDay->getDay()] = $userScheduleDay->getUserScheduleHours();
        }

        return $userSchedule2;
    }

    public function timeStringToDateTime($time){
        $hour = explode(":",$time);
        $result = new \DateTime("now");
        $result->setTime(intval($hour[0]),intval($hour[1]));
        return $result;
    }

    public function sendEmailToWorker($worker,$offer){
        try{
            if(!($worker instanceof User))
                throw new Exception('Invalid User parameter at sendEmailWorker function');
            
            if(!($offer instanceof Offer))
                throw new Exception('Invalid Offer parameter at sendEmailWorker function');
            
            $confirmation_link = $this->securityService->generateConfirmationLinkToOffers($offer->getOfferHash(), $worker->getUserHash());
            $status = $this->mailer->sendNewOfferInfoToWorkers($offer,$worker->getEmail(),$confirmation_link);        
            $this->getDoctrine()->getRepository(EmailConfirmation::class)->save($offer,$worker,"OFFER_WORKER",$status);
            $this->logger->info("Sending email to the worker");
        }catch(Exception $error){
            $this->logger->error("Something Wrong: " . $error->getMessage());
        }
    }


    /**
     * @param ViewEvent $event
     * @return bool
     */
    private function supportsOperation(ViewEvent $event): bool
    {
        //check whatever you need to check to figure out the route is a new/update action
        $method = $event->getRequest()->getMethod();

        return (Request::METHOD_POST === $method);
    }

    /**
     * @param $entity
     * @return bool
     */
    private function supportsEntity($entity): bool
    {
        return (is_object($entity)) && ($entity instanceof Offer);
    }
}