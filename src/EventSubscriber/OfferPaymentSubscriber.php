<?php

namespace App\EventSubscriber;

use Symfony\Component\Dotenv\Dotenv;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Payment;
use App\Entity\Offer;

class OfferPaymentSubscriber extends AbstractController implements EventSubscriber
{

    private $logger;

    private $env;

    private $charge;

    public function __construct(
        LoggerInterface $logger
    ){
        $this->logger = $logger;
        $this->charge = null;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist
        ];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $status = 'processing';
        $errorMessage = '';

        try{
            $offer = $args->getObject();                 
            
            if($offer instanceOf Offer){             
                
                $payment = $this->getDoctrine()->getRepository(Payment::class)->findTheFirstPayment(['offer_id' => $offer->getId()]);

                if($payment instanceof Payment){
                    \Stripe\Stripe::setApiKey($this->getParameter('stripe_key'));
                
                    // $stripe = new \Stripe\StripeClient(
                    //     $this->getParameter('stripe_key')
                    // );
                    
                    // $token = $stripe->tokens->create([
                    //     'card' => [
                    //         'number' => '4242424242424242',
                    //         'exp_month' => 9,
                    //         'exp_year' => 2021,
                    //         'cvc' => '314',
                    //     ],
                    // ]);

                    if($payment->getCardToken() !== ''){
                        $this->charge = \Stripe\Charge::create([
                            'amount' => $payment->getAmount(),
                            'currency' => $payment->getCurrency(),
                            'description' => $payment->getDescription(),
                            'source' => $payment->getCardToken()
                        ]);
    
                        $status = 'Completed';
                    }                    
                }else{
                    return;
                }                
            }else{
                return;
            }

        }catch(\Stripe\Exception\CardException $e) {
            $status = 'Error';
            $errorMessage = $e->getMessage();
            $this->charge = null;
            $this->logger->error($e->getMessage());
        } catch (\Stripe\Exception\RateLimitException $e) {
            $status = 'Error';
            $errorMessage = $e->getMessage();
            $this->charge = null;
            $this->logger->error($e->getMessage());
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $status = 'Error';
            $errorMessage = $e->getMessage();
            $this->charge = null;
            $this->logger->error($e->getMessage());
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $status = 'Error';
            $errorMessage = $e->getMessage();
            $this->charge = null;
            $this->logger->error($e->getMessage());
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $status = 'Error';
            $errorMessage = $e->getMessage();
            $this->charge = null;
            $this->logger->error($e->getMessage());
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $status = 'Error';
            $errorMessage = $e->getMessage();
            $this->charge = null;
            $this->logger->error($e->getMessage());
        } catch (Exception $e) {
            $status = 'Error';
            $errorMessage = $e->getMessage();
            $this->charge = null;
            $this->logger->error($e->getMessage());
        }

        if($offer instanceOf Offer && $payment instanceOf Payment){
            if($this->charge !== null){
                $createdDate = new \DateTime();
                $createdDate->setTimestamp($this->charge->created);
                $this->getDoctrine()->getRepository(Payment::class)->savePaymentCreatingOffer([
                    'code' => $this->charge->id,
                    'status' => $status,
//                    'date' => date_format($createdDate, 'Y-m-d H:i:s'),
                    'date' => $createdDate->format('Y-m-d H:i:s'),
                    'amount' => $this->charge->amount,
                    'description' => $this->charge->description,
                    'paymentUrl' => $this->charge->receipt_url,
                    'errorMessage' => '',
                    'offer' => $offer
                ]);
            }else{
                $this->getDoctrine()->getRepository(Payment::class)->savePaymentCreatingOfferError([
                    'status' => $status,
                    'errorMessage' => $errorMessage,
                    'offer' => $offer
                ]);
            }    
        }
    }
}