- test user register
curl -X POST -H "Content-Type: application/json" "http://localhost:8000/register?email=test1@mail.com&password=test1"

## Jwt token articles
- https://symfonycasts.com/screencast/symfony-rest4/jwt-other-things

# Install
## Dev environment
- create jwt keys in config/jwt with 0777 rights
- create .env file using .env.dev.dist file
- run
````
composer install
sudo chmod -R 0777 var
php bin/console cache:warmup
php bin/console doctrine:schema:update --dump-sql -f
php bin/console server:run
````

- generate jwt token files
mkdir -p config/jwt
jwt_passhrase=$(grep ''^JWT_PASSPHRASE='' .env | cut -f 2 -d ''='')
echo "$jwt_passhrase" | openssl genpkey -out config/jwt/private.pem -pass stdin -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
echo "$jwt_passhrase" | openssl pkey -in config/jwt/private.pem -passin stdin -out config/jwt/public.pem -pubout
setfacl -R -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
setfacl -dR -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt


### Loading fixtures
````
php bin/console doctrine:fixtures:load
````