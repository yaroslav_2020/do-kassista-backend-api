<?php


namespace App\Tests\Translation;

use App\Application\Translation\DbTranslator;
use Nelmio\ApiDocBundle\Tests\Functional\FunctionalTest;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class BaseTest
 * @package App\Tests\Translation
 */
class BaseTest extends TestCase
{
    public function testAdd()
    {
//        $crawler = $this->client->request('GET', '/');
//        echo $crawler->filter('body section.content h1')->text();
//        $this->assertContains('unicorns_page.title', $crawler->filter('body section.content h1')->text());

        $tranlsated = DbTranslator::translate("welcome.user", ["%user%" => "Ionut"], "register", "en", [
            "register" => [
                "welcome.user" => "welcome %user%"
            ]
        ]);
        self::assertSame($tranlsated, "welcome Ionut");
        // assert that your calculator added the numbers correctly!
        $this->assertEquals(42, 42);
    }
}