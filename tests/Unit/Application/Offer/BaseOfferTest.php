<?php


namespace App\Tests\Unit\Application\Offer;


use App\Application\Voucher\VoucherManager;
use App\Entity\ExtraService;
use App\Entity\Offer;
use App\Entity\OfferBuilding;
use App\Entity\OfferPeriodPoint;
use App\Entity\OfferPointService;
use App\Entity\Voucher;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\User;
use Symfony\Component\Console\Tester\CommandTester;

class BaseOfferTest extends WebTestCase
{
    public function testOffer()
    {
        self::bootKernel();

        // returns the real and unchanged service container
        $container = self::$kernel->getContainer();

        // gets the special container that allows fetching private services
        $container = self::$container;

        $em = self::$container->get('doctrine.orm.entity_manager');

        try {
            $offer = new Offer();
            $offer->setStartFromDate(new \DateTime('now'));
            $offer->setPeriod(Offer::PERIOD_TYPE_FOUR_WEEKS);

            //add voucher
            $voucherManager = $container->get(VoucherManager::class);
            $newVoucher = $voucherManager->factoryVoucher();
            $offer->setVouchers($newVoucher);

            //add building
            $offerBuilding = new OfferBuilding();
            $offerBuilding->setRoomNumber(4);
            $offer->setOfferBuilding($offerBuilding);
            $offerBuilding->setOffer($offer);

            //add point with one extra service
            $point = new OfferPeriodPoint();
            $point->setValue(44);
            $point->setDurationInMinutes(33);
            $point->setOffer($offer);

            $extraService = new ExtraService();
            $extraService->setName(rand(1, 400000) . "_service");
            $extraService->setAddedValue(44);

            $pointService = new OfferPointService();
            $pointService->setOfferPeriodPoint($point);
            $pointService->setExtraService($extraService);
            $point->setPointServices(new ArrayCollection([$pointService]));

            $offer->setPeriodPoints(new ArrayCollection([$point]));

            // save data
            $em->persist($offer);
            $em->flush($offer);
        } catch (\Exception $exception) {
            die(print_r($exception->getMessage()));
            self::assertSame(true, false);
        }
        self::assertSame(true, true);
        //        $em->persist($offer);
        //        $em->flush($offer);
        //        $user = self::$container->get('doctrine')->getRepository(Offer::class)->findOneByEmail('...');

        //        $this->assertTrue(self::$container->get('security.password_encoder')->isPasswordValid($user, '...');

    }
}
