<?php


namespace App\Tests\Unit\Application\Intl;


use App\Application\Intl\LocaleService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorBagInterface;
use Symfony\Contracts\Translation\LocaleAwareInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class LocaleServiceTest
 * @package App\Tests\Unit\Application\Intl
 */
class LocaleServiceTest extends TestCase
{
    public function testLocale()
    {
        $locales = ["en", "ro", "fr"];

        $localeService = new LocaleService(
            new class() implements TranslatorInterface, LocaleAwareInterface, TranslatorBagInterface {

                protected $locale;

                public function setLocale($locale)
                {
                    $this->locale = $locale;
                }

                public function getLocale()
                {
                    return $this->locale;
                }


                public function getCatalogue($locale = null)
                {
                    return [
                        "en" => [
                            "test" => "test_en"
                        ],
                        "fr" => [
                            "test" => "test_fr"
                        ]
                    ];
                }

                public function trans($id, array $parameters = [], $domain = null, $locale = null)
                {
                    $this->getCatalogue()[$locale][$domain][$id];
                }
            },
            new class() implements RouterInterface {

                public function setContext(RequestContext $context)
                {
                    // TODO: Implement setContext() method.
                }

                public function getContext()
                {
                    // TODO: Implement getContext() method.
                }
                public function getRouteCollection()
                {
                    // TODO: Implement getRouteCollection() method.
                }

                public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH)
                {
                    // TODO: Implement generate() method.
                }

                public function match($pathinfo)
                {
                    // TODO: Implement match() method.
                }
            }
        );

        foreach ($locales as $locale) {
            $localeService->push($locale);
            self::assertEquals($locale, $localeService->top());
            $localeService->pop();
            self::assertEquals(null, $localeService->top());
        }

        $localeService->push("en");
        $localeService->clear();;
        self::assertEquals(0, $localeService->count());
    }
}