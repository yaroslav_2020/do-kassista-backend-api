<?php


namespace App\Tests\Unit\Application\Voucher;

use App\Application\Intl\LocaleService;
use App\Application\Voucher\VoucherManager;
use App\Entity\Voucher;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorBagInterface;
use Symfony\Contracts\Translation\LocaleAwareInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class VoucherManagerTest
 * @package App\Tests\Unit\Application\Voucher
 */
class VoucherManagerTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testLocale()
    {
        $manager = new VoucherManager($this->getEmMock());

        $startDate = new \DateTime('now', new \DateTimeZone('UTC'));

        //test new voucher works
        $newVoucherOptions = [
            'title' => "test_1",
            'type' => Voucher::TYPE_PERCENT,
            'active_status' => 0,
            'usage_type' => Voucher::USAGE_ONE_TIME_BY_FIRST_CLIENT,
            'start' => $startDate,
        ];
        $newVoucher = $manager->factoryVoucher($newVoucherOptions);
        $this->checkNewVoucher($newVoucher, $startDate);

        //test expire
        $end = new \DateTime("now", new \DateTimeZone("UTC"));
        $end->modify("+1 day");
        $manager->setValidityInterval(
            $newVoucher,
            new \DateTime("now", new \DateTimeZone("UTC")),
            $end
        );

        //voucher inactive, test can not be used
        self::assertSame(false, $manager->canUse($newVoucher));

        //ativate, test can be used
        $manager->activateVoucher($newVoucher);
        self::assertSame(true, $manager->canUse($newVoucher));

        //deactivate voucher, test can not be used
        $manager->deactivateVoucher($newVoucher);
        self::assertSame(false, $manager->canUse($newVoucher));
        //revert
        $manager->activateVoucher($newVoucher);

        //test expire
        self::assertSame(true, $manager->canUse($newVoucher));
        $manager->expireVoucher($newVoucher);
        self::assertSame(false, $manager->canUse($newVoucher));

        //revert, make it active again
        $start = new \DateTime("now", new \DateTimeZone("UTC"));
        $manager->setValidityInterval($newVoucher, $start, $end);
        self::assertSame(true, $manager->canUse($newVoucher));

        //set start in the feature, check inactive
        $start->modify("+1 day");
        $manager->setValidityInterval($newVoucher, $start, $end);
        self::assertSame(false, $manager->canUse($newVoucher));
        //revert
        $manager->setValidityInterval($newVoucher, new \DateTime("now", new \DateTimeZone("UTC")), $end);
        self::assertSame(true, $manager->canUse($newVoucher));

        $manager->increaseUsages($newVoucher);
        self::assertSame(1, $newVoucher->getUsageCount());
        //can be used any more, usages > 1 and only once can be used
        self::assertSame(false, $manager->canUse($newVoucher));
        $manager->increaseUsages($newVoucher);
        self::assertSame(2, $newVoucher->getUsageCount());

        //now can be used because i changed the type
        $newVoucher->setUsageType(Voucher::USAGE_TYPE_BY_ANY_CLIENT_ANY_TIME);
        self::assertSame(true, $manager->canUse($newVoucher));

    }

    private function checkIsEpired(Voucher $newVoucher)
    {

    }
    /**
     * @param Voucher $newVoucher
     * @param \DateTime $startDate
     */
    private function checkNewVoucher(Voucher $newVoucher, \DateTime $startDate)
    {
        self::assertSame($newVoucher->getTitle(), 'test_1');
        self::assertSame($newVoucher->getType(), Voucher::TYPE_PERCENT);
        self::assertSame($newVoucher->getType(), Voucher::TYPE_PERCENT);
        self::assertSame($newVoucher->getActiveStatus(), 0);
        self::assertSame($newVoucher->getUsageType(), Voucher::USAGE_ONE_TIME_BY_FIRST_CLIENT);
        self::assertSame($newVoucher->getEndDate(), null);
        self::assertSame($newVoucher->getStartDate()->format('Y-m-d H:i:s'), $startDate->format('Y-m-d H:i:s'));
    }


    /**
     * @return EntityManagerInterface
     */
    protected function getEmMock(): EntityManagerInterface
    {
        $mockBuilder = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor();

        $emMock  = $mockBuilder->getMock();
        $emMock->expects($this->any())
            ->method('persist')
            ->will($this->returnValue(null));
        $emMock->expects($this->any())
            ->method('flush')
            ->will($this->returnValue(null));
        $emMock->expects($this->any())
            ->method('refresh')
            ->will($this->returnValue(null));

        return $emMock;
    }
}